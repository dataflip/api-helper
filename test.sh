#!/bin/bash
#
# This is running tests scripts
#
echo -n starting processsing tests .

# use --debug for debugging purposes https://codeception.com/docs/reference/Commands
 export XDEBUG_MODE=coverage
## Execute tests
 php vendor/bin/codecept run --coverage-html
 
echo "Done"