<?php

namespace DataBackFlip;

use Exception;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;

class ApiDocumentation extends ApiHelper
{
    public string $workSpaceId;
    public string $environmentId;

    public array $folderIds = [];
    public array $folderNames = [];
    public array $insomniaStructure = [];
    public array $getRequestRules = [];
    public array $getQueryParamRules = [];
    public array $getResponseRules = [];

    /**
     * @param array $params
     * @param array $extras
     * @param bool $debug
     * @param string|null $defaultNamespace
     * @param string|null $defaultNamespaceFolder
     * @throws Exception
     */
    public function __construct(array $params, ?string $defaultNamespace = null, ?string $defaultNamespaceFolder = null, array $extras = [], bool $debug = false)
    {
        $documentationParams = json_decode($_SERVER['WEB_API_DOCUMENTATION_PARAMS'] ?? '{}', true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception('Invalid JSON provided for documentation params', 400);
        }

        if (count($documentationParams) == 0 || count(array_diff(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'], array_keys($documentationParams))) > 0) {
            throw new Exception('You must specify documentation params', 400);
        }

        $params['apiMethod'] = 'get';
        parent::__construct($params, $extras, $debug);
        $this->getQueryParamRules = [
            ['field' => 'a', 'type' => 'string', 'required' => true, 'restrict_empty' => true, 'in_array' => $documentationParams['a']],
            ['field' => 'b', 'type' => 'string', 'required' => true, 'restrict_empty' => true, 'in_array' => $documentationParams['b']],
            ['field' => 'c', 'type' => 'string', 'required' => true, 'restrict_empty' => true, 'in_array' => $documentationParams['c']],
            ['field' => 'd', 'type' => 'string', 'required' => true, 'restrict_empty' => true, 'in_array' => $documentationParams['d']],
            ['field' => 'e', 'type' => 'string', 'required' => true, 'restrict_empty' => true, 'in_array' => $documentationParams['e']],
            ['field' => 'f', 'type' => 'string', 'required' => true, 'restrict_empty' => true, 'in_array' => $documentationParams['f']],
            ['field' => 'g', 'type' => 'string', 'required' => true, 'restrict_empty' => true, 'in_array' => $documentationParams['g']],
            ['field' => 'h', 'type' => 'string', 'required' => true, 'restrict_empty' => true, 'in_array' => $documentationParams['h']],
            ['field' => 'i', 'type' => 'string', 'required' => true, 'restrict_empty' => true, 'in_array' => $documentationParams['i']],
            ['field' => 'j', 'type' => 'string', 'required' => true, 'restrict_empty' => true, 'in_array' => $documentationParams['j']],
        ];
        $this->validator->generateSchema = true;
        $this->validator->exitOnSchemaGenerate = false;
        $randomString = bin2hex(random_bytes(16));
        $this->workSpaceId = "wrk_$randomString";
        $this->environmentId = "env_$randomString";
        $this->insomniaStructure = [
            '_type' => 'export',
            '__export_format' => 4,
            '__export_source' => 'insomnia.desktop.app:v2023.4.0',
            'resources' => [
                [
                    '_id' => $this->workSpaceId,
                    'parentId' => null,
                    'name' => $_SERVER['HTTP_HOST'] . ' API',
                    'scope' => 'collection',
                    '_type' => 'workspace'
                ],
                [
                    '_id' => $this->environmentId,
                    'parentId' => $this->workSpaceId,
                    'name' => 'Base Environment',
                    'data' => ['base_url' => $this->baseUrl, 'bearer_token' => 'abcdefg'],
                    'dataPropertyOrder' => ['&' => ['base_url', 'bearer_token']],
                    '_type' => 'environment'
                ]
            ]
        ];
        $this->defaultNamespace = ($defaultNamespace ?? $this->defaultNamespace);
        $this->defaultNamespaceFolder = ($defaultNamespaceFolder ?? $this->defaultNamespaceFolder);
        $this->params['is_documentation'] = true;
    }

    /**
     * @return void
     * @throws ReflectionException
     * @throws Exception
     */
    public function generateSchema(): void
    {
        $directoryArray = $this->getSchemas();
        header('Content-Type: text/html; charset=UTF-8'); ?>
        <!doctype html>
        <html lang="en">

        <head>
            <meta charset="utf-8">
            <title>API Structure</title>
            <style>
                body {
                    font-family: Arial, sans-serif;
                    margin: 0;
                    padding: 0;
                }

                #sidebar {
                    min-width: 300px; /* Set a minimum width */
                    height: 100vh;
                    overflow: auto;
                    float: left;
                    border-right: 1px solid #ccc;
                    padding: 10px;
                    background-color: #f9f9f9;
                }

                #sidebar ul {
                    padding-left: 10px;
                    list-style-type: none;
                }

                #content {
                    margin-left: 410px;
                    padding: 20px;
                    flex-wrap: wrap;
                }

                .request-item {
                    background-color: #f4f4f4;
                    padding: 10px;
                    margin: 10px;
                    border: 1px solid #e4e4e4;
                    border-radius: 5px;
                    flex: 0 0 calc(50% - 20px); /* Two columns layout */
                }

                .request-item h2 {
                    margin-top: 30px;
                }

                .request-item h3 {
                    margin-top: 30px;
                    color: #333;
                }

                .request-item p {
                    margin: 5px 0;
                    color: #666;
                }

                .sidebar2 {
                    display: flex;
                    justify-content: space-between; /* This will space the columns evenly */
                }

                .col {
                    flex: 1; /* This makes each column take equal space */
                    padding: 10px; /* Adds padding around the content of each column */
                    box-sizing: border-box; /* Includes padding in the width calculation */
                }
            </style>

        </head>

        <div id="sidebar"></div>
        <div id="content"></div>

        <script>
            // Sample directory array
            const directoryArray = <?php echo json_encode($directoryArray); ?>; // Convert PHP $directoryArray to JavaScript object
            const directoryMap = {};
            directoryArray.forEach(item => {
                const segments = item.url_hierarchy.split('/').filter(segment => segment.trim() !== '');
                let current = directoryMap;
                segments.forEach((segment, index) => {
                    if (!current[segment]) {
                        current[segment] = {};
                    }

                    if (index === segments.length - 1) {
                        current[segment] = item;
                    }

                    current = current[segment];
                });
            });

            // Render the directory structure
            function renderDirectory(directoryObj, parentElement, baseUrl = "", depth = 0) {
                // Split items into folders and clickables
                const folders = [];
                const clickables = [];
                for (const key in directoryObj) {
                    const item = directoryObj[key];
                    if (item && typeof item === 'object' && Object.keys(item).length > 0 && !item.method) {
                        folders.push({key, item});
                    } else {
                        clickables.push({key, item});
                    }
                }

                // Sort clickables alphabetically
                clickables.sort((a, b) => a.key.localeCompare(b.key));

                // Sort folders alphabetically
                folders.sort((a, b) => a.key.localeCompare(b.key));

                // Merge sorted clickables and folders
                const sortedItems = clickables.concat(folders);

                // Render sorted items
                const ul = document.createElement('ul');
                for (const {key, item} of sortedItems) {
                    const li = document.createElement('li');

                    // Create a container div to hold folder icon and text
                    const container = document.createElement('div');
                    container.classList.add('folder-container');

                    // Add a dropdown arrow for folders
                    if (item && typeof item === 'object' && Object.keys(item).length > 0 && !item.method) {
                        const arrow = document.createElement('span');
                        arrow.innerHTML = '&#9654;'; // Right-pointing small triangle
                        arrow.classList.add('dropdown-arrow');
                        arrow.style.cursor = 'pointer'; // Set cursor to pointer
                        arrow.addEventListener('click', function () {
                            const childUl = li.querySelector('ul');
                            if (childUl) {
                                childUl.style.display = childUl.style.display === 'none' ? 'block' : 'none';
                                arrow.textContent = childUl.style.display === 'none' ? '▶' : '▼';
                            }
                        });
                        container.appendChild(arrow);

                        // Add folder icon
                        const folderIcon = document.createElement('span');
                        folderIcon.innerHTML = '&#128193;'; // Folder icon
                        folderIcon.classList.add('folder-icon');
                        container.appendChild(folderIcon);
                    }

                    // Add text
                    const span = document.createElement('span');
                    span.textContent = key;
                    span.classList.add('folder-text');
                    container.appendChild(span);

                    li.appendChild(container);

                    if (item && typeof item === 'object' && Object.keys(item).length > 0 && !item.method) {
                        const childUl = document.createElement('ul');
                        childUl.style.display = 'none'; // Hide initially
                        renderDirectory(item, childUl, baseUrl + "/" + key, depth + 1);
                        li.appendChild(childUl);
                    } else {
                        // For clickable items
                        span.style.paddingLeft = `30px`; // Adjust indentation based on depth
                        span.style.cursor = 'pointer';
                        span.style.color = 'blue'; // Default color for clickable items
                        span.style.textDecoration = 'underline';
                        span.addEventListener('click', () => populateContent(item));
                    }

                    ul.appendChild(li);
                }
                parentElement.appendChild(ul);
            }

            function objectToString(obj) {
                if (typeof obj === 'object') {
                    return JSON.stringify(obj, null, 2);
                } else {
                    return obj.toString();
                }
            }

            // Function to convert any string to PascalCase
            function toPascalCase(str) {
                return str.match(/[a-z]+/gi).map((word) => word.charAt(0).toUpperCase() + word.substring(1).toLowerCase()).join('');
            }

            function createInterface(item, type) {
                let interfaces = '';
                const target = item[type];
                const namePart = item.name.match(/\(([^)]+)\)/);
                let formattedName = namePart ? toPascalCase(namePart[1]) : 'Example';

                if (target && target.example) {
                    interfaces += `interface ${formattedName}${type.charAt(0).toUpperCase() + type.slice(1)} {\n`;
                    interfaces += generateInterfaceProperties(target.example[0] || target.example, target.required, '');
                    interfaces += '\n}\n\n';
                }
                return interfaces;
            }

            function generateInterfaceProperties(example, requiredArray, prefix) {
                let propertiesCode = '';
                const requiredProps = parseRequiredFields(requiredArray);

                for (const key in example) {
                    const value = example[key];
                    const optional = requiredProps.has(key) ? '' : '?';
                    let type = '';

                    if (Array.isArray(value)) {
                        if (value.length > 0 && typeof value[0] === 'object') {
                            type = generateNestedInterface(value[0], requiredProps, prefix + capitalizeFirstLetter(key)) + '[]';
                        } else {
                            type = 'any[]';
                        }
                    } else if (typeof value === 'object' && value !== null && Object.keys(value).length > 0) {
                        type = generateNestedInterface(value, requiredProps, prefix + capitalizeFirstLetter(key));
                    } else {
                        type = determineType(value);
                    }

                    propertiesCode += `    ${key}${optional}: ${type};\n`;
                }

                return propertiesCode;
            }

            function generateNestedInterface(example, requiredProps, prefix) {
                let nestedCode = '{\n';
                for (const key in example) {
                    const value = example[key];
                    const optional = requiredProps.has(key) ? '' : '?';
                    let type = '';

                    if (Array.isArray(value)) {
                        if (value.length > 0 && typeof value[0] === 'object') {
                            type = generateNestedInterface(value[0], requiredProps, prefix + capitalizeFirstLetter(key)) + '[]';
                        } else {
                            type = 'any[]';
                        }
                    } else if (typeof value === 'object' && value !== null && Object.keys(value).length > 0) {
                        type = generateNestedInterface(value, requiredProps, prefix + capitalizeFirstLetter(key));
                    } else {
                        type = determineType(value);
                    }

                    nestedCode += `    ${key}${optional}: ${type};\n`;
                }
                nestedCode += '  }';
                return nestedCode;
            }

            function parseRequiredFields(requiredArray) {
                const requiredProps = new Set();
                requiredArray.forEach(field => {
                    // Flatten the structure for simplification
                    const cleanField = field.replace(/\*\.|\*/g, '').trim();
                    if (cleanField.length > 0) {
                        requiredProps.add(cleanField);
                    }
                });
                return requiredProps;
            }

            function capitalizeFirstLetter(string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            }

            function determineType(value) {
                if (typeof value === 'number') {
                    return 'number';
                } else if (typeof value === 'string') {
                    return 'string';
                } else if (value === null) {
                    return 'any';
                } else {
                    return 'any';  // Fallback for unhandled types
                }
            }

            function generateExampleApiCall(item, type, fullItem) {
                const target = item[type];
                if (!target || !target.example) {
                    return '// No example available';
                }
                let url = '';
                if (fullItem && fullItem.url) {
                    url = fullItem.url.replace('{{base_url}}', '');  // Replace the placeholder
                }
                // Use JSON.stringify for the data object and adjust the indentation
                const dataObjectStr = JSON.stringify(target.example, null, 4).split('\n').map(line => '        ' + line).join('\n').trimEnd();

                const apiCall = `
getItem() {
    return this.apiHelper.apiCall({
        'url': '${url}',
        'dataObject': ${dataObjectStr}
    });
}`;
                return apiCall;
            }

            function generateExampleAngularTemplate(item, type) {
                const target = item[type];
                if (!target || !target.example) {
                    return '<!-- No example available -->';
                }
                let templateCode = '';
                if (Array.isArray(target.example) && target.example.length > 0) {
                    templateCode += generateArrayTemplate(target.example, 'items');
                } else if (Array.isArray(target.example)) {
                    templateCode += '<div *ngFor="let item of items">\n  <!-- Empty Array -->\n</div>\n'; // Empty array case
                } else {
                    templateCode += generateObjectTemplate(target.example, 'item');
                }
                return templateCode;
            }

            function generateObjectTemplate(object, objectName) {
                let html = `<div *ngIf="${objectName}">\n`;
                Object.keys(object).forEach(key => {
                    if (Array.isArray(object[key])) {
                        html += `  <div *ngFor="let ${key} of ${objectName}.${key}">\n    <div>{{${key}}}</div>\n  </div>\n`;
                    } else if (typeof object[key] === 'object' && object[key] !== null) {
                        html += `  <div *ngIf="${objectName}.${key}">\n    <div>{{${objectName}.${key} | json}}</div>\n  </div>\n`;
                    } else {
                        html += `  <div>{{${objectName}.${key}}}</div>\n`;
                    }
                });
                html += '</div>\n';
                return html;
            }

            function generateArrayTemplate(array, arrayName) {
                let itemTemplate = generateObjectTemplate(array[0], 'item');
                return `<div *ngFor="let item of ${arrayName}">\n${itemTemplate}</div>\n`;
            }


            // Populate content on item click
            function populateContent(item) {
                console.log('item', item);
                if (item && item.method) {
                    const contentDiv = document.getElementById('content');
                    contentDiv.innerHTML = '';
                    const div = document.createElement('div');
                    let contentHtml = `
            <div class="request-item">
                <h1>${item.name}</h1>
                <h2>Method: ${item.method}</h2>
                <h2>URL:</h2>
                <p>${item.url}</p>
                <h2>Notes:</h2>
                <p>${item.notes}</p>
                <h2>Header:</h2>
                <p>${item.header}</p>
            </div>
            <div class="sidebar2">`;

                    contentHtml += `<div class="col"><h2>Request Body</h2>`;
                    if (item.request) {
                        contentHtml += createSection(item.request, 'request', item);
                    }
                    contentHtml += `</div>`;
                    contentHtml += `<div class="col"><h2>Request Query</h2>`;
                    if (item.query_params) {
                        contentHtml += createSection(item.query_params, 'query_params', item);
                    }
                    contentHtml += `</div>`;
                    contentHtml += `<div class="col"><h2>Response</h2>`;
                    if (item.response) {
                        contentHtml += createSection(item.response, 'response', item);
                    }
                    contentHtml += `</div>`;
                    contentHtml += `</div>`;

                    div.innerHTML = contentHtml;
                    contentDiv.appendChild(div);
                }
            }

            function createSection(section, type, item) {
                let html = '';
                if (section.example) {
                    html += `<div><h3>Example:</h3> <pre>${objectToString(section.example)}</pre></div>`;
                }
                if (section.required) {
                    html += `<div><h3>Required:</h3> <pre>${objectToString(section.required)}</pre></div>`;
                }
                if (section.properties) {
                    const interfaceText = createInterface({name: `(${type})`, [type]: section}, type);
                    html += `<div><h3>TypeScript Interface:</h3> <pre>${interfaceText}</pre></div>`;
                }
                if (section.example) {
                    if (type === 'request') {
                        html += `<div><h3>Example API Call:</h3><pre>${generateExampleApiCall({[type]: section}, type, item)}</pre></div>`;
                    } else if (type === 'query_params') {
                        html += `<div><h3>Example API Call:</h3><pre>${escapeHtml(generateExampleApiCall({[type]: section}, type))}</pre></div>`;
                    } else if (type === 'response') {
                        html += `<div><h3>Example Angular Template:</h3><pre>${escapeHtml(generateExampleAngularTemplate({[type]: section}, type))}</pre></div>`;
                    }
                }
                if (section.properties) {
                    html += `<div><h3>Properties:</h3> <pre>${objectToString(section.properties)}</pre></div>`;
                }
                return html;
            }

            function escapeHtml(text) {
                var map = {
                    '&': '&amp;',
                    '<': '&lt;',
                    '>': '&gt;',
                    '"': '&quot;',
                    "'": '&#039;'
                };
                return text.replace(/[&<>"']/g, function (m) {
                    return map[m];
                });
            }

            renderDirectory(directoryMap, document.getElementById('sidebar'));
        </script>
        </html>
        <?php
        exit();
    }

    /**
     * @return void
     * @throws ReflectionException
     * @throws Exception
     */
    public function generateInsomniaImportFile(): void
    {
        $this->getSchemas();
        header('Content-Disposition: attachment; filename="' . $this->workSpaceId . '.json"');
        header('Content-Type: text/html; charset=UTF-8');
        echo json_encode($this->insomniaStructure);

        exit();
    }

    /**
     * @return array
     * @throws ReflectionException
     * @throws Exception
     * @throws Exception
     */
    private function getSchemas(): array
    {
        $this->initValidator();
        $schemas = [];
        $recordPrefix = 'record_';
        $validMethods = array_map('strtolower', $this::$allowedMethods);
        $methods = array_merge($validMethods, array_map(fn($method) => "$recordPrefix$method", $validMethods));
        $webApiPath = DIRECTORY_SEPARATOR . $this->defaultNamespaceFolder . DIRECTORY_SEPARATOR . 'api-modules' . DIRECTORY_SEPARATOR;
        $dir = $_SERVER['DOCUMENT_ROOT'] . $webApiPath;
        $dirIterator = new RecursiveDirectoryIterator($dir);
        $iterator = new RecursiveIteratorIterator($dirIterator, RecursiveIteratorIterator::SELF_FIRST);
        $documentationPaths = [
            $dir . 'ZzzAutoDocumentation/GenerateSchema/GenerateSchema.php',
            $dir . 'ZzzAutoDocumentation/GenerateInsomniaImportFile/GenerateInsomniaImportFile.php',
        ];
        $files = $documentationPaths;
        array_map(function ($file) use (&$files) {
            if ($file->getExtension() === 'php') {
                $files[] = $file->getPathname();
            }
        }, iterator_to_array($iterator, false));
        foreach ($files as $file) {
            $filePath = str_replace('.php', '', $file);
            $class = str_replace(DIRECTORY_SEPARATOR, '\\', $this->defaultNamespace . DIRECTORY_SEPARATOR . explode($webApiPath, $filePath, 2)[1]);
            $classNameParts = explode('\\', $class);
            $className = end($classNameParts);
            $isDocumentation = in_array($file, $documentationPaths);
            if (class_exists($class) || $isDocumentation) {
                $callMethods = $isDocumentation ? ['get'] : array_intersect($this->getChildClassMethods($class), $methods);
                if (count($callMethods) > 0) {
                    $obj = $isDocumentation ? $this : new $class($this->params);
                    array_pop($classNameParts);
                    foreach ($callMethods as $callMethod) {
                        $parentId = $this->workSpaceId;
                        $urlPath = '';
                        $urlHierarchy = '';
                        foreach ($classNameParts as $k => $classNamePart) {
                            $urlPath .= '/' . $classNamePart;
                            $classNamePart = $this->misc->toKebabCase($classNamePart);
                            $urlHierarchy .= ($k == 0 ? '' : '/' . $classNamePart);
                            if (empty($folder_ids[$urlPath])) {
                                $folderId = 'fld_' . bin2hex(random_bytes(16));
                                $this->insomniaStructure['resources'][] = [
                                    '_id' => $folderId,
                                    'parentId' => $parentId,
                                    'name' => $classNamePart,
                                    '_type' => 'request_group'
                                ];
                                $this->folderIds[$urlPath] = $folderId;
                                $this->folderNames[$folderId] = $classNamePart;
                            }

                            $parentId = $this->folderIds[$urlPath];
                        }

                        $urlHierarchy .= '/' . $callMethod;
                        $_SERVER['REQUEST_METHOD'] = strtoupper(str_replace($recordPrefix, '', $callMethod));
                        if (!empty($obj::$isDeprecated)) {
                            $obj->isDocumentation = true;
                            if (str_starts_with($class, "{$this->defaultNamespace}\Plugins")) {
                                if ($className == 'Chat') {
                                    $pluginId = 'P100003';
                                } else {
                                    $pluginId = 'P100001';
                                }

                                $reflection = new ReflectionClass($obj);
                                $reqProperty = $reflection->getProperty('request');
                                $queryProperty = $reflection->getProperty('queryParams');
                                $reqProperty->setValue($obj, ['id' => $pluginId]);
                                $queryProperty->setValue($obj, ['id' => $pluginId]);
                            }

                            $obj->$callMethod();
                        }

                        $queryParamRules = $obj->{"{$callMethod}QueryParamRules"} ?? [];
                        $requestRules = $obj->{"{$callMethod}RequestRules"} ?? [];
                        $responseRules = $obj->{"{$callMethod}ResponseRules"} ?? [];
                        $request_name = ucfirst($callMethod);
                        $info = $this->validator->init($requestRules, $responseRules, $_SERVER['REQUEST_METHOD'], [], $queryParamRules);
                        $url = '{{base_url}}';
                        $apiModules = array_filter(explode(DIRECTORY_SEPARATOR, $this->misc->toKebabCase(trim($urlPath, '/'))));
                        $moduleResources = array_map(function ($element) {
                            return $this->misc->toKebabCase($element);
                        }, $this::$moduleResourceSegments);
                        if (count(array_intersect($apiModules, $moduleResources)) > 0) {
                            $totalApiModules = count($apiModules);
                            $moduleResourcesFound = false;
                            for ($i = 0; $i < $totalApiModules; $i++) {
                                $url .= '/' . $apiModules[$i];
                                if (!$moduleResourcesFound && in_array($apiModules[$i], $moduleResources)) {
                                    $moduleResourcesFound = true;
                                    continue;
                                }

                                if ($moduleResourcesFound) {
                                    if (!str_starts_with($callMethod, 'record_') && $i == $totalApiModules - 1) {
                                        continue;
                                    }

                                    $url .= '/{record_id}';
                                }
                            }

                            if (!$moduleResourcesFound) {
                                continue;
                            }
                        } else {
                            $url .= $this->misc->toKebabCase($urlPath);
                        }

                        if (!empty($parentId) && !empty($this->folderNames[$parentId])) {
                            $request_name = $request_name . ' (' . $this->folderNames[$parentId] . ')';
                        }

//                        $this->methodCallsHeaderRestrictions($obj, $callMethod, $class);
                        $info['notes'] = $obj->{"{$callMethod}Notes"} ?? '';
                        $info['header'] = $obj->{"{$callMethod}Header"} ?? '';
                        $info['name'] = $request_name;
                        $info['url'] = $url;
                        $info['url_hierarchy'] = $urlHierarchy;
                        $schemas[] = $info;
                        $tRequest = [
                            '_id' => 'req_' . bin2hex(random_bytes(16)),
                            'parentId' => $parentId, // You may want to adjust this to nest requests properly
                            'url' => $url,
                            'name' => $request_name,
                            'method' => $_SERVER['REQUEST_METHOD'],
                            'headers' => [['name' => 'Content-Type', 'value' => 'application/json']],
                            '_type' => 'request'
                        ];
                        if (in_array($_SERVER['REQUEST_METHOD'], ['POST', 'PUT', 'PATCH'])) {
                            $tRequest['body'] = ['mimeType' => 'application/json', 'text' => '{}'];
                        }

                        if (str_contains($urlPath, 'auth-person')) {
                            $tRequest['authentication'] = ['type' => 'bearer', 'token' => '{{bearer_token}}'];
                        }

                        $this->insomniaStructure['resources'][] = $tRequest;
                    }
                }
            }

        }

        return $schemas;
    }

    /**
     * This ignores parent class methods if they are not explicitly defined in the current class
     * @param $class
     * @return array
     */
    private function getChildClassMethods($class): array
    {
        $classReflection = new ReflectionClass($class);
        $classMethods = [];
        foreach ($classReflection->getMethods() as $method) {
            // Check if the method is declared in the current class (not inherited)
            if ($method->getDeclaringClass()->getName() === $class) {
                $classMethods[] = $method->getName();
            }
        }

        return $classMethods;
    }

    /**
     * @param $className
     * @param string $methodName
     * @param string $class
     * @return bool
     */
    private function methodCallsHeaderRestrictions($className, string $methodName, string $class): bool
    {
        if (method_exists($className, $methodName)) {
            $reflection = new ReflectionMethod($className, $methodName);
            $filename = $reflection->getFileName();
            $startLine = $reflection->getStartLine() - 1;
            $endLine = $reflection->getEndLine();
            $fileContent = file($filename);
            $methodCode = implode('', array_slice($fileContent, $startLine, $endLine - $startLine));
            if (str_contains($methodCode, '$this->validateHeaderDgId()')) {
                $header = $className->{"{$methodName}Header"};
                if (empty($header)) {
                    echo "$class ==> $methodName --> DG_ID empty\n\n\n";
                    return false;
                }
                if ($header !== 'DG_ID') {
                    echo "$class ==> $methodName --> DG_ID mismatch\n\n\n";
                    return false;
                }
                return true;
            } elseif (str_contains($methodCode, '$this->validateHeaderDbId()')) {
                $header = $className->{"{$methodName}Header"};
                if (empty($header)) {
                    echo "$class ==> $methodName --> D_ID empty\n\n\n";
                    return false;
                }
                if ($header !== 'D_ID') {
                    echo "$class ==> $methodName --> D_ID mismatch\n\n\n";
                    return false;
                }
                return true;
            }
        }

        echo "$class ==> $methodName --> invalidated\n\n\n";
        return false;
    }
}