<?php

namespace DataBackFlip;

use Closure;
use Exception;
use finfo;

class Misc
{
    public array $csvMimes = [
        'text/x-comma-separated-values',
        'text/comma-separated-values',
        'application/octet-stream',
        'application/vnd.ms-excel',
        'application/x-csv',
        'text/x-csv',
        'text/csv',
        'application/csv',
        'application/excel',
        'application/vnd.msexcel'
    ];

    /**
     * @param mixed $data
     * @param callable|null $callback
     * @param bool $filterDecodedArray - filters decoded null array eg {"key1":null,"key2":null} to {}
     * @param array $decodeExclusiveArrayKeys - field keys to be excluded from decoding
     * @return void
     */
    public function json_decode_recursively(mixed &$data, ?callable $callback = NULL, bool $filterDecodedArray = false, array $decodeExclusiveArrayKeys = []): void
    {
        if (is_array($data)) {
            array_walk($data, function (&$value, $key) use ($filterDecodedArray, $callback, $decodeExclusiveArrayKeys) {
                if (in_array($key, $decodeExclusiveArrayKeys)) {
                    return;
                }

                $this->json_decode_recursively($value, $callback, $filterDecodedArray, $decodeExclusiveArrayKeys);
            });
        } elseif (is_string($data)) {
            $val = json_decode($data, true, 512, JSON_BIGINT_AS_STRING);
            if (json_last_error() === JSON_ERROR_NONE) {
                if (is_array($val)) {
                    $this->json_decode_recursively($val, $callback, $filterDecodedArray, $decodeExclusiveArrayKeys);
                } elseif (is_float($val) && is_infinite($val)) {
                    $val = $data;
                }

                $data = (is_array($val) && $filterDecodedArray) ? array_filter($val) : $val;
            }
        }
    }

    /**
     * @param array $array
     * @param callable|null $callback
     * @param bool $filterNull
     * @return array
     */
    public function array_filter_recursive(array $array, ?callable $callback = NULL, bool $filterNull = false): array
    {
        foreach ($array as &$value) {
            if (is_array($value)) {
                $value = $this->array_filter_recursive($value, $callback, $filterNull);
            }
        }

        return $filterNull ? array_filter($array, function ($value) {
            return !is_null($value);
        }) : array_filter($array, $callback);
    }


    /**
     * @param array $data
     * @param array $keys
     * @return void
     * @throws Exception
     */
    public function unset_recursive(array &$data, array $keys): void
    {
        if (count($keys) > 0) {
            foreach ($data as $i => &$val) {
                foreach ($keys as $k) {
                    if (is_string($k)) {
                        $tKeys = explode('.', $k, 2);
                        if (!array_key_exists(1, $tKeys) && $tKeys[0] == $i) {
                            unset($data[$i]);
                        } elseif ($tKeys[0] == '*' || $tKeys[0] == $i) {
                            if (count($tKeys) == 2) {
                                if (is_array($val)) {
                                    $this->unset_recursive($val, [$tKeys[1]]);
                                }
                            } else {
                                unset($data[$i]);
                            }
                        }
                    } else {
                        throw new Exception('Keys should be strings optionally separated by a dot(.)', 400);
                    }
                }
            }
        }
    }

    /**
     * @param string $date
     * @param string $format
     * @param bool $throwException
     * @return string|null
     * @throws Exception
     */
    public function date_format(string $date, string $format = 'Y-m-d', bool $throwException = true): ?string
    {
        if (empty($date)) {
            return NULL;
        }

        $time = strtotime($date);
        if ($time) {
            return date($format, $time);
        }

        if ($throwException) {
            throw new Exception("Invalid date $date");
        }

        return NULL;
    }

    /**
     * @param int|null $year
     * @return array
     */
    public function get_year_quarters(?int $year = null): array
    {
        $year ??= date('Y');
        $quarters = [];
        $monthGap = 3;
        for ($startMonth = 1; $startMonth <= 12; $startMonth += $monthGap) {
            $quarterNumber = ceil($startMonth / $monthGap);
            $quarters[$quarterNumber] = [
                'quarter' => $quarterNumber,
                'start_date' => date('Y-m-d', strtotime("first day of $year-$startMonth")),
                'end_date' => date('Y-m-d', strtotime('last day of +2 months', strtotime("$year-$startMonth-01"))),
            ];
        }

        return $quarters;
    }

    /**
     * @param array $input
     * @return array
     */
    public function array_unique_multidimensional(array $input): array
    {
        $unique = array_unique(array_map('serialize', $input));

        return array_values(array_intersect_key($input, $unique));
    }

    /**
     * @param array $data
     * @param string|null $fileName
     * @param bool $download
     * @param array $fields
     * @return string|null
     * @throws Exception
     */
    public function exportCSV(array $data, ?string $fileName, bool $download = false, array $fields = []): ?string
    {
        $filePath = null;
        if (count($data) > 0) {
            $data = $this->replaceDataGroupKeysWithLabels($data, $fields);
            $fileName = empty($fileName) ? date('Y-m-d H:i:s') . '.csv' : $fileName;
            $this->setCSVData($data, $fileName, $download);
            $filePath = $fileName;
        }

        return $filePath;
    }

    /**
     * @param array $data
     * @param array $fields
     * @return array
     */
    public function replaceDataGroupKeysWithLabels(array $data, array $fields): array
    {
        if (empty($fields) || empty($data)) {
            return $data;
        }

        $replacements = [];
        foreach ($fields as $field) {
            if (isset($field['field_name']) && isset($field['label'])) {
                $replacements[] = [$field['field_name'], $field['label']];
            }
        }

        if (empty($replacements)) {
            return $data;
        }

        return array_map(function ($item) use ($replacements) {
            return $this->array_rename_keys($item, ...$replacements);
        }, $data);
    }

    /**
     * @param array $data
     * @param string $filename
     * @param bool $downlaod
     * @return string
     * @throws Exception
     */
    public function setCSVData(array $data, string $filename, bool $downlaod = false): string
    {
        $output = fopen(($downlaod ? 'php://output' : 'php://temp'), 'w') or throw new Exception("Can't open Temp File", 500);
        header('Content-Type:application/csv');
        header('Content-Disposition:attachment;filename=' . $filename);
        fputcsv($output, array_keys($data[0]));
        foreach ($data as $val) {
            foreach ($val as $k => $v) {
                if (is_array($v) || is_object($v)) {
                    $val[$k] = json_encode($v);
                }
            }

            fputcsv($output, $val);
        }

        if ($downlaod) {
            fclose($output) or throw new Exception("Can't close Temp File", 500);
            $file_contents = '';
        } else {
            rewind($output);
            $file_contents = stream_get_contents($output);
            fclose($output) or throw new Exception("Can't close Temp File", 500);
            if (empty($file_contents)) {
                throw new Exception('Error When streaming csv contents', 500);
            }
        }

        return $file_contents;
    }


    /**
     * Generate a more truly "random" alpha-numeric string.
     * @param int $length
     * @return string
     * @throws Exception
     */
    public function str_random(int $length = 16): string
    {
        return bin2hex(random_bytes($length / 2));
    }

    /**
     * @param array $file
     * @param int $limit
     * @return array
     * @throws Exception
     */
    public function csvPreviewLocal(array $file, int $limit = 0): array
    {
        if (empty($file['tmp_name'])) {
            throw new Exception('No file to read');
        }

        $fileInfo = new finfo(FILEINFO_MIME_TYPE);
        $fileType = $fileInfo->file($file['tmp_name']);

        if (!in_array($fileType, $this->csvMimes)) {
            throw new Exception('Invalid file type ' . $fileType, 400);
        }

        $data = [];
        if ($limit > 0) {
            $csvLines = array_slice(file($file['tmp_name'], FILE_SKIP_EMPTY_LINES), 0, $limit + 1);
        } else {
            $csvLines = file($file['tmp_name'], FILE_SKIP_EMPTY_LINES);
        }

        if (count($csvLines) > 0) {
            $header = str_getcsv($csvLines[0]);
            array_shift($csvLines);
            foreach ($csvLines as $i => $line) {
                $data[$i] = array_combine($header, str_getcsv($line));
                unset($data[$i]['']); // remove empty columns
            }
        }

        return array_filter($data);
    }

    /**
     * @param $array
     * @param ...$replacements
     * @return array
     */
    public function array_rename_keys($array, ...$replacements): array
    {
        $array = array_change_key_case($array);
        $keys = array_keys($array);
        foreach ($replacements as $replacement) {
            list($oldKey, $newKey) = $replacement;
            $index = array_search((is_string($oldKey) ? strtolower($oldKey) : $oldKey), $keys);
            if ($index !== false) {
                $keys[$index] = $newKey;
            }
        }

        return array_combine($keys, $array);
    }

    /**
     * Get an item from an array or object using "dot" notation.
     *
     * @param mixed $target
     * @param string|array|int|null $key
     * @param mixed $default
     * @return mixed
     */
    public function data_get(mixed $target, string|array|int|null $key, mixed $default = null): mixed
    {
        if (is_null($key)) {
            return $target;
        }

        $key = is_array($key) ? $key : explode('.', $key);
        foreach ($key as $i => $segment) {
            unset($key[$i]);
            if (is_null($segment)) {
                return $target;
            }

            if ($segment === '*') {
                if (is_iterable($target)) {
                    $target = (array)($target);
                } else {
                    return $this->value($default);
                }

                $result = [];
                foreach ($target as $item) {
                    $result[] = $this->data_get($item, $key);
                }

                return in_array('*', $key) ? $this->array_collapse($result) : $result;
            }

            if (is_array($target) && array_key_exists($segment, $target)) {
                $target = $target[$segment];
            } elseif (is_object($target) && isset($target->{$segment})) {
                $target = $target->{$segment};
            } else {
                return $this->value($default);
            }
        }

        return $target;
    }

    /**
     * @param mixed $target
     * @param string|array $key
     * @param mixed $value
     * @param bool $overwrite
     * @return mixed
     */
    public function data_set(mixed &$target, string|array $key, mixed $value, bool $overwrite = true): mixed
    {
        $segments = is_array($key) ? $key : explode('.', $key);
        $segment = array_shift($segments);
        if ($segment === '*') {
            if (!is_array($target) || empty($target)) {
                $target = [[]];
            }

            if ($segments) {
                foreach ($target as &$inner) {
                    $this->data_set($inner, $segments, $value, $overwrite);
                }
            } elseif ($overwrite) {
                foreach ($target as &$inner) {
                    $inner = $value;
                }
            }
        } elseif (is_array($target)) {
            if ($segments) {
                if (!array_key_exists($segment, $target)) {
                    $target[$segment] = [];
                }

                $this->data_set($target[$segment], $segments, $value, $overwrite);
            } elseif ($overwrite || !array_key_exists($segment, $target)) {
                $target[$segment] = $value;
            }
        } elseif (is_object($target)) {
            if ($segments) {
                if (!isset($target->{$segment})) {
                    $target->{$segment} = [];
                }

                $this->data_set($target->{$segment}, $segments, $value, $overwrite);
            } elseif ($overwrite || !isset($target->{$segment})) {
                $target->{$segment} = $value;
            }
        } else {
            $target = [];
            if ($segments) {
                $this->data_set($target[$segment], $segments, $value, $overwrite);
            } elseif ($overwrite) {
                $target[$segment] = $value;
            }
        }

        return $target;
    }

    /**
     * @param mixed $target
     * @param string|array $key
     * @return mixed
     */
    public function data_unset(mixed &$target, string|array $key): mixed
    {
        $segments = is_array($key) ? $key : explode('.', $key);
        if (($segment = array_shift($segments)) === '*') {
            if (is_array($target)) {
                if ($segments) {
                    $segment = array_shift($segments);
                    if (array_key_exists($segment, $target)) {
                        if (empty($segments)) {
                            unset($target[$segment]);
                        } else {
                            $this->data_unset($inner, $segments);
                        }
                    }
                } else {
                    foreach ($target as $k => &$inner) {
                        unset($target[$k]);
                    }
                }
            }
        } elseif (is_array($target)) {
            if ($segments) {
                if (array_key_exists($segment, $target)) {
                    $this->data_unset($target[$segment], $segments);
                }
            } elseif (array_key_exists($segment, $target)) {
                unset($target[$segment]);
            }
        } elseif (is_object($target)) {
            if ($segments) {
                if (isset($target->{$segment})) {
                    $this->data_unset($target->{$segment}, $segments);
                }
            } elseif (isset($target->{$segment})) {
                unset($target->{$segment});
            }
        }

        return $target;
    }


    /**
     * Collapses an array of arrays into a single array.
     *
     * @param array $array
     * @return array
     */
    public function array_collapse(array $array): array
    {
        $results = [];
        foreach ($array as $values) {
            if (is_iterable($values)) {
                $values = (array)$values;
            } elseif (!is_array($values)) {
                continue;
            }

            $results[] = $values;
        }

        return array_merge([], ...$results);
    }

    /**
     * Return the default value of the given value.
     *
     * @param mixed $value
     * @param mixed ...$args
     * @return mixed
     */
    public function value(mixed $value, mixed ...$args): mixed
    {
        return $value instanceof Closure ? $value(...$args) : $value;
    }

    /**
     * Return the values from a colum from all levels of the input array
     * @param array $haystack
     * @param string $needle
     * @return array
     */
    public function array_column_recursive(array $haystack, string $needle): array
    {
        $items = [];
        array_walk_recursive($haystack, function ($value, $key) use (&$items, $needle) {
            if ($key == $needle)
                $items[] = $value;
        });

        return $items;
    }

    /**
     * converts inner uppercase to lowercase- e.g. TestWebApi will be test-web-api
     * @param $string
     * @return string
     */
    public function toKebabCase($string): string
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', '$1-$2', $string));
    }

    /**
     * converts inner uppercase to lowercase_ e.g. TestWebApi will be test_web_api
     * @param $string
     * @return string
     */
    public function toSnakeCase($string): string
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $string));
    }

    /**
     * @param $string
     * @param bool $keepDot
     * @return string
     */
    public function sanitizeAndHyphenFormatString($string, bool $keepDot = false): string
    {
        $dot = $keepDot ? '.' : '';

        return strtolower(trim(preg_replace('/-+/', '-', preg_replace(["/[^\w\s$dot-]/", '/[ _]+/'], ['-', '-'], $string))));
    }

    /**
     * This function replaces non-word characters, whitespace and hyphens with underscores,
     * and ensures that multiple consecutive underscores are replaced with only one underscore.
     * @param string $string The input string to be sanitized and formatted.
     * @param string|null $prefix
     * @param bool $lowercase
     * @return string
     */
    public function sanitizeAndFormatString(string $string, ?string $prefix = null, bool $lowercase = false): string
    {
        $formattedString = $prefix . trim(preg_replace('/_+/', '_', preg_replace(['/[^\w\s-]/', '/[ -]+/'], ['_', '_'], $string)));

        return $lowercase ? strtolower($formattedString) : $formattedString;
    }

    /**
     * @param array $objData
     * @return array
     */
    public function filter_object(array $objData): array
    {
        if (array_sum(array_map('is_null', $objData)) === count($objData)) {
            $objData = [];
        }

        return $objData;
    }

    /**
     * @param array $arrData
     * @return array
     */
    public function filter_multi_array(array $arrData): array
    {
        return array_values(array_filter($arrData, function ($subArray) {
            return array_sum(array_map('is_null', $subArray)) < count($subArray);
        }));
    }

    /**
     * Replace the last occurrence of a given value in the string.
     * @param string $search
     * @param string $replace
     * @param string $subject
     * @return string
     */
    public function str_replace_last(string $search, string $replace, string $subject): string
    {
        if ($search === '') {
            return $subject;
        }

        $position = strrpos($subject, $search);
        if ($position !== false) {
            return substr_replace($subject, $replace, $position, strlen($search));
        }

        return $subject;
    }

    /**
     * @param array $array
     * @return mixed
     */
    public function array_first(array $array): mixed
    {
        if (empty($array)) {
            return null;
        }

        return $array[array_key_first($array)];
    }

    /**
     * @return string
     */
    public function get_base_url(): string
    {
        if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $protocol = 'https://';
        } else {
            $protocol = 'http://';
        }

        return $protocol . $_SERVER['SERVER_NAME'];
    }
}