<?php

namespace DataBackFlip;

use DateTime;
use Exception;
use ZipArchive;

class Validator
{
    public const array VARIABLE_TYPES = [
        'boolean' => 'boolean',
        'integer' => 'integer',
        'float' => 'double',
        'double' => 'double',
        'string' => 'string',
        'array' => 'array',
        'object' => 'array',
        'file' => 'file',
        'date' => 'string',
        'datetime' => 'string',
        'year' => 'string',
        'time' => 'string',
        'line_separated_string' => 'array',
        'json_object' => 'array',
        'json_array' => 'array',
        'comma_separated_string' => 'array',
    ];

    public const array PARSABLE_TYPES = [
        'boolean',
        'integer',
        'double',
        'array',
    ];
    public const array PARSABLE_TYPE_KEYS = [
        'line_separated_string',
        'comma_separated_string',
        'json_object',
        'json_array'
    ];

    public array $validatorDefaultOptions = ['set_null_fields' => FALSE];
    public const array PERMITTED_OPTIONS = [
        'debug',
        'generateSchema',
        'exitOnSchemaGenerate',
        'skipRequestValidation',
        'skipResponseValidation',
        'parseRequestTypes',
        'parseResponseTypes',
        'field_prefix'
    ];

    public bool $generateSchema = FALSE;
    public bool $exitOnSchemaGenerate = TRUE;
    public array $requestRules = [];
    public array $responseRules = [];
    public array $queryParamRules = [];
    protected bool $debug = FALSE;
    private Misc $misc;
    private ?string $requestMethod = NULL;
    private bool $skipRequestValidation = FALSE;
    private bool $skipResponseValidation = FALSE;
    private bool $parseRequestTypes = TRUE; // set when you want to convert request values to respective types
    private bool $parseResponseTypes = TRUE; // set when you want to convert response values to respective types

    /**
     * @throws Exception
     */
    public function __construct(bool $debug = FALSE, $options = [])
    {
        $this->debug = $debug;
        $this->misc = new Misc();
        if (!empty($options)) {
            foreach ($options as $key => $value) {
                if (in_array($key, self::PERMITTED_OPTIONS)) {
                    $this->{$key} = $value;
                } else {
                    throw new Exception('Invalid option: ' . $key, 405);
                }
            }
        }
    }

    /*
        PUBLIC METHODS
        ############################################################################
        #############################################################################
        ############################################################################
        #############################################################################
    */

    /**
     * @throws Exception
     */
    public function getOption($option)
    {
        if (in_array($option, self::PERMITTED_OPTIONS)) {
            return $this->{$option};
        } else {
            throw new Exception("Option '$option' is not permitted", 405);
        }
    }

    /**
     * @param array $requestRules - Request Rules
     * @param array $responseRules - Response rules
     * @param string|NULL $requestMethod - The request method
     * @param array $options - Extra options
     * @param array $queryParamRules
     * @return array
     * @throws Exception
     */
    public function init(array $requestRules = [], array $responseRules = [], ?string $requestMethod = NULL, array $options = [], array $queryParamRules = []): array
    {
        $this->requestRules = $requestRules;
        $this->queryParamRules = $queryParamRules;
        $this->responseRules = $responseRules;
        $this->requestMethod = $requestMethod;
        $response = ['request' => NULL, 'response' => NULL, 'method' => $requestMethod];
        if (!empty($options)) {
            foreach ($options as $key => $value) {
                if (in_array($key, self::PERMITTED_OPTIONS)) {
                    $this->{$key} = $value;
                } else {
                    throw new Exception('Invalid option: ' . $key, 405);
                }
            }
        }

        if ($this->generateSchema) {
            $options['generate_schema'] = TRUE;
            if (!is_null($requestRules) && count($requestRules) > 0) {
                $response['request'] = $this->validate([], NULL, $requestRules, $options);
            }

            if (!is_null($queryParamRules) && count($queryParamRules) > 0) {
                $response['query_params'] = $this->validate([], NULL, $queryParamRules, $options);
            }

            if (!is_null($responseRules) && count($responseRules) > 0) {
                $response['response'] = $this->validate([], NULL, $responseRules, $options);
            }

            if ($this->exitOnSchemaGenerate) {
                header('Content-Type: application/json');
                echo json_encode([
                    'success' => true,
                    'info' => $response,
                ]);
                exit;
            }
        }

        return $response;
    }

    /**
     * @param array $data
     * @param array|null $options - Request Extra options
     * @param array $queryParams
     * @return array
     * @throws Exception
     */
    public function validateRequest(array $data = [], ?array $options = null, array $queryParams = []): array
    {
        if ($this->skipRequestValidation) {
            return $data;
        }
        $options ??= $this->validatorDefaultOptions;
        $options['parse_type'] = $this->parseRequestTypes;
        $data = $this->validate($data, $this->requestMethod, $this->requestRules, $options);
        $qData = $this->validate($queryParams, $this->requestMethod, $this->queryParamRules, $options);
        foreach ($qData as $key => $req) {
            if (isset($data[$key])) {
                throw new Exception("Query parameter $key has same name as Body parameter", 400);
            }

            $data[$key] = $req;
        }

        return $data;
    }

    /**
     * @param array $data
     * @param array $options - Response Extra options
     * @return array
     * @throws Exception
     */
    public function validateResponse(array $data = [], array $options = []): array
    {
        if ($this->skipResponseValidation) {
            return $data;
        }

        $options['parse_type'] = $this->parseResponseTypes;

        return $this->validate($data, NULL, $this->responseRules, $options);
    }

    /**
     * @param array $data
     * @param array $options - Response Extra options
     * @return array
     * @throws Exception
     */
    public function validateResponseArray(array $data = [], array $options = []): array
    {
        return $this->validateResponse($data, $options)[0] ?? [];
    }


    /**
     * @param array $data - The data to be validated
     * @param string|NULL $requestMethod - The request method (only used with Request)
     * @param array $rules - The rules to validate the data against
     * @param array $options - Extra options
     * @param string $fieldPrefix
     * @return array
     * @throws Exception
     */
    private function validate(array $data = [], ?string $requestMethod = NULL, array $rules = [], array $options = [], string $fieldPrefix = ''): array
    {
        $response = [];
        if (isset($options['generate_schema']) && is_bool($options['generate_schema']) && $options['generate_schema']) {
            $response = $this->generate_json_schema($rules, $options, $fieldPrefix);
        } else {
            if (!is_null($requestMethod) && $_SERVER['REQUEST_METHOD'] != strtoupper($requestMethod)) {
                throw new Exception('Method Not Allowed', 405);
            }

            foreach ($rules as $rule) {
                if (!isset($rule['field'])) {
                    throw new Exception("validator \"field\"" . ' is not provided', 400);
                }

                if ($fieldPrefix == '0.0.') {
                    $fieldPrefix = '0.';
                }

                if (array_key_exists('default', $rule) && !array_key_exists($rule['field'], $data)) {
                    $data[$rule['field']] = $rule['default'];
                }

                // If the field under validation is not empty, all other specified pipe-separated fields must be empty.
                if (!empty($rule['prohibits']) && is_string($rule['prohibits']) && !empty($data[$rule['field']])) {
                    $fields = explode('|', $rule['prohibits']);
                    $nonEmptyFields = array_filter($fields, function ($field) use ($data) {
                        return !empty($data[$field]);
                    });

                    if (!empty($nonEmptyFields)) {
                        throw new Exception("{$rule['field']} is mutually exclusive with " . implode(', ', $nonEmptyFields), 400);
                    }
                }

                //The field under validation is should be empty if the other field is equal to given pipe-separated values.
                if (!empty($rule['prohibited_if']) && is_string($rule['prohibited_if'])) {
                    $prohibited = explode('=', $rule['prohibited_if'], 2);
                    list($prohibitedKey, $prohibitedValue) = $prohibited;
                    $prohibitedValues = explode('|', $prohibitedValue);
                    if (isset($data[$rule['field']]) && in_array($data[$prohibitedKey], $prohibitedValues)) {
                        throw new Exception("{$rule['field']} is prohibited when $prohibitedKey is {$data[$prohibitedKey]}", 400);
                    }
                }

                //The field under validation only accepts a value if the other field is equal to given pipe-separated values.
                if (!empty($rule['present_if']) && is_string($rule['present_if'])) {
                    $present = explode('=', $rule['present_if'], 2);
                    list($presentKey, $presentValue) = $present;
                    $presentValues = explode('|', $presentValue);
                    if (isset($data[$rule['field']]) && !in_array($data[$presentKey], $presentValues)) {
                        throw new Exception("{$rule['field']} is prohibited when $presentKey is {$data[$presentKey]}", 400);
                    }
                }

                //The field under validation is required if the other field is equal to given value.
                if (!empty($rule['required_if']) && is_string($rule['required_if'])) {
                    $required = explode('=', $rule['required_if'], 2);
                    list($requiredKey, $requiredValue) = $required;
                    if (count($required) == 2 && (isset($data[$requiredKey]) && $data[$requiredKey] == $requiredValue)) {
                        $rule['required'] = true;
                    }
                }

                if (!empty($rule['required_if'])) {// The field under validation is required only when any of the other specified pipe-separated fields meets the given value criteria.
                    $requiredIfKey = 'required_if';
                    $breakTrue = true;
                    $breakFalse = false;
                } elseif (!empty($rule['required_if_all'])) { // The field under validation is required only when all the other specified pipe-separated fields meet the given value criteria.
                    $requiredIfKey = 'required_if_all';
                    $breakTrue = false;
                    $breakFalse = true;
                } else {
                    $requiredIfKey = null;
                    $breakTrue = null;
                    $breakFalse = null;
                }

                if (!empty($requiredIfKey) && is_string($rule[$requiredIfKey])) {
                    $conditions = explode('|', $rule[$requiredIfKey]);
                    $trueConditions = 0;
                    $operators = [
                        '>=' => function ($fieldValue, $compareValue) {
                            return $fieldValue >= $compareValue;
                        },
                        '<=' => function ($fieldValue, $compareValue) {
                            return $fieldValue <= $compareValue;
                        },
                        '>' => function ($fieldValue, $compareValue) {
                            return $fieldValue > $compareValue;
                        },
                        '<' => function ($fieldValue, $compareValue) {
                            return $fieldValue < $compareValue;
                        },
                        '!=' => function ($fieldValue, $compareValue) {
                            return $fieldValue != $compareValue;
                        },
                        '=' => function ($fieldValue, $compareValue) {
                            return $fieldValue == $compareValue;
                        }
                    ];

                    foreach ($conditions as $condition) {
                        foreach ($operators as $operator => $comparisonFn) {
                            if (str_contains($condition, $operator)) {
                                list($requiredKey, $requiredValue) = explode($operator, $condition, 2);
                                if (isset($data[$requiredKey]) && $comparisonFn($data[$requiredKey], $requiredValue)) {
                                    $trueConditions++;
                                    if ($breakTrue) {
                                        $conditions = [$condition];
                                        break 2;
                                    }
                                } elseif ($breakFalse) {
                                    break 2;
                                }
                            }
                            break;
                        }
                    }

                    if ($trueConditions == count($conditions)) {
                        $rule['required'] = true;
                    }
                }

                //The field under validation is required only when any of the other specified pipe-separated fields are empty or not present.
                if (!empty($rule['required_without']) && is_string($rule['required_without'])) {
                    $requiredWithout = array_filter(explode('|', $rule['required_without']), function ($field) use ($data) {
                        return empty($data[$field]);
                    });

                    $rule['required'] = !empty($requiredWithout);
                }

                //The field under validation is required only when all the other specified pipe-separated fields are empty or not present.
                if (!empty($rule['required_without_all']) && is_string($rule['required_without_all'])) {
                    $fields = explode('|', $rule['required_without_all']);
                    $missingFields = array_filter($fields, function ($field) use ($data) {
                        return empty($data[$field]);
                    });

                    $rule['required'] = count($missingFields) === count($fields);
                }

                if (isset($rule['type']) && in_array($rule['type'], ['array', 'object']) && isset($data[$rule['field']]) && is_array($data[$rule['field']])) {
                    if (empty($rule['format']) || $rule['format'] !== 'multi_array') {
                        $data[$rule['field']] = array_filter($data[$rule['field']], function ($v) {
                            return !is_null($v);
                        });
                    } else {
                        $data[$rule['field']] = $this->misc->filter_object($data[$rule['field']]);
                    }
                }

                if (isset($rule['required']) && $rule['required']) {
                    if (isset($rule['type']) && $rule['type'] == 'file') {
                        if (!isset($_FILES[$rule['field']])) {
                            throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is not provided', 400);
                        }
                    } elseif (!isset($data[$rule['field']])) {
                        throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is not provided', 400);
                    }
                }

                if ($rule['field'] == 0) {
                    $data = [$data];
                }

                if (isset($data[$rule['field']])) {
                    if (isset($rule['type'])) {
                        $rule['type'] = strtolower($rule['type']);
                        if (!in_array($rule['type'], array_keys(self::VARIABLE_TYPES))) {
                            throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is invalid', 400);
                        }

                        if ($rule['type'] == 'string' && is_numeric($data[$rule['field']])) {
                            settype($data[$rule['field']], 'string'); // take care of varchar
                        }

                        if (isset($options['parse_type']) && is_bool($options['parse_type']) && $options['parse_type']) {
                            if (in_array($rule['type'], self::PARSABLE_TYPE_KEYS)) {
                                $this->parseTypes($data[$rule['field']], $rule, $rule['type']);
                            } elseif (in_array(self::VARIABLE_TYPES[$rule['type']], self::PARSABLE_TYPES)) {
                                $this->parseTypes($data[$rule['field']], $rule, self::VARIABLE_TYPES[$rule['type']]);
                            }
                        }

                        $expectedType = gettype($data[$rule['field']] ?? NULL);
                        if (ctype_alpha($expectedType) && preg_match('/^[aeiou]/i', $expectedType)) {
                            $expectedTypeG = 'an';
                        } else {
                            $expectedTypeG = 'a';
                        }

                        if (ctype_alpha($rule['type']) && preg_match('/^[aeiou]/i', $rule['type'])) {
                            $typeG = 'an';
                        } else {
                            $typeG = 'a';
                        }

                        $typeError = "\"" . $fieldPrefix . $rule['field'] . "\"" .
                            ' is ' . $expectedTypeG . " \"" . $expectedType . "\"" .
                            ' and should be ' . $typeG . " \"" . $rule['type'] . "\"";
                        if (self::VARIABLE_TYPES[$rule['type']] !== $expectedType && $rule['type'] !== $expectedType) {
                            throw new Exception($typeError, 400);
                        } elseif ($rule['type'] == 'array' && count($data[$rule['field']]) > 0 && count(array_filter(array_keys($data[$rule['field']]), 'is_int')) !== count($data[$rule['field']])) {
                            throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\" is an \"object\" and should be an " . "\"" . $rule['type'] . "\"", 400);
                        } elseif ($rule['type'] == 'object') {
                            $tData = (array)$data[$rule['field']];
                            if (count($tData) > 0 && count(array_filter(array_keys($tData), 'is_string')) == 0 && (empty($rule['integer_keys']) || !is_bool($rule['integer_keys']))) {
                                throw new Exception($typeError, 400);
                            }
                        } elseif (in_array($rule['type'], ['date', 'datetime', 'year', 'time'])) {
                            $format = match ($rule['type']) {
                                'date' => 'Y-m-d',
                                'datetime' => 'Y-m-d H:i:s',
                                'year' => 'Y',
                                'time' => 'H:i:s',
                            };
                            if (in_array($rule['type'], ['datetime', 'time']) && str_contains($data[$rule['field']], '.')) {
                                $data[$rule['field']] = preg_replace('/\..*/', '', $data[$rule['field']]);
                            }

                            $dates = $this->validateDatetime($data[$rule['field']], $format);
                            if (empty($dates)) {
                                throw new Exception($typeError, 400);
                            }

                            $data[$rule['field']] = $dates;
                        }

                        // force boolean fields to be quoted since some apis rejects 1 & 0 for boolean
                        if (isset($options['quote_boolean']) && is_bool($options['quote_boolean']) && $options['quote_boolean'] && $expectedType == 'boolean') {
                            if ($data[$rule['field']]) {
                                $data[$rule['field']] = 'TRUE';
                            } else {
                                $data[$rule['field']] = 'FALSE';
                            }
                        }
                    }

                    if (isset($rule['in_array'])) {
                        if (is_array($data[$rule['field']])) {
                            if (count(array_intersect(array_map('strtolower', $data[$rule['field']]), array_map('strtolower', (array)$rule['in_array']))) != count($data[$rule['field']])) {
                                throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is invalid', 400);
                            }
                        } elseif (!in_array(strtolower($data[$rule['field']]), array_map('strtolower', (array)$rule['in_array']))) {
                            throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is invalid', 400);
                        }
                    }

                    // case-insensitive search
                    if (isset($rule['in_arrayi'])) {
                        $matches = false;
                        foreach ((array)$rule['in_arrayi'] as $pattern) {
                            $pattern = str_replace('/', '\/', preg_quote($pattern));
                            if (preg_match("/$pattern/i", $data[$rule['field']])) {
                                $matches = true;
                                break;
                            }
                        }

                        if (!$matches) {
                            throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is invalid', 400);
                        }
                    }

                    if (is_array($data[$rule['field']])) {
                        if (isset($rule['restrict_empty']) && $rule['restrict_empty'] && empty($data[$rule['field']])) {
                            throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' cannot be an empty array', 400);
                        }

                        $count = count($data[$rule['field']]);
                        if (isset($rule['min']) && $count < $rule['min']) {
                            throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . " minimum size is \"" . $rule['min'] . "\"", 400);
                        }

                        if (isset($rule['format']) && $rule['type'] == 'array') {
                            if ($rule['format'] === 'multi_array' && $count > 0 && count(array_filter($data[$rule['field']], 'is_array')) == 0) {
                                throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is not a multidimensional array', 400);
                            }

                            if ($rule['format'] === 'integer' && $count > 0 && $count !== count(array_filter($data[$rule['field']], 'is_numeric'))) {
                                throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is not an array of integers', 400);
                            }

                            if ($rule['format'] === 'string' && $count > 0 && $count !== count(array_filter($data[$rule['field']], 'is_string'))) {
                                throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is not an array of string', 400);
                            }

                            if ($rule['format'] === 'email' && $count > 0 && $count !== count(array_filter($data[$rule['field']], function ($email) {
                                    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
                                }))) {
                                throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is not an array of emails', 400);
                            }

                            if ($rule['format'] === 'url' && $count > 0 && $count !== count(array_filter($data[$rule['field']], function ($url) {
                                    return filter_var($url, FILTER_VALIDATE_URL) !== false;
                                }))) {
                                throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is not an array of URLs', 400);
                            }
                        }

                        if (isset($rule['is_unique']) && is_bool($rule['is_unique']) && $rule['is_unique'] && count($this->misc->array_unique_multidimensional($data[$rule['field']])) != $count) {
                            throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' must contain unique values', 400);
                        }
                    }

                    if (is_string($data[$rule['field']])) {
                        if (isset($rule['restrict_empty']) && $rule['restrict_empty'] && $data[$rule['field']] == '') {
                            throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' cannot be an empty string', 400);
                        }

                        if (isset($rule['min']) && strlen($data[$rule['field']]) < $rule['min']) {
                            throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . " minimum size is \"" . $rule['min'] . "\"", 400);
                        }
                    }

                    if (is_numeric($data[$rule['field']])) {
                        if (isset($rule['restrict_zero']) && $rule['restrict_zero'] && empty($data[$rule['field']])) {
                            throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' cannot be zero', 400);
                        }
                    }

                    if (isset($rule['format'])) {
                        if ($rule['format'] === 'email' && is_string($data[$rule['field']])) {
                            if (!filter_var($data[$rule['field']], FILTER_VALIDATE_EMAIL)) {
                                throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is not an email address', 400);
                            }
                        }

                        if ($rule['format'] === 'url' && is_string($data[$rule['field']])) {
                            if (!filter_var($data[$rule['field']], FILTER_VALIDATE_URL)) {
                                throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is not a valid URL', 400);
                            }
                        }
                    }

                    $response[$rule['field']] = $data[$rule['field']];

                    //recurse if nested
                    if (isset($rule['nested']) && is_array($data[$rule['field']])) {
                        $response[$rule['field']] = [];
                        if (!empty($data[$rule['field']])) {
                            if (isset($rule['type']) && $rule['type'] == 'object') {
                                $response[$rule['field']] = $this->validate($data[$rule['field']], NULL, $rule['nested'], $options, $fieldPrefix . $rule['field'] . '.');
                            } else {
                                foreach ($data[$rule['field']] as $k => $field) {
                                    $response[$rule['field']][] = $this->validate((array)$field, NULL, (array)$rule['nested'], $options, $fieldPrefix . $rule['field'] . '.' . $k . '.');
                                }
                            }
                        }
                    }
                } elseif (isset($rule['type']) && $rule['type'] == 'file') {
                    if (isset($_FILES[$rule['field']])) {
                        if (isset($rule['restrict_empty']) && $rule['restrict_empty']) {
                            if (!empty($rule['format']) && $rule['format'] == 'array') {
                                if (!is_array($_FILES[$rule['field']]['size']) || !is_array($_FILES[$rule['field']]['error'])) {
                                    throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is not an array of files', 400);
                                }

                                foreach ($_FILES[$rule['field']]['size'] as $v) {
                                    if ($v == 0) {
                                        throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is not provided', 400);
                                    }
                                }

                                foreach ($_FILES[$rule['field']]['error'] as $v) {
                                    if ($v != 0) {
                                        throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is not provided', 400);
                                    }
                                }
                            } elseif ($_FILES[$rule['field']]['size'] == 0 || $_FILES[$rule['field']]['error'] != 0) {
                                throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is not provided', 400);
                            } elseif (!empty($rule['format']) && $rule['format'] == 'zip') {
                                $zip = new ZipArchive();
                                if ($zip->open($_FILES[$rule['field']]['tmp_name']) === true) {
                                    $zip->close();
                                } else {
                                    throw new Exception("\"" . $fieldPrefix . $rule['field'] . "\"" . ' is not a zip file', 400);
                                }
                            }
                        }

                        $response[$rule['field']] = $_FILES[$rule['field']];
                    }
                } elseif (array_key_exists($rule['field'], $data)) {
                    if (isset($rule['type']) && in_array($rule['type'], ['array', 'object'])) {
                        $response[$rule['field']] = [];
                    } else {
                        $response[$rule['field']] = $data[$rule['field']];
                    }
                } elseif ((!isset($options['set_null_fields']) || !is_bool($options['set_null_fields'])) || $options['set_null_fields']) {
                    $response[$rule['field']] = null;
                }
            }
        }

        return $response;
    }

    /*
        PRIVATE METHODS
        ############################################################################
        #############################################################################
        ############################################################################
        #############################################################################
    */
    /**
     * @param $rules
     * @param array $options
     * @param string $fieldPrefix
     * @param array $required
     * @return array
     * @throws Exception
     */
    private function generate_json_schema($rules, array $options = [], string $fieldPrefix = '', array &$required = []): array
    {
        $response = [];
        foreach ($rules as $rule) {
            if (!isset($rule['field'])) {
                throw new Exception("validator \"field\"" . ' is not provided', 400);
            }

            if ($fieldPrefix == '0.*.') {
                $fieldPrefix = '*.';
            }

            $tField = $fieldPrefix . $rule['field'];
            if (str_contains($tField, '0.')) {
                $tField = str_replace('0.', '*.', $tField);
            } elseif (str_contains($tField, '.0')) {
                $tField = str_replace('.0', '.*', $tField);
            } elseif ($tField == '0') {
                $tField = '*';
            }

            $default = $rule['default'] ?? NULL;
            if (isset($rule['type'])) {
                $type = strtolower(self::VARIABLE_TYPES[$rule['type']]);
                if (!in_array($type, array_keys(self::VARIABLE_TYPES))) {
                    throw new Exception("Type \"$type\" in \"" . $tField . "\"" . ' is invalid', 400);
                }

                if ($type == 'file') {
                    $default = 'file';
                } else {
                    settype($default, $type);
                }
            } else {
                $type = 'mixed';
            }

            $response['example'][$rule['field']] = $default;
            if (isset($rule['required']) && $rule['required']) {
                $required[] = $tField;
            }

            $response['required'] = $required;
            $response['properties'][$rule['field']] = [
                'type' => $type,
                'format' => $rule['format'] ?? NULL,
                'required' => $rule['required'] ?? false,
                'default' => $default,
                'enum' => $rule['in_array'] ?? $rule['in_arrayi'] ?? NULL,
                'properties' => NULL
            ];

            if (isset($rule['nested']) && is_array($rule['nested']) && self::VARIABLE_TYPES[$rule['type']] == 'array') {
                if ($rule['type'] == 'object') {
                    $FieldSuffix = '.';
                } else {
                    $FieldSuffix = '.*.';
                }

                $tResponse = $this->generate_json_schema($rule['nested'], $options, $fieldPrefix . $rule['field'] . $FieldSuffix, $required);
                $response['required'] = $response['required'] ?? [] + $tResponse['required'];
                foreach ($tResponse as $k => $v) {
                    if ($k != 'required') {
                        $v = (array)$v;
                        if ($k == 'properties') {
                            $val = array_merge((array)$response[$k][$rule['field']][$k] ?? [], $v);
                            if ($FieldSuffix == '.') {
                                $response[$k][$rule['field']][$k] = $val;
                            } else {
                                $response[$k][$rule['field']][$k][0] = $val;
                            }
                        } else {
                            $val = array_merge((array)$response[$k][$rule['field']] ?? [], $v);
                            if ($FieldSuffix == '.') {
                                $response[$k][$rule['field']] = $val;
                            } else {
                                $response[$k][$rule['field']][0] = $val;
                            }
                        }
                    }
                }
            }

            if ($rule['field'] == 0) {
                $response['example'] = $response['example'][0];
            }
        }

        return $response;
    }

    /**
     * @param mixed $value
     * @param array $rule
     * @param string $expectedType
     * @return void
     */
    private function parseTypes(mixed &$value, array &$rule, string $expectedType): void
    {
        switch (true) {
            case $expectedType == 'boolean' :
                if ($value === 'false') {
                    $value = 0;
                }

                if ((filter_var($value, FILTER_VALIDATE_BOOLEAN) || in_array($value, [0, 1, '0', '1']))) {
                    settype($value, 'boolean');
                }
                break;
            case $expectedType == 'integer' && is_numeric($value):
                settype($value, 'integer');
                break;
            case $expectedType == 'double' && (filter_var($value, FILTER_VALIDATE_FLOAT) || in_array($value, [0, '0'])):
                settype($value, 'float');
                break;
            case $expectedType == 'line_separated_string' && is_string($value):
                $value = array_map('trim', explode("\n", $value));
                settype($value, 'array');
                break;
            case $expectedType == 'comma_separated_string' && is_string($value):
                $value = array_map('trim', explode(',', $value));
                settype($value, 'array');
                break;
            case $expectedType == 'json_object' && is_string($value):
                $rule['type'] = 'object';
                $this->misc->json_decode_recursively($value);
                settype($value, 'array');
                break;
            case $expectedType == 'json_array' && is_string($value):
                $rule['type'] = 'array';
                $this->misc->json_decode_recursively($value);
                settype($value, 'array');
                break;
        }
    }

    /**
     * @param string $date
     * @param string $format
     * @return string|null
     * @throws Exception
     */
    private function validateDatetime(string $date, string $format = 'Y-m-d H:i:s'): ?string
    {
        $dateTime = DateTime::createFromFormat($format, $date);
        if ($dateTime) {
            return $this->misc->date_format($date, $format, false);
        }

        return null;
    }
}