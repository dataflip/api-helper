<?php

namespace DataBackFlip;

use Exception;
use Throwable;

/**
 *
 */
trait DynamicModuleResource
{
    private mixed $dynamicResourceSystemConn;
    private mixed $dynamicResourceConfigConn;
    private mixed $dynamicResourceConn;
    private string $dynamicResourceTablePrefix = '';
    public array $dynamicResourceMainTableInfo = [];
    private array $dynamicResourcePivotInfo = [];
    protected bool $dynamicResourceMethodDefault = true;
    protected bool $dynamicResourceDontExecuteQuery = false;
    protected array $dynamicResourceRelationshipTypes = [];
    public array $dynamicResourceGetRequestRules = [];
    public array $dynamicResourcePostQueryParamRules = [];
    public array $dynamicResourcePatchQueryParamRules = [];
    public array $dynamicResourceDeleteQueryParamRules = [];
    public array $dynamicResourceRecord_getRequestRules = [];
    public array $dynamicResourceRecord_postQueryParamRules = [];
    public array $dynamicResourceRecord_patchQueryParamRules = [];
    public array $dynamicResourceRecord_deleteQueryParamRules = [];
    public array $dynamicResourcePostRequestRules = [
        ['field' => 'data', 'required' => true],
        ['field' => 'options', 'type' => 'object']
    ];
    public array $dynamicResourceGetQueryParamRules = [
        ['field' => 'params', 'type' => 'json_object',
            'nested' => [
                ['field' => 'include_default_fields', 'type' => 'boolean'],
                ['field' => 'all_fields', 'type' => 'boolean'],
                ['field' => 'field_ids', 'type' => 'array', 'format' => 'integer'],
                ['field' => 'exclude_field_ids', 'type' => 'array', 'format' => 'integer'],
                ['field' => 'limit', 'type' => 'integer'],
                ['field' => 'offset', 'type' => 'integer'],
                ['field' => 'filters', 'type' => 'object'],
                ['field' => 'query_params', 'type' => 'object'],
                ['field' => 'relationships', 'type' => 'array', 'format' => 'multi_array',
                    'nested' => [
                        ['field' => 'property_name', 'type' => 'string', 'required' => true, 'restrict_empty' => true],
                        ['field' => 'relationship_id', 'type' => 'integer', 'required' => true, 'restrict_zero' => true],
                        ['field' => 'include_default_fields', 'type' => 'boolean'],
                        ['field' => 'all_fields', 'type' => 'boolean'],
                        ['field' => 'field_ids', 'type' => 'array', 'format' => 'integer'],
                        ['field' => 'exclude_field_ids', 'type' => 'array', 'format' => 'integer'],
                        ['field' => 'primary_relationship_table_id', 'type' => 'integer', 'restrict_zero' => true],//needed if we have more than 1 main table columns. This should not be in relationship_tables[]
                        ['field' => 'relationship_tables', 'type' => 'array', 'format' => 'multi_array',
                            'nested' => [
                                ['field' => 'relationship_table_id', 'type' => 'integer', 'required' => true, 'restrict_zero' => true],
                                ['field' => 'property_name', 'required' => true, 'type' => 'string', 'restrict_empty' => true],
                            ]
                        ],
                    ]
                ],
            ]
        ],
    ];
    public array $dynamicResourceGetResponseRules = [];
    public array $dynamicResourcePostResponseRules = [
        ['field' => 'data', 'required' => true],
        ['field' => 'options', 'type' => 'object']
    ];
    public array $dynamicResourcePatchRequestRules = [
        ['field' => 'data', 'type' => 'array', 'format' => 'multi_array', 'min' => 1, 'required' => true],
    ];
    public array $dynamicResourcePatchResponseRules = [
        ['field' => 'data', 'type' => 'array', 'format' => 'multi_array', 'min' => 1, 'required' => true],
    ];
    public array $dynamicResourceDeleteRequestRules = [
        ['field' => 'ids', 'type' => 'comma_separated_string', 'min' => 1, 'required' => true],
    ];
    public array $dynamicResourceDeleteResponseRules = [];
    public array $dynamicResourceRecord_getQueryParamRules = [
        ['field' => 'params', 'type' => 'json_object',
            'nested' => [
                ['field' => 'include_default_fields', 'type' => 'boolean'],
                ['field' => 'all_fields', 'type' => 'boolean'],
                ['field' => 'field_ids', 'type' => 'array', 'format' => 'integer'],
                ['field' => 'exclude_field_ids', 'type' => 'array', 'format' => 'integer'],
                ['field' => 'limit', 'type' => 'integer'],
                ['field' => 'offset', 'type' => 'integer'],
                ['field' => 'filters', 'type' => 'object'],
                ['field' => 'query_params', 'type' => 'object'],
                ['field' => 'relationships', 'type' => 'array', 'format' => 'multi_array',
                    'nested' => [
                        ['field' => 'property_name', 'type' => 'string', 'required' => true, 'restrict_empty' => true],
                        ['field' => 'relationship_id', 'type' => 'integer', 'required' => true, 'restrict_zero' => true],
                        ['field' => 'include_default_fields', 'type' => 'boolean'],
                        ['field' => 'all_fields', 'type' => 'boolean'],
                        ['field' => 'field_ids', 'type' => 'array', 'format' => 'integer'],
                        ['field' => 'exclude_field_ids', 'type' => 'array', 'format' => 'integer'],
                        ['field' => 'primary_relationship_table_id', 'type' => 'integer', 'restrict_zero' => true],//needed if we have more than 1 main table columns. This should not be in relationship_tables[]
                        ['field' => 'relationship_tables', 'type' => 'array', 'format' => 'multi_array',
                            'nested' => [
                                ['field' => 'relationship_table_id', 'type' => 'integer', 'required' => true, 'restrict_zero' => true],
                                ['field' => 'property_name', 'required' => true, 'type' => 'string', 'restrict_empty' => true],
                            ]
                        ],
                    ]
                ],
            ]
        ],
    ];
    public array $dynamicResourceRecord_getResponseRules = [];
    public array $dynamicResourceRecord_postRequestRules = [
        ['field' => 'options', 'type' => 'object']
    ];
    public array $dynamicResourceRecord_postResponseRules = [
        ['field' => 'data', 'type' => 'object', 'required' => true, 'min' => 1],
        ['field' => 'options', 'type' => 'object']
    ];
    public array $dynamicResourceRecord_patchRequestRules = [
        ['field' => 'data', 'type' => 'object', 'min' => 1, 'required' => true],
    ];
    public array $dynamicResourceRecord_patchResponseRules = [
        ['field' => 'data', 'type' => 'object', 'min' => 1, 'required' => true],
    ];
    public array $dynamicResourceRecord_deleteRequestRules = [];
    public array $dynamicResourceRecord_deleteResponseRules = [];
    public string $dynamicResourceGetNotes = 'Returns all records for the given resource';
    public string $dynamicResourcePostNotes = 'Creates new record(s) for the given resource and optionally links to the given relationship. Data can either be an object or a multidimensional array';
    public string $dynamicResourcePatchNotes = 'Updates the given resources';
    public string $dynamicResourceDeleteNotes = 'Deletes the given resources or unlinks the given relationship';
    public string $dynamicResourceRecord_getNotes = 'Returns the given resource record';
    public string $dynamicResourceRecord_postNotes = 'Links existing resource to the given relationship';
    public string $dynamicResourceRecord_patchNotes = 'Updates the given resource';
    public string $dynamicResourceRecord_deleteNotes = 'Deletes the given resource or unlinks the given relationship';

    /**
     * @param mixed $systemConn
     * @param mixed $configConn
     * @param mixed $conn
     * @param string $tablePrefix
     * @param bool $dontExecuteQuery
     * @return mixed
     * @throws Exception
     */
    public function dynamicResourceAction(mixed $systemConn, mixed $configConn, mixed $conn, string $tablePrefix, bool $dontExecuteQuery = false): mixed
    {
        $this->dynamicResourceSystemConn = $systemConn;
        $this->dynamicResourceConfigConn = $configConn;
        $this->dynamicResourceConn = $conn;
        $this->dynamicResourceTablePrefix = $tablePrefix;
        $this->dynamicResourceDontExecuteQuery = $dontExecuteQuery;
        $this->dynamicResourceMainTableInfo = $this->getDynamicModuleResourceInfo();

        return $this->{$this->apiMethod}();
    }

    /**
     * @param bool $returnPivots
     * @param bool $isPost
     * @return array
     * @throws Exception
     */
    private function dynamicResourceGet(bool $returnPivots = false, bool $isPost = false): array
    {
        if (!$returnPivots) {
            $this->request = $this->initValidator();
        }

        $request = $this->request['params'];
        $idValuesReversed = array_reverse($this->idValues);
        $mainTable = $idValuesReversed[0];
        $mainTableName = $this->dynamicResourceMainTableInfo['table_name'];
        $mainTableId = $mainTable['table'];
        $mainDatabase = $this->dynamicResourceMainTableInfo['database'];
        if (empty($this->dynamicResourceMainTableInfo['relationship_table'])) {
            $tMainTableAlias = "$mainTableName";
        } else {
            $tMainTableAlias = "{$mainTableName}_{$this->dynamicResourceMainTableInfo['relationship_id']}";
        }

        $tableAliases = [$mainTableId => $tMainTableAlias];
        $mainTableAlias = "AS $tMainTableAlias";
        $mainTableColumnPrefix = "$tMainTableAlias";
        $mainTablePk = $this->dynamicResourceMainTableInfo['primary_key'];
        $relationshipTableIds = array_column($idValuesReversed, 'relationship_table');
        $totalRelationshipTableIds = count($relationshipTableIds);
        $join = '';
        if (is_null($mainTable['id']) || $mainTable['id'] == '*') {
            $action = 'show_in_list';
            $returnObj = false;
            $whereArr = [];
            $filters = $request['filters'] ?? [];
            if (!empty($filters)) {
                $fieldIds = array_values(array_unique(array_filter($this->misc->array_column_recursive($filters, 'field_id') ?? [])));
                $totalFieldIds = count($fieldIds);
                $fieldIdNameMap = [];
                if ($totalFieldIds > 0) {
                    $fieldIdNameMap = $this->dynamicResourceConfigConn->getRows("SELECT JSON_OBJECTAGG({$this->dynamicResourceTablePrefix}fields.id, CONCAT({$this->dynamicResourceTablePrefix}fields.{$this->dynamicResourceTablePrefix}table_id,'.',{$this->dynamicResourceTablePrefix}fields.field_name)) AS data,'field_id' AS filter_field_key FROM {$this->dynamicResourceTablePrefix}fields WHERE {$this->dynamicResourceTablePrefix}fields.id IN(?" . str_repeat(',?', $totalFieldIds - 1) . ')', $fieldIds)[0] ?? [];
                    $this->misc->json_decode_recursively($fieldIdNameMap);
                    if (count($fieldIdNameMap['data']) != $totalFieldIds) {
                        throw new Exception('Invalid filter field id(s)', 400);
                    }
                }

                $whereArr = $this->dynamicResourceConfigConn->setFilters($filters, $fieldIdNameMap, $request['query_params'] ?? []);
            }

            $where = [];
            $tWhere = $whereArr['query'] ?? '';
            $params = $whereArr['params'] ?? [];
        } else {
            $tWhere = '';
            $where = ["$mainTableColumnPrefix.$mainTablePk"];
            $params = [$mainTable['id']];
            $returnObj = true;
            $action = 'show_in_item';
        }
        $allMainTableFields = array_column(array_filter($this->dynamicResourceMainTableInfo['fields'], function ($v) use ($action, $request) {
            if (!empty($request['all_fields']) || (!in_array($v['field_id'], $request['exclude_field_ids'] ?? []) && (((empty($request['field_ids']) || !empty($request['include_default_fields'])) && ($v[$action] || $v['field_type_id'] == 1)) || (!empty($request['field_ids']) && in_array($v['field_id'], $request['field_ids']))))) {
                return true;
            }
            return false;
        }), 'field_name');
        if (empty($allMainTableFields)) {
            throw new Exception("No valid $action fields found", 403);
        }

        $allRelationshipProperties = [];
        $relationshipsArr = $request['relationships'] ?? [];
        $totalRelationshipArr = count($relationshipsArr);
        if ($totalRelationshipArr > 0) {
            if ($totalRelationshipArr != count(array_unique(array_column($relationshipsArr, 'relationship_id')))) {
                throw new Exception('Relationship id conflict', 403);
            }

            $relationshipsArr = array_column($relationshipsArr, null, 'relationship_id');
            $allMainPropertyNames = array_column($relationshipsArr, 'property_name');
            $allNestedPropertyNames = $this->misc->data_get('relationship_tables.*.property_name', $relationshipsArr) ?? [];
            if (count(array_intersect($allMainTableFields, $allMainPropertyNames)) > 0 || count($allMainPropertyNames) != count(array_unique($allMainPropertyNames)) || count($allNestedPropertyNames) != count(array_unique($allNestedPropertyNames))) {
                throw new Exception('Conflicting property names', 403);
            }
        }

        $mainTableFields = "$mainTableColumnPrefix.$mainTablePk,$mainTableColumnPrefix." . implode(",$mainTableColumnPrefix.", $allMainTableFields);
        $query = "SELECT DISTINCT $mainTableFields %s FROM $mainDatabase.$mainTableName $mainTableAlias";
        $relationsResponse = [];
        if ($totalRelationshipTableIds > 0) {
            $relationshipTableIdsStr = '?' . str_repeat(',?', $totalRelationshipTableIds - 1);
            $relations = $this->dynamicResourceConfigConn->getRows("SELECT DISTINCT {$this->dynamicResourceTablePrefix}relationship_tables.id AS relationship_table_id,{$this->dynamicResourceTablePrefix}relationships.id AS relationship_id,
                {$this->dynamicResourceTablePrefix}relationships.relationship_type_id,main_table.id AS main_table_id,main_table.table_name AS main_table_name,
                IF({$this->dynamicResourceTablePrefix}relationship_tables.pivot_column_name IS NULL OR {$this->dynamicResourceTablePrefix}relationship_tables.pivot_column_name = '',main_table.pivot_column_name, {$this->dynamicResourceTablePrefix}relationship_tables.pivot_column_name) AS main_pivot_column_name,
                main_table.primary_key AS main_primary_key,IFNULL(main_d.name, '{$this->dynamicResourceConfigConn->dbName}') AS main_database,referenced_table.table_name AS referenced_table_name,
                IF(referenced_relationship_tables.pivot_column_name IS NULL OR referenced_relationship_tables.pivot_column_name = '', referenced_table.pivot_column_name,referenced_relationship_tables.pivot_column_name) AS referenced_pivot_column_name,
                referenced_table.primary_key AS referenced_primary_key,IFNULL(referenced_d.name, '{$this->dynamicResourceConfigConn->dbName}') AS referenced_database,
                {$this->dynamicResourceTablePrefix}relationships.use_pivot_table,{$this->dynamicResourceTablePrefix}relationships.custom_pivot_table_name,pivot_table.table_name AS pivot_table_name,{$this->dynamicResourceTablePrefix}relationships.pivot_table_id,IFNULL(pivot_d.name, '{$this->dynamicResourceConfigConn->dbName}') AS pivot_database
            FROM {$this->dynamicResourceTablePrefix}relationships
            INNER JOIN {$this->dynamicResourceTablePrefix}relationship_tables ON {$this->dynamicResourceTablePrefix}relationships.id = {$this->dynamicResourceTablePrefix}relationship_tables.{$this->dynamicResourceTablePrefix}relationship_id
            LEFT JOIN {$this->dynamicResourceTablePrefix}tables main_table ON {$this->dynamicResourceTablePrefix}relationship_tables.{$this->dynamicResourceTablePrefix}table_id = main_table.id
            LEFT JOIN {$this->dynamicResourceTablePrefix}tables referenced_table ON referenced_table.id = {$this->dynamicResourceTablePrefix}relationship_tables.referenced_table AND {$this->dynamicResourceTablePrefix}relationship_tables.referenced_table IS NOT NULL
            LEFT JOIN {$this->dynamicResourceTablePrefix}tables pivot_table ON pivot_table.id = {$this->dynamicResourceTablePrefix}relationships.pivot_table_id AND {$this->dynamicResourceTablePrefix}relationships.use_pivot_table = TRUE
            LEFT JOIN {$this->dynamicResourceTablePrefix}databases main_d ON main_table.{$this->dynamicResourceTablePrefix}database_id = main_d.id
            LEFT JOIN {$this->dynamicResourceTablePrefix}databases referenced_d ON referenced_table.{$this->dynamicResourceTablePrefix}database_id = referenced_d.id
            LEFT JOIN {$this->dynamicResourceTablePrefix}databases pivot_d ON pivot_table.{$this->dynamicResourceTablePrefix}database_id = pivot_d.id
            LEFT JOIN (SELECT referenced_relationship_tables.pivot_column_name,referenced_relationship_tables.{$this->dynamicResourceTablePrefix}table_id,referenced_relationship_tables.{$this->dynamicResourceTablePrefix}relationship_id
                FROM {$this->dynamicResourceTablePrefix}relationship_tables AS referenced_relationship_tables 
                WHERE referenced_relationship_tables.referenced_table IS NULL) referenced_relationship_tables ON referenced_relationship_tables.{$this->dynamicResourceTablePrefix}relationship_id = {$this->dynamicResourceTablePrefix}relationships.id AND {$this->dynamicResourceTablePrefix}relationship_tables.referenced_table = referenced_relationship_tables.{$this->dynamicResourceTablePrefix}table_id
            WHERE {$this->dynamicResourceTablePrefix}relationship_tables.{$this->dynamicResourceTablePrefix}relationship_id IN (SELECT {$this->dynamicResourceTablePrefix}relationship_id FROM {$this->dynamicResourceTablePrefix}relationship_tables WHERE id IN ($relationshipTableIdsStr)) AND {$this->dynamicResourceTablePrefix}relationship_tables.{$this->dynamicResourceTablePrefix}table_id IN (SELECT {$this->dynamicResourceTablePrefix}table_id FROM {$this->dynamicResourceTablePrefix}relationship_tables WHERE id IN ($relationshipTableIdsStr))
            ORDER BY CASE WHEN {$this->dynamicResourceTablePrefix}relationships.relationship_type_id = 2 THEN 1 WHEN {$this->dynamicResourceTablePrefix}relationships.relationship_type_id = 3 THEN 2 ELSE 3 END, {$this->dynamicResourceTablePrefix}relationships.id", array_merge($relationshipTableIds, $relationshipTableIds));
            if (($totalRelationshipTableIds) !== count(array_intersect($relationshipTableIds, array_column($relations, 'relationship_table_id')))) {
                throw new Exception('API is not found', 404);
            }

            $relationshipGroups = array_values(array_reduce($relations, function ($carry, $item) {
                $carry[$item['relationship_id']][] = $item;
                return $carry;
            }, []));
            $relationshipTableGroups = array_map(function ($key) use ($relationshipTableIds) {
                return array_slice($relationshipTableIds, $key, 2);
            }, array_keys($relationshipTableIds));
            if (count(end($relationshipTableGroups)) < 2) {
                array_pop($relationshipTableGroups);
            }

            $relationsTables = array_column($relations, 'main_table_id', 'relationship_table_id');
            $relationsRelationships = array_column($relations, 'relationship_id', 'relationship_table_id');
            $joins = [];
            $totalRelationshipTableGroups = count($relationshipTableGroups);
            $totalPivotJoins = 0;
            $mainTableRelationshipId = 0;
            $times = 0;
            foreach ($relationshipTableGroups as $i => $relationshipTableGroup) {
                $tRelationshipTableGroup = $relationshipTableGroup;
                $found = false;
                $prevColumnBracket = '';
                $qIndex = $i + 1;
                foreach ($relationshipGroups as $k => $relationshipGroup) {
                    $tTotalRelationshipGroup = count($relationshipGroup);
                    if ($i == $totalRelationshipTableGroups - 1) {
                        $relationshipGroup = array_filter($relationshipGroup, function ($v) use ($relationshipTableGroup) {
                            return in_array($v['relationship_table_id'], $relationshipTableGroup);
                        });
                        $tRelationshipGroup = array_column($relationshipGroup, 'relationship_table_id');
                    } else {
                        $relationshipGroup = array_filter($relationshipGroup, function ($rg) use ($relationshipTableGroup, $relationsRelationships, $relationsTables) {
                            return $rg['relationship_table_id'] == $relationshipTableGroup[0] || ($rg['relationship_id'] == ($relationsRelationships[$relationshipTableGroup[0]] ?? 0) && $rg['main_table_id'] == ($relationsTables[$relationshipTableGroup[1]] ?? 0));
                        });
                        $tRelationshipGroup = count($relationshipGroup) == 2 ? $tRelationshipTableGroup : [];
                    }

                    sort($tRelationshipTableGroup);
                    sort($tRelationshipGroup);
                    if ($tRelationshipGroup == $tRelationshipTableGroup) {
                        $lookup = array_flip($relationshipTableGroup);
                        usort($relationshipGroup, function ($a, $b) use ($lookup) {
                            $existsA = isset($lookup[$a['relationship_table_id']]);
                            $existsB = isset($lookup[$b['relationship_table_id']]);
                            if ($existsA && $existsB) {
                                return $lookup[$a['relationship_table_id']] <=> $lookup[$b['relationship_table_id']];
                            }
                            return $existsA ? -1 : ($existsB ? 1 : 0);
                        });

                        $totalRelationshipGroup = count($relationshipGroup);
                        $sameTableRelation = ($totalRelationshipGroup > 2);
                        if ($sameTableRelation) {
                            $startBracket = '(';
                            $endBracket = ')';
                        } else {
                            $startBracket = '';
                            $endBracket = '';
                        }

                        $columnBracket = '';
                        foreach ($relationshipGroup as $j => $relationship) {
                            $joinTableAliasSuffix = "_{$relationship['relationship_id']}";
                            $tJoinTableAliasSuffix = ($relationship['main_table_id'] == $mainTableId) ? "__$joinTableAliasSuffix" : $joinTableAliasSuffix;
                            $joinTableAlias = "{$relationship['main_table_name']}$joinTableAliasSuffix";
                            $tJoinTableAlias = "{$relationship['main_table_name']}$tJoinTableAliasSuffix";
                            $tableAliases[$relationship['main_table_id']] = $joinTableAlias;
                            if ($relationship['use_pivot_table']) {
                                if (empty($mainTableRelationshipId) && $i == 0) {
                                    $mainTableRelationshipId = $relationship['relationship_id'];
                                    $relationsResponse['table_name'] = $relationship['pivot_table_name'];
                                    $relationsResponse['database'] = $relationship['pivot_database'];
                                }

                                if ($relationship['relationship_table_id'] == $relationshipTableGroup[0]) {
                                    $joinTableAlias = "{$relationship['pivot_table_name']}$joinTableAliasSuffix";
                                    $tJoinTableAlias = "{$relationship['pivot_table_name']}$tJoinTableAliasSuffix";
                                    $tableAliases[$relationship['pivot_table_id']] = $joinTableAlias;
                                    $joinTable = "{$relationship['pivot_database']}.{$relationship['pivot_table_name']} AS $joinTableAlias";
                                    $on = "$joinTableAlias.{$relationship['main_pivot_column_name']}={$relationship['main_table_name']}$joinTableAliasSuffix.{$relationship['main_primary_key']}";
                                    if (empty($joins[$joinTableAlias])) {
                                        $joins[$joinTableAlias] = "INNER JOIN $joinTable ON ($on";
                                        $totalPivotJoins++;
                                        if ($mainTableRelationshipId == $relationship['relationship_id']) {
                                            $relationsResponse['columns'][$relationship['main_pivot_column_name']] = $mainTable['id'] ?? null;
                                            $relationsResponse['column_name'] = $relationship['main_pivot_column_name'];
                                        }
                                    }
                                } else {
                                    $whereColumn = "$tJoinTableAlias.{$relationship['main_primary_key']}";
                                    $joinTable = "{$relationship['main_database']}.{$relationship['main_table_name']} AS $tJoinTableAlias";
                                    $on = "$whereColumn={$relationship['pivot_table_name']}$joinTableAliasSuffix.{$relationship['main_pivot_column_name']}";
                                    if (empty($joins[$joinTableAlias])) {
                                        $prevColumnBracket = $columnBracket;
                                        $joins[$joinTableAlias] = "INNER JOIN $joinTable ON ($startBracket$on";
                                        if (!$this->dynamicResourceDontExecuteQuery && ($isPost && $mainTableRelationshipId == $relationship['relationship_id'])) {
                                            $this->dynamicResourceConn->getRecord($relationship['main_table_name'], $idValuesReversed[$qIndex]['id']);
                                        } elseif ($idValuesReversed[$qIndex]['id'] !== '*') {
                                            $where[] = $whereColumn;
                                            $params[] = $idValuesReversed[$qIndex]['id'];
                                        }

                                        $columnBracket = $joinTableAlias;
                                        if ($mainTableRelationshipId == $relationship['relationship_id']) {
                                            $relationsResponse['columns'][$relationship['main_pivot_column_name']] = $idValuesReversed[$qIndex]['id'];
                                        }
                                    }
                                }

                                $found = true;
                                if ($j == $totalRelationshipGroup - 1 && !empty($columnBracket)) {
                                    $joins[$columnBracket] .= $endBracket;
                                }

                                if (!empty($prevColumnBracket)) {
                                    $joins[$prevColumnBracket] .= $endBracket;
                                    $prevColumnBracket = '';
                                }
                            } elseif (!empty($relationship['referenced_table_name'])) {
                                $found = true;
                                $relationsResponse['table_name'] = $relationship['main_table_name'];
                                $relationsResponse['database'] = $relationship['main_database'];
                                if ($relationship['relationship_table_id'] != $relationshipTableGroup[0]) {
                                    $relationsResponse['columns'][$relationship['main_primary_key']] = $idValuesReversed[$qIndex]['id'];
                                    $relationsResponse['columns'][$relationship['referenced_pivot_column_name']] = $mainTable['id'] ?? null;
                                    $relationsResponse['column_name'] = $relationship['referenced_pivot_column_name'];
                                    $joinTable = "{$relationship['main_database']}.{$relationship['main_table_name']} AS $tJoinTableAlias";
                                    if ($idValuesReversed[$qIndex]['id'] !== '*') {
                                        $where[] = "$tJoinTableAlias.{$relationship['main_primary_key']}";
                                        $params[] = $idValuesReversed[$qIndex]['id'];
                                    }

                                    $on = "{$relationship['main_table_name']}$tJoinTableAliasSuffix.{$relationship['referenced_pivot_column_name']}={$relationship['referenced_table_name']}$joinTableAliasSuffix.{$relationship['referenced_primary_key']}";
                                    if (empty($joins[$joinTableAlias])) {
                                        $joins[$joinTableAlias] = "INNER JOIN $joinTable ON ($on";
                                    }
                                } else {
                                    $relationsResponse['columns'][$relationship['main_primary_key']] = $mainTable['id'] ?? null;
                                    $relationsResponse['column_name'] = $relationship['main_primary_key'];
                                }
                            } elseif ($relationship['relationship_table_id'] != $relationshipTableGroup[0]) {
                                $found = true;
                                $whereColumn = "$tJoinTableAlias.{$relationship['main_primary_key']}";
                                $joinTable = "{$relationship['main_database']}.{$relationship['main_table_name']} AS $tJoinTableAlias";
                                $on = "$whereColumn=$tMainTableAlias.{$relationship['main_pivot_column_name']}";
                                if (empty($joins[$joinTableAlias])) {
                                    $joins[$joinTableAlias] = "INNER JOIN $joinTable ON ($on";
                                    if ($idValuesReversed[$qIndex]['id'] !== '*') {
                                        $where[] = $whereColumn;
                                        $params[] = $idValuesReversed[$qIndex]['id'];
                                    }

                                    $relationsResponse['columns'][$relationship['main_pivot_column_name']] = $idValuesReversed[$qIndex]['id'];
                                }
                            }
                        }

                        $times++;
                        if ($tTotalRelationshipGroup <= 2 || $tTotalRelationshipGroup == $times) {
                            unset($relationshipGroups[$k]);
                        }

                        break;
                    }
                }

                if (!$found) {
                    throw new Exception('API is not found', 404);
                }
            }

            if (count($joins) - $totalPivotJoins != $totalRelationshipTableIds - 1) {
                throw new Exception('API is not found', 404);
            }

            if (count($joins) > 0) {
                $join = " \n" . implode(") \n", $joins) . ')';
                $query .= $join;
            }

        } else {
            $relationsResponse['table_name'] = $mainTableName;
            $relationsResponse['database'] = $mainDatabase;
        }

        if ($totalRelationshipArr > 0) {
            $relationshipTypes = empty($this->dynamicResourceRelationshipTypes) ? $this->dynamicResourceSystemConn->getAllRows('relationship_types') ?? [] : $this->dynamicResourceRelationshipTypes;
            $relationships = $this->dynamicResourceConfigConn->getRows("SELECT DISTINCT {$this->dynamicResourceTablePrefix}relationship_tables.id AS relationship_table_id,{$this->dynamicResourceTablePrefix}relationships.id AS relationship_id,
                {$this->dynamicResourceTablePrefix}relationships.relationship_type_id,main_table.id AS main_table_id,main_table.table_name AS main_table_name,main_table_fields.fields,
                IF({$this->dynamicResourceTablePrefix}relationship_tables.pivot_column_name IS NULL OR {$this->dynamicResourceTablePrefix}relationship_tables.pivot_column_name = '',main_table.pivot_column_name, {$this->dynamicResourceTablePrefix}relationship_tables.pivot_column_name) AS main_pivot_column_name,
                main_table.primary_key AS main_primary_key,IFNULL(main_d.name, '{$this->dynamicResourceConfigConn->dbName}') AS main_database,referenced_table.table_name AS referenced_table_name, referenced_table.id AS referenced_table_id,
                IF(referenced_relationship_tables.pivot_column_name IS NULL OR referenced_relationship_tables.pivot_column_name = '', referenced_table.pivot_column_name,referenced_relationship_tables.pivot_column_name) AS referenced_pivot_column_name,
                referenced_table.primary_key AS referenced_primary_key,referenced_relationship_tables.relationship_table_id AS referenced_relationship_table_id,IFNULL(referenced_d.name, '{$this->dynamicResourceConfigConn->dbName}') AS referenced_database,
                {$this->dynamicResourceTablePrefix}relationships.use_pivot_table,{$this->dynamicResourceTablePrefix}relationships.custom_pivot_table_name,pivot_table.table_name AS pivot_table_name,{$this->dynamicResourceTablePrefix}relationships.pivot_table_id,IFNULL(pivot_d.name, '{$this->dynamicResourceConfigConn->dbName}') AS pivot_database
            FROM {$this->dynamicResourceTablePrefix}relationships
            INNER JOIN {$this->dynamicResourceTablePrefix}relationship_tables ON {$this->dynamicResourceTablePrefix}relationships.id = {$this->dynamicResourceTablePrefix}relationship_tables.{$this->dynamicResourceTablePrefix}relationship_id
            LEFT JOIN {$this->dynamicResourceTablePrefix}tables main_table ON {$this->dynamicResourceTablePrefix}relationship_tables.{$this->dynamicResourceTablePrefix}table_id = main_table.id
            LEFT JOIN {$this->dynamicResourceTablePrefix}tables referenced_table ON referenced_table.id = {$this->dynamicResourceTablePrefix}relationship_tables.referenced_table AND {$this->dynamicResourceTablePrefix}relationship_tables.referenced_table IS NOT NULL
            LEFT JOIN {$this->dynamicResourceTablePrefix}tables pivot_table ON pivot_table.id = {$this->dynamicResourceTablePrefix}relationships.pivot_table_id AND {$this->dynamicResourceTablePrefix}relationships.use_pivot_table = TRUE
            LEFT JOIN {$this->dynamicResourceTablePrefix}databases main_d ON main_table.{$this->dynamicResourceTablePrefix}database_id = main_d.id
            LEFT JOIN {$this->dynamicResourceTablePrefix}databases referenced_d ON referenced_table.{$this->dynamicResourceTablePrefix}database_id = referenced_d.id
            LEFT JOIN {$this->dynamicResourceTablePrefix}databases pivot_d ON pivot_table.{$this->dynamicResourceTablePrefix}database_id = pivot_d.id
            LEFT JOIN (SELECT referenced_relationship_tables.id AS relationship_table_id,referenced_relationship_tables.pivot_column_name,referenced_relationship_tables.{$this->dynamicResourceTablePrefix}table_id,referenced_relationship_tables.{$this->dynamicResourceTablePrefix}relationship_id
                FROM {$this->dynamicResourceTablePrefix}relationship_tables AS referenced_relationship_tables
                WHERE referenced_relationship_tables.referenced_table IS NULL) referenced_relationship_tables ON referenced_relationship_tables.{$this->dynamicResourceTablePrefix}relationship_id = {$this->dynamicResourceTablePrefix}relationships.id AND {$this->dynamicResourceTablePrefix}relationship_tables.referenced_table = referenced_relationship_tables.{$this->dynamicResourceTablePrefix}table_id 
            LEFT JOIN (SELECT {$this->dynamicResourceTablePrefix}table_id,JSON_ARRAYAGG(JSON_OBJECT('field_id',id,'field_name',field_name,'show_in_list',show_in_list,'show_in_item',show_in_item,'field_type_id',field_type_id)) AS fields FROM {$this->dynamicResourceTablePrefix}fields GROUP BY {$this->dynamicResourceTablePrefix}table_id) AS main_table_fields ON main_table.id = main_table_fields.{$this->dynamicResourceTablePrefix}table_id 
            WHERE {$this->dynamicResourceTablePrefix}relationship_tables.{$this->dynamicResourceTablePrefix}relationship_id IN (?" . str_repeat(',?', $totalRelationshipArr - 1) . ") 
            ORDER BY CASE WHEN {$this->dynamicResourceTablePrefix}relationships.relationship_type_id = 2 THEN 1 WHEN {$this->dynamicResourceTablePrefix}relationships.relationship_type_id = 3 THEN 2 ELSE 3 END, {$this->dynamicResourceTablePrefix}relationships.id,referenced_table.id DESC", array_column($relationshipsArr, 'relationship_id'));
            $tableInfo = [];
            array_map(function ($relationship) use (&$relationshipsArr, $mainTableId, &$tableInfo) {
                $relationship['fields'] = json_decode($relationship['fields'], true);
                if ($relationship['main_table_id'] == $mainTableId) {
                    if (!empty($relationship['referenced_relationship_table_id']) && $relationship['referenced_relationship_table_id'] != $relationship['relationship_table_id']) {
                        $relationshipsArr[$relationship['relationship_id']]['primary_relationship_table_id'] ??= $relationship['relationship_table_id'];
                    }

                    $tableInfo[$relationship['relationship_id']]['primary_relationship_table_id'] = $relationshipsArr[$relationship['relationship_id']]['primary_relationship_table_id'] ?? $relationship['relationship_table_id'];
                }

                $tableInfo[$relationship['relationship_id']]['referenced_relationship_table_id'] ??= $relationship['referenced_relationship_table_id'];
                $tableInfo[$relationship['relationship_id']]['use_pivot_table'] = $relationship['use_pivot_table'];
                $tableInfo[$relationship['relationship_id']]['pivot_table_name'] = $relationship['pivot_table_name'];
                $tableInfo[$relationship['relationship_id']]['tables'][$relationship['relationship_table_id']] = $relationship;
            }, $relationships);
            if (count($tableInfo) != $totalRelationshipArr) {
                throw new  Exception('Invalid relationship(s)', 403);
            }

            $extraJoins = [];
            $extraSelect = [];
            foreach ($relationshipsArr as $rel) {
                $currentRelationship = $tableInfo[$rel['relationship_id']];
                $relTables = $currentRelationship['tables'];
                $totalRelTables = count($relTables);
                $tRelTables = array_column($rel['relationship_tables'] ?? [], null, 'relationship_table_id');
                if (empty($tRelTables) && $totalRelTables > 2) {
                    throw new Exception('Relationship tables for many-many relationship is required', 403);
                } elseif (count(array_diff(array_keys($tRelTables), array_column($relTables, 'relationship_table_id'))) > 0) {
                    throw new Exception('Invalid relationship table(s)', 403);
                } elseif (!empty($rel['primary_relationship_table_id']) && empty($relTables[$rel['primary_relationship_table_id']])) {
                    throw new Exception('Invalid primary relationship table', 403);
                } elseif (count(array_unique(array_column($currentRelationship['tables'], 'main_table_id'))) != $totalRelTables && empty($rel['primary_relationship_table_id'])) {
                    throw new Exception("Primary relationship table for relationship {$rel['relationship_id']} is required", 403);
                }

                $relationshipTypeNames = array_column($relationshipTypes, 'type_name', 'id');
                $joinTableAliasSuffix = "__{$rel['relationship_id']}";
                if (empty($currentRelationship['use_pivot_table'])) {
                    if ($relTables[$currentRelationship['primary_relationship_table_id']] == $relTables[$currentRelationship['referenced_relationship_table_id']]) {
                        $otherTable = [];
                        $coalesce = 'JSON_ARRAY()';
                        $type = 'array';
                        $filterFields = 'show_in_list';
                        $fieldsStrStart = 'JSON_ARRAYAGG(JSON_OBJECT(';
                        $fieldsStrEnd = '))';
                        foreach ($relTables as $relTable) {
                            if ($relTable['relationship_table_id'] != $currentRelationship['primary_relationship_table_id']) {
                                if ($relationshipTypeNames[$relTable['relationship_type_id']] == 'one-one') {
                                    $filterFields = 'show_in_item';
                                }
                                $otherTable = [
                                    'table_name' => $relTable['main_table_name'],
                                    'pivot_column_name' => $relTable['main_primary_key'],
                                    'main_primary_key' => $relTable['referenced_pivot_column_name'],
                                    'fields' => $relTable['fields']
                                ];
                                break;
                            }
                        }
                    } else {
                        $coalesce = 'JSON_OBJECT()';
                        $type = 'object';
                        $filterFields = 'show_in_item';
                        $fieldsStrStart = 'JSON_OBJECT(';
                        $fieldsStrEnd = ')';
                        $otherTable = [
                            'table_name' => $relTables[$currentRelationship['primary_relationship_table_id']]['referenced_table_name'],
                            'pivot_column_name' => $relTables[$currentRelationship['primary_relationship_table_id']]['referenced_pivot_column_name'],
                            'main_primary_key' => $relTables[$currentRelationship['primary_relationship_table_id']]['main_primary_key'],
                            'fields' => $relTables[$currentRelationship['referenced_relationship_table_id']]['fields']
                        ];
                    }

                    if (empty($otherTable['pivot_column_name'])) {
                        throw new Exception("Table $mainTableId is not found in relationship {$rel['relationship_id']}");
                    }

                    $tFieldIds = array_column($otherTable['fields'], 'field_id');
                    if (count(array_diff($rel['field_ids'] ?? [], $tFieldIds)) > 0 || count(array_diff($rel['exclude_field_ids'] ?? [], $tFieldIds)) > 0) {
                        throw new Exception("Invalid field ids for relationships {$rel['relationship_id']}", 403);
                    }

                    $tExtraSelect = [];
                    array_filter($otherTable['fields'], function ($v) use ($joinTableAliasSuffix, $otherTable, $filterFields, $rel, &$tExtraSelect) {
                        if (!empty($rel['all_fields']) || (!in_array($v['field_id'], $rel['exclude_field_ids'] ?? []) && (((empty($rel['field_ids']) || !empty($rel['include_default_fields'])) && ($v[$filterFields] || $v['field_type_id'] == 1)) || (!empty($rel['field_ids']) && in_array($v['field_id'], $rel['field_ids']))))) {
                            $tExtraSelect[] = "'{$v['field_name']}',{$otherTable['table_name']}$joinTableAliasSuffix.{$v['field_name']}";
                        }
                    });

                    if (empty($tExtraSelect)) {
                        throw new Exception("Table {$otherTable['table_name']} does not have defined $filterFields fields or primary key", 403);
                    }

                    $extraJoins["{$otherTable['table_name']}$joinTableAliasSuffix"] = "LEFT JOIN(
                        SELECT {$otherTable['table_name']}$joinTableAliasSuffix.{$otherTable['main_primary_key']},$fieldsStrStart " . implode(',', $tExtraSelect) . "$fieldsStrEnd  AS {$rel['property_name']} 
                        FROM {$otherTable['table_name']} AS {$otherTable['table_name']}$joinTableAliasSuffix 
                        WHERE {$otherTable['table_name']}$joinTableAliasSuffix.{$otherTable['main_primary_key']} IS NOT NULL 
                        GROUP BY {$otherTable['table_name']}$joinTableAliasSuffix.{$otherTable['main_primary_key']}
                    ) AS {$otherTable['table_name']}$joinTableAliasSuffix ON $mainTableColumnPrefix.{$otherTable['pivot_column_name']} = {$otherTable['table_name']}$joinTableAliasSuffix.{$otherTable['main_primary_key']}";
                    $extraSelect[] = "COALESCE({$otherTable['table_name']}$joinTableAliasSuffix.{$rel['property_name']},$coalesce) AS {$rel['property_name']}";
                    $allRelationshipProperties[$type][] = $rel['property_name'];
                } else {
                    $nested = (count($tRelTables) > 1);
                    $tExtraSelect = [];
                    $tExtraJoins = [];
                    $coalesce = '';
                    foreach ($relTables as $relTable) {
                        if ($relTable['relationship_table_id'] != $currentRelationship['primary_relationship_table_id']) {
                            if (empty($tRelTables) || (!empty($tRelTables[$relTable['relationship_table_id']]))) {
                                if ($relationshipTypeNames[$relTable['relationship_type_id']] == 'one-one' || (empty($relTable['referenced_pivot_column_name']) && $relationshipTypeNames[$relTable['relationship_type_id']] == 'one-many')) {
                                    $filterFields = 'show_in_item';
                                    $type = 'object';
                                    $fieldsStrStart = '';
                                    $fieldsStrEnd = '';
                                    $coalesce = 'JSON_OBJECT()';
                                } else {
                                    $filterFields = 'show_in_list';
                                    $type = 'array';
                                    $fieldsStrStart = 'JSON_ARRAYAGG(';
                                    $fieldsStrEnd = ')';
                                    $coalesce = 'JSON_ARRAY()';
                                }

                                $tExtraJoins["{$relTable['main_table_name']}$joinTableAliasSuffix"] = "LEFT JOIN {$relTable['main_table_name']} AS {$relTable['main_table_name']}$joinTableAliasSuffix ON {$currentRelationship['pivot_table_name']}$joinTableAliasSuffix.{$relTable['main_pivot_column_name']} = {$relTable['main_table_name']}$joinTableAliasSuffix.{$relTable['main_primary_key']}";
                                $ztExtraSelect = [];
                                array_filter($relTable['fields'], function ($v) use ($relTable, $joinTableAliasSuffix, $filterFields, $rel, &$ztExtraSelect) {
                                    if (!empty($rel['all_fields']) || (!in_array($v['field_id'], $rel['exclude_field_ids'] ?? []) && (((empty($rel['field_ids']) || !empty($rel['include_default_fields'])) && ($v[$filterFields] || $v['field_type_id'] == 1)) || (!empty($rel['field_ids']) && in_array($v['field_id'], $rel['field_ids']))))) {
                                        $ztExtraSelect[] = "'{$v['field_name']}',{$relTable['main_table_name']}$joinTableAliasSuffix.{$v['field_name']}";
                                    }
                                });
                                if (empty($ztExtraSelect)) {
                                    throw new Exception("Table {$relTable['main_table_name']} does not have defined $filterFields fields or primary key", 403);
                                }

                                $ztExtraSelect = "COALESCE({$fieldsStrStart}JSON_OBJECT(" . implode(',', $ztExtraSelect) . ")$fieldsStrEnd,$coalesce)";
                                if ($nested) {
                                    $tExtraSelect[] = "'{$tRelTables[$relTable['relationship_table_id']]['property_name']}',$ztExtraSelect";
                                    $allRelationshipProperties[$type][] = $tRelTables[$relTable['relationship_table_id']]['property_name'];
                                } else {
                                    $tExtraSelect[] .= "$ztExtraSelect AS {$rel['property_name']}";
                                    $allRelationshipProperties[$type][] = $rel['property_name'];
                                }

                            }
                        }
                    }

                    $zztExtraSelect = implode(',', $tExtraSelect);
                    if ($nested) {
                        $coalesce = 'JSON_OBJECT()';
                        $type = 'object';
                        $zztExtraSelect = "COALESCE(JSON_OBJECT($zztExtraSelect),$coalesce) AS {$rel['property_name']}";
                        $allRelationshipProperties[$type][] = $rel['property_name'];
                    }

                    if (empty($relTables[$currentRelationship['primary_relationship_table_id']]['main_pivot_column_name'])) {
                        throw new Exception("Table $mainTableId is not found in relationship {$rel['relationship_id']}");
                    }

                    $extraJoins["{$currentRelationship['pivot_table_name']}$joinTableAliasSuffix"] = "LEFT JOIN (
                        SELECT {$currentRelationship['pivot_table_name']}$joinTableAliasSuffix.{$relTables[$currentRelationship['primary_relationship_table_id']]['main_pivot_column_name']},$zztExtraSelect 
                        FROM {$currentRelationship['pivot_table_name']} AS {$currentRelationship['pivot_table_name']}$joinTableAliasSuffix \n" . implode("\n", $tExtraJoins) . "
                        WHERE {$currentRelationship['pivot_table_name']}$joinTableAliasSuffix.{$relTables[$currentRelationship['primary_relationship_table_id']]['main_pivot_column_name']} IS NOT NULL 
                        GROUP BY {$currentRelationship['pivot_table_name']}$joinTableAliasSuffix.{$relTables[$currentRelationship['primary_relationship_table_id']]['main_pivot_column_name']}
                    ) AS {$currentRelationship['pivot_table_name']}$joinTableAliasSuffix ON $mainTableColumnPrefix.$mainTablePk={$currentRelationship['pivot_table_name']}$joinTableAliasSuffix.{$relTables[$currentRelationship['primary_relationship_table_id']]['main_pivot_column_name']}";
                    $extraSelect[] = "COALESCE({$currentRelationship['pivot_table_name']}$joinTableAliasSuffix.{$rel['property_name']},$coalesce) AS {$rel['property_name']}";
                    $allRelationshipProperties[$type][] = $rel['property_name'];
                }
            }

            $query = sprintf($query, (',' . implode(',', $extraSelect)));
            $query .= " \n" . implode(" \n", $extraJoins);
        } else {
            $query = sprintf($query, '');
        }
        if (!empty($tWhere)) {
            $query .= ' WHERE ' . preg_replace_callback('/(\d+)\./', function ($matches) use ($tableAliases) {
                    $key = $matches[1];
                    if (empty($tableAliases[$key])) {
                        throw new Exception('Invalid filter field', 400);
                    }

                    return $tableAliases[$key] . '.';
                }, $tWhere);
        }

        if (count($where) > 0) {
            $query .= (empty($tWhere) ? ' WHERE ' : ' AND ') . implode('=? AND ', $where) . '=?';
        }

        if ($returnPivots) {
            $query .= ' LIMIT 1';
        } elseif (!empty($request['limit']) || !empty($request['offset'])) {
            $query .= $this->dynamicResourceConn->setLimitAndOffset($request['limit'], $request['offset']);
        }
        $data = [];
        if (!$this->dynamicResourceDontExecuteQuery) {
            $response = $this->dynamicResourceConn->getRows($query, $params) ?? [];
            $this->misc->json_decode_recursively($response);
            if (!$isPost && $returnObj && count($response) == 0) {
                throw new Exception('The resource does not exist', 404);
            }

            $data = ($returnObj ? $response[0] ?? [] : $response);
        }

        $return = $returnPivots ? ['data' => $data, 'pivots' => $relationsResponse] : $data;
        if ($this->dynamicResourceDontExecuteQuery) {
            $return['query_info'] = [
                'query' => $query,
                'params' => $params,
                'table' => $mainTableName,
                'database' => $mainDatabase,
                'primary_key' => $mainTablePk,
                'columns' => $allMainTableFields,
                'relationships' => $allRelationshipProperties
            ];
        }

        return $return;
    }

    /**
     * @param bool $returnPivots
     * @param bool $isPost
     * @return array
     * @throws Exception
     */
    private function dynamicResourceRecord_get(bool $returnPivots = false, bool $isPost = false): array
    {
        return $this->dynamicResourceGet($returnPivots, $isPost);
    }

    /**
     * @return array
     * @throws Exception
     */
    private function dynamicResourcePost(): array
    {
        $action = 'can_insert';
        $info = $this->validateUpsertDynamicModuleResource(array_merge($this->request, ['action' => $action, 'error' => 'insertable']));
        $mainTable = $info['main_table'];
        $pivots = empty($info['pivot_table']['columns']) ? [] : $info['pivot_table'];
        if ($this->dynamicResourceDontExecuteQuery) {
            return [
                '$tableName' => $mainTable['table_name'],
                '$relationships' => $pivots['columns'] ?? [],
                '$pivotTableName' => $pivots['table_name'] ?? '',
                '$pivotColumnName' => $pivots['column_name'] ?? '',
                '$validFields' => $info["{$action}_fields"] ?? [],
                '$primaryKey' => $mainTable['primary_key'],
                '$database' => $mainTable['database'] ?? null,
                '$pivotDatabase' => $pivots['database'] ?? null,
            ];
        }


        return $this->postDataAndLinkRecords($this->dynamicResourceConn, $mainTable['table_name'], $pivots['columns'] ?? [], $pivots['table_name'] ?? '', $pivots['column_name'] ?? '', $info["{$action}_fields"] ?? [], $mainTable['primary_key'], $mainTable['database'] ?? null, $pivots['database'] ?? null);
    }

    /**
     * @throws Throwable
     */
    private function dynamicResourceRecord_post(): array
    {
        if ((count($this->idValues)) == 1 && !is_null($this->idValues[0]['id'])) {
            throw new Exception('API is not found', 404);
        }

        $action = 'can_insert';
        $info = $this->validateUpsertDynamicModuleResource(array_merge($this->request, ['action' => $action, 'error' => 'insertable']));
        $pivots = $info['pivot_table'];
        $mainTable = $info['main_table'];
        $this->dynamicResourcePivotInfo = [
            'table_name' => $mainTable['table_name'],
            'database' => $mainTable['database'],
            'primary_key' => $mainTable['primary_key'],
            'fields' => array_column(array_filter($mainTable['fields'], function ($v) {
                return !empty($v['show_in_item']);
            }), 'field_name'),
            'pivot_table' => $pivots['table_name'],
            'pivot_database' => $pivots['database'],
            'pivot_primary_key' => $pivots['column_name'],
            'where_fields' => $pivots['columns']
        ];

        if ($this->dynamicResourceDontExecuteQuery) {
            return [
                '$tableName' => $pivots['table_name'],
                '$data' => $pivots['columns'] ?? [],
                '$validFields' => $info["{$action}_fields"] ?? [],
                '$database' => $mainTable['database'] ?? null,
                'primary_key' => $mainTable['primary_key'],
                'query' => "SELECT {$this->dynamicResourcePivotInfo['table_name']}." . implode(",{$this->dynamicResourcePivotInfo['table_name']}.", $this->dynamicResourcePivotInfo['fields']) .
                    " FROM {$this->dynamicResourcePivotInfo['database']}.{$this->dynamicResourcePivotInfo['table_name']}
              INNER JOIN {$this->dynamicResourcePivotInfo['pivot_database']}.{$this->dynamicResourcePivotInfo['pivot_table']} AS pivot ON {$this->dynamicResourcePivotInfo['table_name']}.{$this->dynamicResourcePivotInfo['primary_key']} = pivot.{$this->dynamicResourcePivotInfo['pivot_primary_key']}
              WHERE {$this->dynamicResourcePivotInfo['table_name']}.{$this->dynamicResourcePivotInfo['primary_key']} =? AND pivot." . implode("=? AND pivot.", array_keys($this->dynamicResourcePivotInfo['where_fields'])) . '=?',
                'params' => array_merge([$this->dynamicResourcePivotInfo['where_fields'][$this->dynamicResourcePivotInfo['pivot_primary_key']]], array_values($this->dynamicResourcePivotInfo['where_fields']))
            ];
        }

        return $this->linkRecords($this->dynamicResourceConn, $pivots['table_name'], $pivots['columns'] ?? [], 'dynamicResourceGetLinkedResource');
    }

    /**
     * @throws Exception
     */
    private function dynamicResourcePatch(): array
    {
        $action = 'can_edit';
        $info = $this->validateUpsertDynamicModuleResource(array_merge($this->request, ['action' => $action, 'error' => 'editable']));
        $mainTable = $info['main_table'];
        if ($this->dynamicResourceDontExecuteQuery) {
            return [
                '$tableName' => $mainTable['table_name'],
                '$primaryKey' => $mainTable['primary_key'],
                '$database' => $mainTable['database'] ?? null,
                '$validFields' => $info["{$action}_fields"] ?? [],
            ];
        }
        return $this->patchData($this->dynamicResourceConn, $mainTable['table_name'], $mainTable['primary_key'], $mainTable['database'] ?? null, true);
    }

    /**
     * @throws Throwable
     */
    private function dynamicResourceRecord_patch(): array
    {
        $action = 'can_edit';
        $info = $this->validateUpsertDynamicModuleResource(array_merge($this->request, ['action' => $action, 'error' => 'editable']));
        $mainTable = $info['main_table'];
        if ($this->dynamicResourceDontExecuteQuery) {
            return [
                '$tableName' => $mainTable['table_name'],
                '$primaryKey' => $mainTable['primary_key'],
                '$database' => $mainTable['database'] ?? null,
                '$validFields' => $info["{$action}_fields"] ?? [],
                '$recordId' => $mainTable['id'],
            ];
        }
        return $this->recordPatchData($this->dynamicResourceConn, $mainTable['table_name'], $mainTable['id'], $mainTable['primary_key'], $mainTable['database'] ?? null);
    }

    /**
     * @throws Throwable
     */
    private function dynamicResourceDelete(): ?array
    {
        $mainTable = array_reverse($this->idValues)[0];
        if ((count($this->idValues)) > 1) {
            $pivots = $this->dynamicResourceGet(true, false)['pivots'];
        }

        if ($this->dynamicResourceDontExecuteQuery) {
            return [
                '$tableName' => $mainTable['table_name'],
                '$record' => $mainTable['id'],
                '$primaryKey' => $mainTable['primary_key'],
                '$database' => $mainTable['database'] ?? null,
                '$pivotTableName' => $pivots['table_name'] ?? '',
                '$pivotColumnName' => $pivots['column_name'] ?? '',
                '$relationships' => $pivots['columns'] ?? [],
                '$pivotDatabase' => $pivots['database'] ?? null
            ];
        }

        $this->recordDeleteDataRecords($this->dynamicResourceConn, $mainTable['table_name'], $mainTable['id'], $mainTable['primary_key'], $mainTable['database'] ?? null, $pivots['table_name'] ?? '', $pivots['column_name'] ?? '', $pivots['columns'] ?? [], $pivots['database'] ?? null);

        return null;
    }

    /**
     * @return array|null
     * @throws Throwable
     */
    private function dynamicResourceRecord_delete(): ?array
    {
        return $this->dynamicResourceDelete();
    }

    /**
     * @throws Exception
     */
    private function dynamicResourceGetLinkedResource()
    {
        $data = $this->dynamicResourceConn->getRows("SELECT {$this->dynamicResourcePivotInfo['table_name']}." . implode(",{$this->dynamicResourcePivotInfo['table_name']}.", $this->dynamicResourcePivotInfo['fields']) .
            " FROM {$this->dynamicResourcePivotInfo['database']}.{$this->dynamicResourcePivotInfo['table_name']}
              INNER JOIN {$this->dynamicResourcePivotInfo['pivot_database']}.{$this->dynamicResourcePivotInfo['pivot_table']} AS pivot ON {$this->dynamicResourcePivotInfo['table_name']}.{$this->dynamicResourcePivotInfo['primary_key']} = pivot.{$this->dynamicResourcePivotInfo['pivot_primary_key']}
              WHERE {$this->dynamicResourcePivotInfo['table_name']}.{$this->dynamicResourcePivotInfo['primary_key']} =? AND pivot." . implode("=? AND pivot.", array_keys($this->dynamicResourcePivotInfo['where_fields'])) . '=?', array_merge([$this->dynamicResourcePivotInfo['where_fields'][$this->dynamicResourcePivotInfo['pivot_primary_key']]], array_values($this->dynamicResourcePivotInfo['where_fields'])))[0] ?? [];
        if (count($data) == 0) {
            throw new Exception('The resource does not exist', 404);
        }

        return $data;
    }

    /**
     * @param array $options
     * @return array
     * @throws Exception
     */
    private function validateUpsertDynamicModuleResource(array $options): array
    {
        $mainTable = end($this->idValues);
        $validFields = array_column(array_filter($mainTable['fields'], function ($v) use ($options) {
            if (!empty($options['all_fields']) || (!in_array($v['field_id'], $options['exclude_field_ids'] ?? []) && (((empty($options['field_ids']) || !empty($options['include_default_fields'])) && ($v[$options['action']] || $v['field_type_id'] == 1)) || (!empty($options['field_ids']) && in_array($v['field_id'], $options['field_ids']))))) {
                return true;
            }
            return false;
        }), 'field_name');

        if (count($validFields) == 0) {
            throw new Exception("No {$options['error']} field in this table", 400);
        }

        $info = $this->dynamicResourceGet(true, ($this->httpMethod == 'POST'));
        return [
            'main_table' => $mainTable,
            "{$options['action']}_fields" => $validFields,
            'pivot_table' => $info['pivots']
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getDynamicModuleResourceInfo(): array
    {
        $mainTableId = $this->idValues[0]['table'] ?? 0;
        $relationshipTableIds = array_column($this->idValues, 'relationship_table');
        $totalRelationshipTableIds = count($relationshipTableIds);
        $hasRelationships = $totalRelationshipTableIds > 0;
        $extraErrorMessage = '';
        if ($hasRelationships) {
            $query = "SELECT zrt.id AS relationship_table,zrt.{$this->dynamicResourceTablePrefix}relationship_id AS relationship_id,
                COALESCE((SELECT JSON_OBJECT('table_name',zt.table_name,'primary_key',zt.primary_key,'table', zt.id,'database',IFNULL(d.name,'{$this->dynamicResourceConfigConn->dbName}'),'relationship_table',zrt.id, 'relationship_id',zrt.{$this->dynamicResourceTablePrefix}relationship_id,'dynamic_resource_options',COALESCE(zrt.dynamic_resource_options, JSON_OBJECT()),'fields',COALESCE(JSON_ARRAYAGG(JSON_OBJECT('field_id', zf.id, 'field_name', zf.field_name, 'can_edit', zf.can_edit,'can_insert',zf.can_insert, 'show_in_item', zf.show_in_item, 'show_in_list',zf.show_in_list)), JSON_ARRAY()))
                FROM {$this->dynamicResourceTablePrefix}relationship_tables zrt
                INNER JOIN {$this->dynamicResourceTablePrefix}tables zt ON zrt.{$this->dynamicResourceTablePrefix}table_id = zt.id 
                INNER JOIN {$this->dynamicResourceTablePrefix}fields zf ON zt.id = zf.{$this->dynamicResourceTablePrefix}table_id 
                LEFT JOIN {$this->dynamicResourceTablePrefix}databases d ON zt.{$this->dynamicResourceTablePrefix}database_id = d.id
                WHERE zrt.id = ?),JSON_OBJECT()) AS last_relationship_table
            FROM {$this->dynamicResourceTablePrefix}tables zt
            INNER JOIN {$this->dynamicResourceTablePrefix}relationship_tables zrt ON zt.id = zrt.{$this->dynamicResourceTablePrefix}table_id
            WHERE zt.id = ? AND zrt.id !=? AND zrt.{$this->dynamicResourceTablePrefix}relationship_id IN (SELECT {$this->dynamicResourceTablePrefix}relationship_id FROM {$this->dynamicResourceTablePrefix}relationship_tables WHERE id = ?)
            GROUP BY zt.id,zrt.id";
            $params = [end($relationshipTableIds), $mainTableId, $relationshipTableIds[0], $relationshipTableIds[0]];
            $extraErrorMessage = "or is not in the same relationship as relationship table $relationshipTableIds[0]";
        } else {
            $query = "SELECT zt.table_name,zt.primary_key,IFNULL(d.name,'{$this->dynamicResourceConfigConn->dbName}') AS `database`,COALESCE(zt.dynamic_resource_options, JSON_OBJECT()) AS dynamic_resource_options,COALESCE(JSON_ARRAYAGG(JSON_OBJECT('field_id', zf.id, 'field_name', zf.field_name, 'can_edit', zf.can_edit,'can_insert',zf.can_insert, 'show_in_item', zf.show_in_item, 'show_in_list',zf.show_in_list)), JSON_ARRAY()) AS fields
            FROM {$this->dynamicResourceTablePrefix}tables zt
            INNER JOIN {$this->dynamicResourceTablePrefix}fields zf ON zt.id = zf.{$this->dynamicResourceTablePrefix}table_id
            LEFT JOIN {$this->dynamicResourceTablePrefix}databases d ON zt.{$this->dynamicResourceTablePrefix}database_id = d.id
            WHERE zt.id = ?
            GROUP BY zt.id";
            $params = [$mainTableId];
        }

        $allInfo = $this->dynamicResourceConfigConn->getRows($query, $params)[0] ?? [];
        if (empty($allInfo)) {
            throw new Exception("Table $mainTableId is not found $extraErrorMessage", 404);
        }

        $this->misc->json_decode_recursively($allInfo);

        if ($hasRelationships) {
            if (empty($allInfo['last_relationship_table'])) {
                throw new Exception("Relationship table $relationshipTableIds[0] is not found", 404);
            }

            $this->idValues[0] += [
                'relationship_table' => $allInfo['relationship_table'],
                'relationship_id' => $allInfo['relationship_id']
            ];

            usort($allInfo['last_relationship_table']['fields'], function ($a, $b) {
                return $a['field_id'] <=> $b['field_id'];
            });
            $dynamicMainTableInfo = $allInfo['last_relationship_table'];
            $this->idValues[count($this->idValues) - 1] += $dynamicMainTableInfo;
        } else {
            usort($allInfo['fields'], function ($a, $b) {
                return $a['field_id'] <=> $b['field_id'];
            });
            $dynamicMainTableInfo = $allInfo;
            $this->idValues[0] += $dynamicMainTableInfo;
        }

        $mainTable = end($this->idValues);
        $httpMethod = is_null($mainTable['id']) ? strtolower($this->httpMethod) : 'record_' . strtolower($this->httpMethod);
        $mainTable['dynamic_resource_options'] = array_change_key_case($mainTable['dynamic_resource_options']);
        $permissions = (array_key_exists($httpMethod, $mainTable['dynamic_resource_options']) ? $mainTable['dynamic_resource_options'][$httpMethod] : (array_key_exists('default', $mainTable['dynamic_resource_options']) ? $mainTable['dynamic_resource_options']['default'] : $this->dynamicResourceMethodDefault));
        if (!is_bool($permissions) || !$permissions) {
            throw new Exception('Restricted resource action', 400);
        }

        return $dynamicMainTableInfo;
    }
}