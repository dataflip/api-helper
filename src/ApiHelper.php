<?php

namespace DataBackFlip;

use Doctrine\Inflector\InflectorFactory;
use Exception;
use ReflectionClass;
use ReflectionMethod;
use Throwable;

class ApiHelper
{
    /**
     * @var array|string[]
     */
    protected ?array $request;
    protected bool $debug;
    protected array $extras;
    protected array $params;
    protected ?string $httpMethod;
    protected ?array $queryParams;
    protected ?array $cookieParams;
    protected ?array $idValues;
    protected mixed $currentId;
    protected ?string $apiMethod;
    protected Validator $validator;
    protected Misc $misc;
    protected string $baseUrl;
    private static array $http_status_codes = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => '(Unused)',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => "I'm a teapot",
        419 => 'Authentication Timeout',
        420 => 'Enhance Your Calm',
        421 => 'Method Failure',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        444 => 'No Response',
        449 => 'Retry With',
        450 => 'Blocked by Windows Parental Controls',
        451 => 'Unavailable For Legal Reasons',
        494 => 'Request Header Too Large',
        495 => 'Cert Error',
        496 => 'No Cert',
        497 => 'HTTP to HTTPS',
        499 => 'Client Closed Request',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        509 => 'Bandwidth Limit Exceeded',
        510 => 'Not Extended',
        511 => 'Network Authentication Required',
        598 => 'Network read timeout error',
        599 => 'Network connect timeout error'
    ];
    protected static array $allowedMethods = ['GET', 'POST', 'OPTIONS', 'PUT', 'PATCH', 'DELETE'];
    protected static array $moduleResourceSegments = ['ModuleResources', 'DynamicModuleResources'];
    protected string $defaultNamespace = 'WebApi';
    protected string $defaultNamespaceFolder = 'web-api';
    protected ?array $validatorRequestOptions;
    protected ?array $validatorResponseOptions;
    protected array $validatorInitOptions = [];
    protected array $postRequestRules = [];
    protected array $postResponseRules = [];
    protected array $postQueryParamRules = [];
    protected string $postNotes = '';
    protected string $postHeader = '';
    protected array $record_postRequestRules = [];
    protected array $record_postResponseRules = [];
    protected array $record_postQueryParamRules = [];
    protected string $record_postNotes = '';
    protected string $record_postHeader = '';
    protected array $getRequestRules = [];
    protected array $getResponseRules = [];
    protected array $getQueryParamRules = [];
    protected string $getNotes = '';
    protected string $getHeader = '';
    protected array $record_getRequestRules = [];
    protected array $record_getResponseRules = [];
    protected array $record_getQueryParamRules = [];
    protected string $record_getNotes = '';
    protected string $record_getHeader = '';
    protected array $patchRequestRules = [];
    protected array $patchResponseRules = [];
    protected array $patchQueryParamRules = [];
    protected string $patchNotes = '';
    protected string $patchHeader = '';
    protected array $record_patchRequestRules = [];
    protected array $record_patchResponseRules = [];
    protected array $record_patchQueryParamRules = [];
    protected string $record_patchNotes = '';
    protected string $record_patchHeader = '';
    protected array $putRequestRules = [];
    protected array $putResponseRules = [];
    protected array $putQueryParamRules = [];
    protected string $putNotes = '';
    protected string $putHeader = '';
    protected array $record_putRequestRules = [];
    protected array $record_putResponseRules = [];
    protected array $record_putQueryParamRules = [];
    protected string $record_putNotes = '';
    protected string $record_putHeader = '';
    protected array $deleteRequestRules = [];
    protected array $deleteResponseRules = [];
    protected array $deleteQueryParamRules = [];
    protected string $deleteNotes = '';
    protected string $deleteHeader = '';
    protected array $record_deleteRequestRules = [];
    protected array $record_deleteResponseRules = [];
    protected array $record_deleteQueryParamRules = [];
    protected string $record_deleteNotes = '';
    protected string $record_deleteHeader = '';
    //traits
    use DynamicModuleResource;

    /**
     * @throws Exception
     */
    public function __construct(array $params, array $extras = [], bool $debug = false)
    {
        $this->params = $params;
        $this->request = $this->params['request'] ?? [];
        $this->httpMethod = $this->params['httpMethod'] ?? $_SERVER['REQUEST_METHOD'];
        $this->queryParams = $this->params['queryParams'] ?? [];
        $this->cookieParams = $this->params['cookieParams'] ?? [];
        $this->idValues = $this->params['idValues'] ?? [];
        $this->currentId = $this->params['currentId'] ?? null;
        $this->apiMethod = $this->params['apiMethod'] ?? null;
        $this->debug = $debug;
        $this->extras = $extras;
        $this->validator = new Validator($debug);
        $this->misc = new Misc();
        $this->baseUrl = $this->get_base_url();
        $isSchema = $this->request['schema'] ?? $this->queryParams['schema'] ?? false;
        if (!empty($isSchema) && is_bool($isSchema)) {
            $this->validator->generateSchema = true;
        }
    }

    /**
     * @throws Exception
     */
    public function initValidator(): array
    {
        $this->validator->init($this->{$this->apiMethod . 'RequestRules'}, $this->{$this->apiMethod . 'ResponseRules'}, $this->httpMethod, $this->validatorInitOptions, $this->{$this->apiMethod . 'QueryParamRules'});
        return $this->validator->validateRequest($this->request, $this->validatorRequestOptions ?? null, $this->queryParams);
    }

    /**
     * @param mixed|null $error_level
     * @param bool $display_errors
     * @param string $dynamicResourceConfigDBObjName
     * @param string $dynamicResourceDbObjName
     * @param array $customAllowedHeaders
     * @param bool $useQueryParamsAsRequest
     * @param string $tablePrefix
     * @param bool $ignoreURLPrefix
     * @param string|null $defaultNamespace
     * @param string|null $defaultNamespaceFolder
     * @param string $dynamicResourceSystemDBObjName
     * @return void
     */
    public static function init(mixed $error_level = null, bool $display_errors = false, string $dynamicResourceConfigDBObjName = 'configConn', string $dynamicResourceDbObjName = 'conn', array $customAllowedHeaders = ['AUTHORIZATION'], bool $useQueryParamsAsRequest = true, string $tablePrefix = 'zzz_', bool $ignoreURLPrefix = false, ?string $defaultNamespace = null, ?string $defaultNamespaceFolder = null, string $dynamicResourceSystemDBObjName = 'systemConn'): void
    {
        try {
            $customAllowedHeaders = count($customAllowedHeaders) > 0 ? $customAllowedHeaders : ['AUTHORIZATION'];
            error_reporting($error_level);
            ini_set('display_errors', $display_errors);
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, ' . implode(', ', $customAllowedHeaders));
            header('Content-Type: application/json; charset=UTF-8');
            header('Access-Control-Allow-Methods: ' . implode(', ', self::$allowedMethods));
            header('HTTP/1.1 200 OK');

            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                // It's a preflight request. Respond OK without any content.
                http_response_code(200);
                exit;
            }

            $phpInput = json_decode(file_get_contents('php://input'), true);
            $inputVariables = (!empty($phpInput) && is_array($phpInput)) ? $phpInput : [];
            $postVariables = (!empty($_POST) && is_array($_POST)) ? $_POST : [];
            $request = array_merge($postVariables, $inputVariables);
            $errorText = null;
            $_SERVER['REDIRECT_URL'] = rtrim($_SERVER['REDIRECT_URL'] ?? '', DIRECTORY_SEPARATOR);
            $minURLLength = ($ignoreURLPrefix ? 2 : 3);
            if (count(array_filter(explode(DIRECTORY_SEPARATOR, $_SERVER['REDIRECT_URL']))) >= $minURLLength) {
                $urlSplits = explode(DIRECTORY_SEPARATOR, ltrim($_SERVER['REDIRECT_URL'], DIRECTORY_SEPARATOR), $minURLLength - 1);
                $apiModules = array_filter(explode(DIRECTORY_SEPARATOR, preg_replace('/[_-]/', '', ucwords($urlSplits[$minURLLength - 2], '/[_-]/'))));
                if (count($apiModules) == 2 && $apiModules[0] == 'ZzzAutoDocumentation' && ($apiModules[1] == 'GenerateSchema' || $apiModules[1] == 'GenerateInsomniaImportFile') && strtoupper($_SERVER['REQUEST_METHOD']) == 'GET') {
                    $method = lcfirst($apiModules[1]);
                    parse_str($_SERVER['QUERY_STRING'], $queryParams);

                    (new ApiDocumentation(['queryParams' => $queryParams]))->$method();
                }

                $originalApiModules = array_filter(explode(DIRECTORY_SEPARATOR, $urlSplits[$minURLLength - 2]));
                $callMethod = strtolower($_SERVER['REQUEST_METHOD']);
                $tCallMethod = $callMethod;
                $apiMethod = $callMethod;
                $tApiMethod = ucfirst($callMethod);
                $moduleResourcesFound = false;
                $dynamicModuleResourcesFound = false;
                $moduleResourcesCount = 0;
                $prevResource = '';
                $currentId = null;
                $idValues = [];
                $tApiModules = $apiModules;
                $extraRequest = [];
                $totalApiModules = count($tApiModules);
                $reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
                for ($i = 0; $i < $totalApiModules; $i++) {
                    if (!$moduleResourcesFound && in_array($tApiModules[$i], self::$moduleResourceSegments)) {
                        $moduleResourcesFound = true;
                        if ($tApiModules[$i] == 'DynamicModuleResources') {
                            $dynamicModuleResourcesFound = true;
                            unset($apiModules[$i]);
                        }

                        continue;
                    }

                    if ($moduleResourcesFound) {
                        $moduleResourcesCount++;
                        $inflector = InflectorFactory::create()->build();
                        if ($moduleResourcesCount % 2 != 0 || $tApiModules[$i] === 'custom') {
                            $prevResource = $inflector->tableize($tApiModules[$i]);
                            $currentId = null;
                            $callMethod = $tCallMethod;
                            $apiMethod = $tCallMethod;
                            if ($dynamicModuleResourcesFound) {
                                $prevResource = $originalApiModules[$i];
                                if ($i == ($totalApiModules - 1)) {
                                    if (str_replace('_', '-', $originalApiModules[$i - 1]) == 'dynamic-module-resources') {
                                        $idValues[] = [
                                            'table' => $prevResource,
                                            'id' => null
                                        ];
                                    } else {
                                        $idValues[] = [
                                            'relationship_table' => $prevResource,
                                            'id' => null
                                        ];
                                    }
                                }

                                $callMethod = 'dynamicResourceAction';
                                $apiMethod = "dynamicResource$tApiMethod";
                                unset($apiModules[$i]);
                            }
                        } else {
                            $id = $tApiModules[$i];
                            if ($dynamicModuleResourcesFound) {
                                if (empty($idValues)) {
                                    $idValues[] = [
                                        'table' => $prevResource,
                                        'id' => $id
                                    ];
                                } else {
                                    $idValues[] = [
                                        'relationship_table' => $prevResource,
                                        'id' => $id
                                    ];
                                }
                            } else {
                                $key = $inflector->singularize($prevResource) . '_id';
                                $extraRequest[$key] = $id;
                                $idValues[] = [
                                    'table' => $prevResource,
                                    'field' => $key,
                                    'id' => $id
                                ];
                            }

                            $currentId = $id;
                            $callMethod = ($dynamicModuleResourcesFound ? 'dynamicResourceAction' : "record_$callMethod");
                            $apiMethod = ($dynamicModuleResourcesFound ? "dynamicResourceRecord_$tCallMethod" : $callMethod);
                            unset($apiModules[$i]);
                        }
                    }
                }

                $apiModuleStr = implode(DIRECTORY_SEPARATOR, $apiModules);
                $url = DIRECTORY_SEPARATOR . $urlSplits[0] . DIRECTORY_SEPARATOR . 'api-modules' . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $apiModules);
                $apiMethodClassPath = $_SERVER['DOCUMENT_ROOT'] . $url . DIRECTORY_SEPARATOR . end($apiModules) . '.php';
                $fileExists = file_exists($apiMethodClassPath);

                if ($dynamicModuleResourcesFound && count($idValues) > 0 || $fileExists) {
                    if ($fileExists) {
                        $apiMethodClass = str_replace(DIRECTORY_SEPARATOR, '\\', preg_replace('/[_-]/', '', ucwords($urlSplits[0], '/[_-]/')) . '\\' . $apiModuleStr) . '\\' . end($apiModules);
                    } else {
                        $currentClass = __CLASS__;
                        $defaultNamespace ??= (new $currentClass([]))->defaultNamespace;
                        $apiMethodClassPath = $_SERVER['DOCUMENT_ROOT'] . $url . DIRECTORY_SEPARATOR . $defaultNamespace . '.php';
                        $apiMethodClass = "$defaultNamespace\\$defaultNamespace";
                    }
                    require_once($apiMethodClassPath);
                    if (class_exists($apiMethodClass)) {
                        parse_str($_SERVER['QUERY_STRING'], $queryParams);
                        $apiAction = new $apiMethodClass([
                            'request' => $useQueryParamsAsRequest ? array_merge($queryParams, $request, $extraRequest) : array_merge($request, $extraRequest),
                            'httpMethod' => $_SERVER['REQUEST_METHOD'],
                            'apiMethod' => $apiMethod,
                            'queryParams' => array_merge($queryParams, $extraRequest),
                            'cookieParams' => $_COOKIE,
                            'idValues' => $idValues,
                            'currentId' => $currentId
                        ]);

                        if (!$dynamicModuleResourcesFound) {
                            $apiAction->callMethod($callMethod, true, $dynamicModuleResourcesFound, [], true);
                        }

                        if (empty($apiAction->{$dynamicResourceSystemDBObjName})) {
                            $errorText = "Missing Database Connection object ($dynamicResourceSystemDBObjName)";
                        } elseif (empty($apiAction->{$dynamicResourceConfigDBObjName})) {
                            $errorText = "Missing Database Connection object ($dynamicResourceConfigDBObjName)";
                        } elseif (empty($apiAction->{$dynamicResourceDbObjName})) {
                            $errorText = "Missing Database Connection object ($dynamicResourceDbObjName)";
                        }

                        if (empty($errorText)) {
                            $apiAction->callMethod($callMethod, true, $dynamicModuleResourcesFound, [$apiAction->{$dynamicResourceSystemDBObjName}, $apiAction->{$dynamicResourceConfigDBObjName}, $apiAction->{$dynamicResourceDbObjName}, $tablePrefix], true);
                        }
                    }
                }
            }

            self::jsonErrorExit([
                'success' => false,
                'errorText' => is_null($errorText) ? 'API is not found' : $errorText
            ], 401);

        } catch (Exception|Throwable $th) {
            self::jsonErrorExit([
                'success' => false,
                'errorText' => $th->getMessage()
            ], $th->getCode());

        }

        exit;
    }

    /**
     * @param string $method
     * @param bool $ignoreCheck
     * @return bool
     */
    public static function isMethodDefinedDirectlyInClass(string $method, bool $ignoreCheck = false): bool
    {
        if ($ignoreCheck) {
            return true;
        }

        try {
            $method = (new ReflectionClass(static::class))->getMethod($method);
            return $method->class === static::class;
        } catch (Exception) {
            return false;
        }
    }

    /**
     * @param array $output
     * @param mixed $errorCode
     * @return void
     */
    public static function jsonErrorExit(array $output, mixed $errorCode = 400): void
    {
        if (in_array($errorCode, array_keys(self::$http_status_codes))) {
            http_response_code($errorCode);
        } else {
            http_response_code(500);
        }

        header('Content-Type: application/json');
        echo json_encode($output);
    }

    /**
     * @return string
     */
    public function test(): string
    {
        return 'test worked!';
    }

    /**
     * @param string $method
     * @param bool $ignoreDirectCheck
     * @return bool
     */
    public function testMethod(string $method, bool $ignoreDirectCheck = false): bool
    {
        if (method_exists($this, $method)) {
            if (static::isMethodDefinedDirectlyInClass($method, $ignoreDirectCheck)) {
                $reflection = new ReflectionMethod($this, $method);
                if ($reflection->isPublic()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param string $method
     * @param bool $exit
     * @param bool $ignoreDirectCheck
     * @param array $methodArgs
     * @param bool $isAPI
     * @return array|null
     */
    public function callMethod(string $method, bool $exit = true, bool $ignoreDirectCheck = false, array $methodArgs = [], bool $isAPI = false): ?array
    {
        try {
            if ($this->testMethod($method, $ignoreDirectCheck)) {
                return $this->echoSuccessAndExit(call_user_func_array([$this, $method], $methodArgs), $exit);
            } else {
                if ($isAPI) {
                    $errorText = 'Unauthorized or Private API';
                    $errorCode = 404;
                } else {
                    $errorText = 'Unauthorized or Private Method';
                    $errorCode = 401;
                }

                return $this->echoErrorAndExit($errorText, $errorCode, $exit);
            }
        } catch (Exception|Throwable $e) {
            return $this->throwError($e, $exit);
        }
    }

    /**
     * @param mixed $conn
     * @param string $tableName
     * @param string $className
     * @return mixed
     */
    public function getModuleResourceId(mixed $conn, string $tableName, string $className): mixed
    {
        $id = 0;
        foreach ($this::$moduleResourceSegments as $segment) {
            $t = explode("\\$segment\\", $className, 2);
            if (!empty($t[1])) {
                $totalModuleResources = count(explode("\\", $t[1]));
                $id = $this->idValues[$totalModuleResources - 2]['id'] ?? 0;
                break;
            }
        }

        if (!empty($id)) {
            $conn->getRecord($tableName, $id);
        }

        return $id;
    }

    public function getModuleResourceIdByClassName(string $className): mixed
    {
        $id = 0;
        foreach ($this::$moduleResourceSegments as $segment) {
            $t = explode("\\$segment\\", $className, 2);
            if (!empty($t[1])) {
                $totalModuleResources = count(explode("\\", $t[1]));
                $id = $this->idValues[$totalModuleResources - 2]['id'] ?? 0;
                break;
            }
        }

        return $id;
    }

    /**
     * @param $conn
     * @param string $tableName
     * @param array $item
     * @param string|null $idField
     * @return array|mixed|null
     * @throws Throwable
     */
    public function apiCreateItem($conn, string $tableName, array $item, ?string $idField = null): mixed
    {

        if (empty($conn)) {
            return $this->echoErrorAndExit('Database connection not provided', 400);
        }

        if (empty($tableName)) {
            throw new Exception('Table name not provided', 400);
        }

        if (empty($item)) {
            throw new Exception('Item not provided');
        }

        if (!empty($idField)) {
            $insertId = $conn->insertRecord($tableName, $item, false, $idField);
        } else {
            $insertId = $conn->insertRecord($tableName, $item);
        }

        if (empty($insertId)) {
            throw new Exception('Failed to insert record');
        }

        return $this->apiGetItemById($conn, $tableName, $insertId, $idField);
    }

    /**
     * @param $conn
     * @param string $tableName
     * @param mixed $id
     * @param string|null $idField
     * @return array|bool|null
     * @throws Throwable
     */
    public function apiDeleteItem($conn, string $tableName, mixed $id, ?string $idField = null): array|bool|null
    {

        if (empty($conn)) {
            throw new Exception('Database connection not provided', 400);
        }
        if (empty($tableName)) {
            throw new Exception('Table name not provided', 400);
        }
        if (empty($id)) {
            throw new Exception('ID not provided', 400);
        }

        $item = $this->apiGetItemById($conn, $tableName, $id, $idField);
        if (empty($item)) {
            throw new Exception('Item not found', 404);
        }

        if (empty($idField)) {
            $idField = $conn->getPrimaryKey($tableName);
        }

        $conn->deleteRecord($tableName, $id);

        $item = $conn->getRows("SELECT 1 FROM $tableName WHERE $idField =?", [$id]);
        if (count($item) == 0) {
            return true;
        }

        throw new Exception('Item not deleted');
    }

    /**
     * @param $conn
     * @param string $tableName
     * @param array $orderBy
     * @param int $limit
     * @param int $offset
     * @return array|mixed|null
     * @throws Exception
     */
    public function apiGetAllItems($conn, string $tableName, array $orderBy = ['id' => 'asc'], int $limit = 0, int $offset = 0): mixed
    {

        if (empty($conn)) {
            throw new Exception('Database connection not provided', 400);
        }
        if (empty($tableName)) {
            throw new Exception('Table name not provided', 400);
        }

        return $conn->getAllRows($tableName, $orderBy, $limit, $offset);
    }

    /**
     * @param $conn
     * @param string $tableName
     * @param mixed $id
     * @param string|null $idField
     * @return array|mixed|null
     * @throws Throwable
     */
    public function apiGetItemById($conn, string $tableName, mixed $id, ?string $idField = null): mixed
    {

        if (empty($conn)) {
            throw new Exception('Database connection not provided', 400);
        }
        if (empty($tableName)) {
            throw new Exception('Table name not provided', 400);
        }
        if (empty($id)) {
            throw new Exception('ID not provided', 400);
        }

        if (!empty($idField)) {
            $item = $conn->getRecord($tableName, $id, $idField);
        } else {
            $item = $conn->getRecord($tableName, $id);
        }

        if (empty($item)) {
            throw new Exception('Item not found', 404);
        }

        return $item;
    }

    /**
     * @param $conn
     * @param string $tableName
     * @param mixed $id
     * @param array $item
     * @param string|null $idField
     * @return array|mixed|null
     * @throws Throwable
     */
    public function apiPatchItem($conn, string $tableName, mixed $id, array $item, ?string $idField = null): mixed
    {
        return $this->apiUpdateItem($conn, $tableName, $id, $item, $idField);
    }

    /**
     * @param $conn
     * @param string $tableName
     * @param mixed $id
     * @param array $item
     * @param string|null $idField
     * @return array|mixed|null
     * @throws Throwable
     */
    public function apiPutItem($conn, string $tableName, mixed $id, array $item, ?string $idField = null): mixed
    {
        return $this->apiUpdateItem($conn, $tableName, $id, $item, $idField);
    }

    /**
     * @param mixed $conn
     * @param string $tableName
     * @param array $data
     * @param string $dataMethod
     * @return array
     * @throws Throwable
     */
    public function linkRecords(mixed $conn, string $tableName, array $data, string $dataMethod = 'record_get'): array
    {
        $info = $this->initValidator();
        $responseRules = $this->validator->responseRules;
        $conn->insertRecord($tableName, $data, true);
        if (empty($dataMethod)) {
            return [];
        }
        $apiMethod = $this->apiMethod;
        $this->apiMethod = $dataMethod;
        $info['data'] = $this->$dataMethod();
        $this->apiMethod = $apiMethod;
        $this->validator->responseRules = $responseRules;

        return $this->validator->validateResponse($info);
    }

    /**
     * @param mixed $conn
     * @param string $tableName
     * @param array $dataInfo
     * @param string $dataMethod
     * @return array
     * @throws Throwable
     */
    public function linkRecordsAndValidate(mixed $conn, string $tableName, array $dataInfo, string $dataMethod = 'record_get'): array
    {
        foreach ($dataInfo['tables'] ?? [] as $table) {
            $conn->getRecord($table['table_name'], $table['record_id'], $table['primary_key'] ?? null, $table['database'] ?? null);
        }

        return $this->linkRecords($conn, $tableName, $dataInfo['data'], $dataMethod);
    }


    /**
     * @param mixed $conn
     * @param array $relationshipTypes
     * @param int $tableId
     * @param bool $withFields
     * @param bool $withRelationships
     * @param bool $resultAsArray
     * @param bool $allPolymorphicRelations
     * @param string $tablePrefix
     * @return array
     * @throws Exception
     */
    public function selectTable(mixed $conn, array $relationshipTypes, int $tableId, bool $withFields = false, bool $withRelationships = false, bool $resultAsArray = true, bool $allPolymorphicRelations = false, string $tablePrefix = 'zzz_'): array
    {
        $params = [];
        $tableQueryFields = "{$tablePrefix}tables.*,COALESCE(ztso.sort_order, JSON_ARRAY()) AS sort_order,IFNULL({$tablePrefix}databases.name,'{$conn->dbName}') AS `database`";
        $tableQueryWhere = "{$tablePrefix}tables.id=?";
        $tableQueryGroupBy = "{$tablePrefix}tables.id";
        $tableQueryJoin = '';
        if ($withFields) {
            $fieldsQ = "SELECT JSON_ARRAYAGG(q.fields) AS fields, {$tablePrefix}table_id
        FROM (
                SELECT {$tablePrefix}table_id,
                        JSON_OBJECT(
                                'id', {$tablePrefix}fields.id,
                                '{$tablePrefix}table_id', {$tablePrefix}fields.{$tablePrefix}table_id,
                                'field_name', {$tablePrefix}fields.field_name,
                                'field_nickname', {$tablePrefix}fields.field_nickname,
                                'field_type_id', {$tablePrefix}fields.field_type_id,
                                'field_length', {$tablePrefix}fields.field_length,
                                'field_decimals', {$tablePrefix}fields.field_decimals,
                                'can_insert', {$tablePrefix}fields.can_insert,
                                'can_edit', {$tablePrefix}fields.can_edit,
                                'is_required', {$tablePrefix}fields.is_required,
                                'is_auto_generated', {$tablePrefix}fields.is_auto_generated,
                                'is_nullable', {$tablePrefix}fields.is_nullable,
                                'show_in_item', {$tablePrefix}fields.show_in_item,
                                'show_in_list', {$tablePrefix}fields.show_in_list,
                                'is_name', {$tablePrefix}fields.is_name,
                                'is_description', {$tablePrefix}fields.is_description,
                                'placeholder_text', {$tablePrefix}fields.placeholder_text,
                                'description', {$tablePrefix}fields.description,
                                'sort', {$tablePrefix}fields.sort,
                                'expression',{$tablePrefix}fields.expression,
                                'created_at', {$tablePrefix}fields.created_at,
                                'updated_at', {$tablePrefix}fields.updated_at
                            ) AS fields
                FROM {$tablePrefix}fields
                        INNER JOIN {$tablePrefix}tables ON {$tablePrefix}fields.{$tablePrefix}table_id = {$tablePrefix}tables.id
                GROUP BY {$tablePrefix}fields.id ORDER BY {$tablePrefix}fields.id) AS q
        GROUP BY {$tablePrefix}table_id";
            $tableQueryFields .= ',fields.fields';
            $tableQueryGroupBy .= ",fields.{$tablePrefix}table_id";
            $tableQueryJoin .= " LEFT JOIN ($fieldsQ) AS fields ON {$tablePrefix}tables.id = fields.{$tablePrefix}table_id";
        }

        if ($withRelationships) {
            $relationshipsQ = $this->setSelectTableRelationshipsQ($conn, $tablePrefix, $relationshipTypes, $tableId, $params);
            $tableQueryFields .= ',relationships.relationships';
            $tableQueryGroupBy .= ",relationships.{$tablePrefix}table_id";
            $tableQueryJoin .= " LEFT JOIN ($relationshipsQ) AS relationships ON {$tablePrefix}tables.id = relationships.{$tablePrefix}table_id";
        }

        $params[] = $tableId;
        $tableQuery = "SELECT $tableQueryFields FROM {$tablePrefix}tables $tableQueryJoin 
             LEFT JOIN {$tablePrefix}databases ON {$tablePrefix}tables.{$tablePrefix}database_id = {$tablePrefix}databases.id 
             LEFT JOIN (SELECT {$tablePrefix}table_id,JSON_ARRAYAGG(JSON_OBJECT('{$tablePrefix}field_id', ztso.{$tablePrefix}field_id,'order_by_direction',ztso.order_by_direction,'sort',ztso.sort)) AS sort_order
                FROM {$tablePrefix}table_sort_orders ztso
                GROUP BY ztso.{$tablePrefix}table_id) ztso ON {$tablePrefix}tables.id = ztso.{$tablePrefix}table_id
             WHERE $tableQueryWhere GROUP BY $tableQueryGroupBy";
        $conn->execute("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY','')) ");
        $records = $conn->getRows($tableQuery, $params) ?? [];
        $polymorphicRelationships = [];
        if ($allPolymorphicRelations) {
            $params = [];
            $relationshipsQ = $this->setSelectTableRelationshipsQ($conn, $tablePrefix, $relationshipTypes, $tableId, $params, true);
            $polymorphicRelationships = $conn->getRows($relationshipsQ, $params) ?? [];
            $this->misc->json_decode_recursively($polymorphicRelationships);
        }

        $conn->execute("SET sql_mode=(SELECT CONCAT(@@sql_mode,',ONLY_FULL_GROUP_BY')) ");
        $this->misc->json_decode_recursively($records);
        foreach ($records as $key => $record) {
            if (count($polymorphicRelationships) > 0) {
                $polymorphicRelationshipIds = $this->misc->data_get($polymorphicRelationships, '*.relationships.*.id');
                $records[$key]['relationships'] = array_merge(array_filter($record['relationships'] ?? [], function ($v, $k) use ($polymorphicRelationshipIds) {
                    return !in_array($v->id, $polymorphicRelationshipIds);

                }, ARRAY_FILTER_USE_BOTH), ...array_column($polymorphicRelationships, 'relationships'));
            }

            if (!empty($records[$key]['relationships'])) {
                array_walk($records[$key]['relationships'], function (&$rel) {
                    usort($rel['sort_order'], function ($a, $b) {
                        return $a['sort'] - $b['sort'];
                    });
                });
            }

            if (!$resultAsArray) {
                return json_decode(json_encode($records[$key], JSON_FORCE_OBJECT), true);
            }
        }

        return $records;
    }

    /**
     * @param mixed $conn
     * @param string $tableName
     * @param array $relationships
     * @param string $pivotTableName
     * @param string $pivotColumnName
     * @param array $validFields
     * @param string $primaryKey
     * @param string|null $database
     * @param string|null $pivotDatabase
     * @param mixed|null $sessionApiClass
     * @param bool $modifyResponseRules
     * @return array
     * @throws Exception
     */
    protected function postDataAndLinkRecords(mixed $conn, string $tableName, array $relationships = [], string $pivotTableName = '', string $pivotColumnName = '', array $validFields = [], string $primaryKey = 'id', ?string $database = null, ?string $pivotDatabase = null, mixed $sessionApiClass = null, bool $modifyResponseRules = false): array
    {
        $tData = $this->request['data'] ?? [];
        $isObject = !is_array($tData) || count(array_filter(array_keys($tData), 'is_string')) > 0;
        $this->modifyPostResponseRuleType($isObject);
        $this->modifyPostResponseRuleType($isObject, false, true);
        $info = $this->initValidator();
        $data = $info['data'] ?? [];
        $data = $isObject ? [$data] : $data;
        $hasPivotTable = !empty($pivotTableName);
        if ($hasPivotTable && empty($relationships)) {
            throw new Exception('relationships data is missing', 400);
        }

        $tableIds = [];
        $existingPivotData = $relationships;
        $pivots = [];
        $restrictFields = (count($validFields) > 0);
        $tableIsPivot = ($hasPivotTable && (strtolower($tableName) == strtolower($pivotTableName)));
        $pivotColumnName = empty($pivotColumnName) ? $primaryKey : $pivotColumnName;
        foreach ($data as $d) {
            if ($tableIsPivot) {
                $d = array_merge($d, $relationships);
            }

            $d = $restrictFields ? array_filter($d, function ($v, $k) use ($validFields) {
                return in_array($k, $validFields);
            }, ARRAY_FILTER_USE_BOTH) : $d;

            $tableId = $conn->insertRecord($tableName, $d, true, $primaryKey, true, $database);
            if (!empty($sessionApiClass)) {
                $sessionApiClass->newSessionAPI($conn->getRecord($tableName, $tableId), $conn);
            }

            $existingPivotData[$pivotColumnName] = $tableId;
            unset($relationships[$pivotColumnName]);
            $pivots[] = $existingPivotData;
            $tableIds[] = $tableId;
        }

        if ($hasPivotTable) {
            if (!$tableIsPivot) {
                $conn->insertMultipleRecords($pivotTableName, $pivots, false, $pivotDatabase);
            }

            $join = 'JOIN ' . (empty($pivotDatabase) ? '' : "$pivotDatabase.") . "$pivotTableName AS ztg ON t.$primaryKey = ztg.$pivotColumnName";
            $where = 'ztg.' . implode('=? AND ztg.', array_keys($relationships)) . "=? AND ztg.$pivotColumnName";
        } else {
            $join = '';
            $where = "t.$primaryKey";
            $relationships = [];
        }

        $tableData = $conn->getRows('SELECT t.* FROM ' . (empty($database) ? '' : "$database.") . "$tableName AS t $join WHERE $where IN(?" . str_repeat(',?', count($tableIds) - 1) . ')', array_merge(array_values($relationships), $tableIds));
        $info['data'] = ($isObject ? $tableData[0] : $tableData);

        return $this->validator->validateResponse($info);
    }

    /**
     * @param bool $isObject
     * @param bool $isResponse
     * @param bool $skip
     * @return void
     */
    public function modifyPostResponseRuleType(bool $isObject = false, bool $skip = false, bool $isResponse = false): void
    {
        if ($skip) {
            return;
        }

        $ruleType = $isResponse ? 'ResponseRules' : 'RequestRules';
        if (($dataKey = array_search('data', array_column($this->{$this->apiMethod . $ruleType}, 'field'))) !== false) {
            if ($isObject) {
                $this->{$this->apiMethod . $ruleType}[$dataKey]['type'] = 'object';
            } else {
                $this->{$this->apiMethod . $ruleType}[$dataKey]['type'] = 'array';
                $this->{$this->apiMethod . $ruleType}[$dataKey]['format'] = 'multi_array';
            }

            $this->{$this->apiMethod . $ruleType}[$dataKey]['min'] = 1;

        } elseif ($isObject) {
            $this->{$this->apiMethod . $ruleType}[] = ['field' => 'data', 'type' => 'object', 'required' => true, 'min' => 1];
        } else {
            $this->{$this->apiMethod . $ruleType}[] = ['field' => 'data', 'type' => 'array', 'format' => 'multi_array', 'required' => true, 'min' => 1];
        }
    }

    /**
     * @param mixed $conn
     * @param string $tableName
     * @param string $primaryKey
     * @param string|null $database
     * @param bool $skipRecordValidation
     * @param array $pivotColumnIds
     * @return array
     * @throws Exception
     */
    protected function patchData(mixed $conn, string $tableName, string $primaryKey = 'id', ?string $database = null, bool $skipRecordValidation = false, array $pivotColumnIds = []): array
    {
        $data = $this->initValidator();
        $tAllIds = array_column($data['data'], $primaryKey);
        if (count($tAllIds) !== count($data['data'])) {
            throw new Exception('Primary key is not provided in one or more data objects', 400);
        }

        $allIds = array_values(array_unique($tAllIds));
        $totalAllIds = count($allIds);
        $tTableName = (empty($database) ? '' : "$database.") . $tableName;
        if (!$skipRecordValidation) {
            $where = empty($pivotColumnIds) ? '' : 'AND ' . implode(' AND ', array_map(fn($key, $value) => "$key=?", array_keys($pivotColumnIds), $pivotColumnIds));
            $records = array_column($conn->getRows("SELECT $tTableName.* FROM $tTableName WHERE $primaryKey IN(?" . str_repeat(',?', $totalAllIds - 1) . ") $where", array_merge($allIds, array_values($pivotColumnIds))) ?? [], null, $primaryKey);
            if (count($records) != $totalAllIds) {
                throw new Exception('Records not found', 404);
            }

            foreach ($data['data'] as &$d) {
                $d = array_merge($records[$d[$primaryKey]], $d);
            }
        }

        $conn->insertMultipleRecords($tableName, $data['data'], true, $database);

        return $this->validator->validateResponse(['data' => $conn->getRows("SELECT DISTINCT * FROM $tTableName WHERE $primaryKey IN(?" . str_repeat(',?', $totalAllIds - 1) . ')', $allIds) ?? []]);
    }

    /**
     * @throws Throwable
     */
    protected function recordPatchData(mixed $conn, string $tableName, mixed $recordId, string $primaryKey = 'id', ?string $database = null, array $pivotColumnIds = []): array
    {
        $data = $this->initValidator();
        $where = empty($pivotColumnIds) ? '' : 'AND ' . implode(' AND ', array_map(fn($key, $value) => "$key=?", array_keys($pivotColumnIds), $pivotColumnIds));
        if (count($conn->getRows("SELECT $primaryKey FROM $tableName WHERE $primaryKey =? $where", array_merge([$recordId], array_values($pivotColumnIds))) ?? []) == 0) {
            throw new Exception('Record not found', 404);
        }

        $conn->updateRecord($tableName, $recordId, $data['data'], $primaryKey, true, $database);

        return $this->validator->validateResponse(['data' => $conn->getRecord($tableName, $recordId, $primaryKey, $database)]);
    }

    /**
     * @throws Exception
     * @throws Throwable
     */
    protected function recordDeleteDataRecords(mixed $conn, string $tableName, mixed $record, string $primaryKey = 'id', ?string $database = null, string $pivotTableName = '', string $pivotColumnName = '', array $relationships = [], ?string $pivotDatabase = null): void
    {

        $tIds = $this->initValidator();
        if (empty($tIds['ids'])) {
            $conn->getRecord($tableName, $record, $primaryKey, $database);
            $tIds['ids'] = [$record];
        }

        $ids = $tIds['ids'];
        $hasPivotTable = !empty($pivotTableName);
        foreach ($ids as $id) {
            if ($hasPivotTable) {
                $relationships[$pivotColumnName] = $id;
                $conn->execute('DELETE FROM ' . (empty($pivotDatabase) ? '' : "$pivotDatabase.") . "$pivotTableName WHERE $pivotTableName." . implode("=? AND $pivotTableName.", array_keys($relationships)) . '=?', array_values($relationships));
            } else {
                $conn->deleteRecord($tableName, $id, $primaryKey, $database);
            }
        }
    }

    protected function setRulesAndNotes(array $post = [], array $patch = [], array $get = [], array $delete = []): void
    {
        $this->postRequestRules = $post['request'] ?? [];
        $this->postResponseRules = $post['response'] ?? [];
        $this->postQueryParamRules = $post['query'] ?? [];
        $this->postNotes = $post['note'] ?? '';
        $this->postHeader = $post['header'] ?? '';
        $this->record_postRequestRules = $post['record_request'] ?? [];
        $this->record_postResponseRules = $post['record_response'] ?? [];
        $this->record_postQueryParamRules = $post['record_query'] ?? [];
        $this->record_postNotes = $post['record_note'] ?? '';
        $this->record_postHeader = $post['record_header'] ?? '';

        $this->patchRequestRules = $patch['request'] ?? [];
        $this->patchResponseRules = $patch['response'] ?? [];
        $this->patchQueryParamRules = $patch['query'] ?? [];
        $this->patchNotes = $patch['note'] ?? '';
        $this->patchHeader = $patch['header'] ?? '';
        $this->record_patchRequestRules = $patch['record_request'] ?? [];
        $this->record_patchResponseRules = $patch['record_response'] ?? [];
        $this->record_patchQueryParamRules = $patch['record_query'] ?? [];
        $this->record_patchNotes = $patch['record_note'] ?? '';
        $this->record_patchHeader = $patch['record_header'] ?? '';

        $this->getRequestRules = $get['request'] ?? [];
        $this->getResponseRules = $get['response'] ?? [];
        $this->getQueryParamRules = $get['query'] ?? [];
        $this->getNotes = $get['note'] ?? '';
        $this->getHeader = $get['header'] ?? '';
        $this->record_getRequestRules = $get['record_request'] ?? [];
        $this->record_getResponseRules = $get['record_response'] ?? [];
        $this->record_getQueryParamRules = $get['record_query'] ?? [];
        $this->record_getNotes = $get['record_note'] ?? '';
        $this->record_getHeader = $get['record_header'] ?? '';

        $this->deleteRequestRules = $delete['request'] ?? [];
        $this->deleteResponseRules = $delete['response'] ?? [];
        $this->deleteQueryParamRules = $delete['query'] ?? [];
        $this->deleteNotes = $delete['note'] ?? '';
        $this->deleteHeader = $delete['header'] ?? '';
        $this->record_deleteRequestRules = $delete['record_request'] ?? [];
        $this->record_deleteResponseRules = $delete['record_response'] ?? [];
        $this->record_deleteQueryParamRules = $delete['record_query'] ?? [];
        $this->record_deleteNotes = $delete['record_note'] ?? '';
        $this->record_deleteHeader = $delete['record_header'] ?? '';
    }

    /**
     * @param array $rules
     * @param array $requiredFields eg ['post'=>['request'=>'response'],'get'=>[]]
     * @param array $removeFields eg ['post'=>[],'get'=>[]]
     * @return array
     */
    protected function updateFieldRules(array $rules = [], array $requiredFields = [], array $removeFields = []): array
    {
        $response = [];
        if (!empty($removeFields) || !empty($requiredFields)) {
            foreach ($rules as $k => $rule) {
                $field = $rule['field'] ?? '';
                foreach ($requiredFields as $i => $requiredField) {
                    $response[$i] ??= $rules;
                    if (in_array($field, $requiredField)) {
                        $response[$i][$k] ??= $rule;
                        $response[$i][$k]['required'] = true;
                    }
                }

                foreach ($removeFields as $i => $removeField) {
                    $response[$i] ??= $rules;
                    if (in_array($field, $removeField)) {
                        unset($response[$i][$k]);
                    }
                }
            }
        }

        return $response;
    }

    /**
     * @param mixed $info
     * @param bool $exit
     * @return array|null
     */
    protected function echoSuccessAndExit(mixed $info, bool $exit = true): ?array
    {
        $output = [
            'success' => true,
            'info' => $info,
            'extras' => $this->extras
        ];

        if ($this->debug) {
            $output['request'] = array_merge($this->request, $this->queryParams);
            print_r($output);
        } else {
            header('Content-Type: application/json');
            echo json_encode($output);
        }

        if ($exit) {
            exit;
        } else {
            return $output;
        }
    }

    /**
     * @param Exception|Throwable $th
     * @param bool $exit
     * @return array|null
     */
    protected function throwError(Exception|Throwable $th, bool $exit = false): ?array
    {
        return $this->echoErrorAndExit($th->getMessage(), $th->getCode(), $exit);
    }

    /**
     * @param string $errorText
     * @param mixed $errorCode
     * @param bool $exit
     * @return array|null
     */
    protected function echoErrorAndExit(string $errorText = '', mixed $errorCode = 500, bool $exit = true): ?array
    {
        $output = [
            'success' => false,
            'errorText' => $errorText,
            'request' => array_merge($this->request, $this->queryParams),
            'extras' => $this->extras,
            'httpMethod' => $this->httpMethod
        ];
        if ($this->debug) {
            print_r($output);
        } else {
            self::jsonErrorExit($output, $errorCode);
        }

        if ($exit) {
            exit;
        } else {
            return $output;
        }
    }

    protected function getBearerToken(): ?string
    {
        $authHeader = $this->getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($authHeader)) {
            if (preg_match('/^Bearer\s(\S+)/', $authHeader, $matches)) {
                return $matches[1];
            }
        }

        return null;
    }

    /**
     * Get header Authorization
     * */
    private function getAuthorizationHeader(): ?string
    {
        $authHeader = null;
        if (isset($_SERVER['Authorization'])) {
            $authHeader = trim($_SERVER['Authorization']);
        } elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $authHeader = trim($_SERVER['HTTP_AUTHORIZATION']);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            if (isset($requestHeaders['Authorization'])) {
                $authHeader = trim($requestHeaders['Authorization']);
            }
        }
        return $authHeader;
    }

    /**
     * @param $conn
     * @param string $tableName
     * @param $id
     * @param array $item
     * @param string|null $idField
     * @return array|mixed|null
     * @throws Throwable
     */
    private function apiUpdateItem($conn, string $tableName, $id, array $item, ?string $idField = null): mixed
    {

        if (empty($conn)) {
            return $this->echoErrorAndExit('Database connection not provided');
        }
        if (empty($tableName)) {
            return $this->echoErrorAndExit('Table name not provided');
        }
        if (empty($id)) {
            return $this->echoErrorAndExit('ID not provided');
        }
        if (empty($item)) {
            return $this->echoErrorAndExit('Item not provided');
        }

        $existingItem = $this->apiGetItemById($conn, $tableName, $id, $idField);
        if (empty($existingItem)) {
            return $this->echoErrorAndExit('Item not found', 404);
        }

        if (!empty($idField)) {
            $conn->updateRecord($tableName, $id, $item, $idField);
        } else {
            $conn->updateRecord($tableName, $id, $item);
        }

        $newItem = $this->apiGetItemById($conn, $tableName, $id, $idField);
        if (empty($newItem)) {
            return $this->echoErrorAndExit('Item not found', 404);
        }
        return $newItem;
    }

    /**
     * @param mixed $conn
     * @param string $tablePrefix
     * @param array $relationshipTypes
     * @param int $tableId
     * @param array $params
     * @param bool $allPolymorphicRelations
     * @return string
     */
    private function setSelectTableRelationshipsQ(mixed $conn, string $tablePrefix, array $relationshipTypes, int $tableId, array &$params, bool $allPolymorphicRelations = false): string
    {
        $relationshipTypeIds = array_column($relationshipTypes, 'id', 'type_name');
        $manyToManyRelTypeId = $relationshipTypeIds['many-many'] ?? 0;
        $polymorphicRelTypeId = $relationshipTypeIds['polymorphic'] ?? 0;
        $params[] = $tableId;
        $params[] = $tableId;
        $params[] = $tableId;
        $params[] = $manyToManyRelTypeId;
        $params[] = $polymorphicRelTypeId;
        $extraParams = [];
        if ($allPolymorphicRelations) {
            $ifWhere = "IF({$tablePrefix}relationships.relationship_type_id = ?,COALESCE({$tablePrefix}relationship_tables.pivot_column_name,'') !=?";
            $relationshipTablesWhere = "$ifWhere ,{$tablePrefix}relationship_tables.{$tablePrefix}table_id != ?)";
            $relationshipsWhere = 'WHERE relationship_type_id = ?';
            $where = " AND $ifWhere,1)";
            $groupBy = "{$tablePrefix}relationship_id";
            $extraParams = [$polymorphicRelTypeId, "referenced__{$tablePrefix}table_id"];
            $params = array_merge($params, $extraParams);
            $extraParams[] = $polymorphicRelTypeId;
        } else {
            $relationshipTablesWhere = "{$tablePrefix}relationship_tables.{$tablePrefix}table_id !=?";
            $groupBy = "{$tablePrefix}table_id";
            $relationshipsWhere = '';
            $where = '';
        }

        $params[] = $tableId;
        $params[] = $polymorphicRelTypeId;
        $params[] = $tableId;
        $params[] = "referenced__{$tablePrefix}table_id";
        $params[] = "referenced__{$tablePrefix}table_id";
        $params[] = $tableId;
        $params[] = "referenced__{$tablePrefix}table_id";
        $params[] = $polymorphicRelTypeId;
        $params[] = $tableId;
        $params[] = $tableId;
        $params[] = $tableId;
        $params[] = $manyToManyRelTypeId;
        $params[] = $polymorphicRelTypeId;
        $params = array_merge($params, $extraParams);
        $relationshipTablesQ = "SELECT DISTINCT JSON_ARRAYAGG(q.tables) AS tables, {$tablePrefix}relationship_id
        FROM (SELECT DISTINCT {$tablePrefix}relationship_tables.{$tablePrefix}relationship_id,
                    JSON_OBJECT(
                            'id', {$tablePrefix}tables.id,
                            'table_name', {$tablePrefix}tables.table_name,
                            'database', IFNULL({$tablePrefix}databases.name,'{$conn->dbName}'),
                            'primary_key', {$tablePrefix}tables.primary_key,
                            'pivot_column_name', {$tablePrefix}tables.pivot_column_name,
                            'table_type_id', {$tablePrefix}tables.table_type_id,
                            '{$tablePrefix}relationship_table_id', {$tablePrefix}relationship_tables.id,
                            'if_deleted', {$tablePrefix}relationship_tables.if_deleted,
                            'if_updated', {$tablePrefix}relationship_tables.if_updated,
                            'pivot_table_name', pivot_table.table_name,
                            'pivot_database', IF(pivot_table.table_name IS NULL,NULL,IFNULL(pivot_database.name, '{$conn->dbName}')),
                            'relationship_pivot_column_name',IF({$tablePrefix}relationship_tables.pivot_column_name IS NULL OR {$tablePrefix}relationship_tables.pivot_column_name='', {$tablePrefix}tables.pivot_column_name,{$tablePrefix}relationship_tables.pivot_column_name),
                            'custom_relationship_pivot_column_name',IF({$tablePrefix}relationship_tables.pivot_column_name IS NULL OR {$tablePrefix}relationship_tables.pivot_column_name='', false,true),
                            'pivot_table_id', pivot_table.id,
                            'description', {$tablePrefix}tables.description,
                            'name_singular_capitalized', {$tablePrefix}tables.name_singular_capitalized,
                            'name_singular_uncapitalized', {$tablePrefix}tables.name_singular_uncapitalized,
                            'name_plural_capitalized', {$tablePrefix}tables.name_plural_capitalized,
                            'name_plural_uncapitalized', {$tablePrefix}tables.name_plural_uncapitalized,
                            'sort', {$tablePrefix}tables.sort,
                            'created_at', {$tablePrefix}tables.created_at,
                            'updated_at', {$tablePrefix}tables.updated_at,
                            'referenced', IF(ISNULL(referenced_table.id), NULL, JSON_OBJECT(
                            'id', referenced_table.id,
                            'table_name', referenced_table.table_name,
                            'database', IFNULL(referenced_table_databases.name,'{$conn->dbName}'),
                            'primary_key', referenced_table.primary_key,
                            'pivot_column_name', referenced_table.pivot_column_name,
                            'relationship_pivot_column_name',IF(referenced_relationship_tables.pivot_column_name IS NULL OR referenced_relationship_tables.pivot_column_name='', referenced_table.pivot_column_name,referenced_relationship_tables.pivot_column_name),
                            'custom_relationship_pivot_column_name',IF(referenced_relationship_tables.pivot_column_name IS NULL OR referenced_relationship_tables.pivot_column_name='', false,true),
                            'table_type_id', referenced_table.table_type_id,
                            'description', referenced_table.description,
                            'name_singular_capitalized', referenced_table.name_singular_capitalized,
                            'name_singular_uncapitalized', referenced_table.name_singular_uncapitalized,
                            'name_plural_capitalized', referenced_table.name_plural_capitalized,
                            'name_plural_uncapitalized', referenced_table.name_plural_uncapitalized,
                            'sort', referenced_table.sort,
                            'created_at', referenced_table.created_at,
                            'updated_at', referenced_table.updated_at))) AS tables
            FROM {$tablePrefix}relationship_tables
                    INNER JOIN {$tablePrefix}tables ON
                {$tablePrefix}tables.id = {$tablePrefix}relationship_tables.{$tablePrefix}table_id
                    INNER JOIN {$tablePrefix}relationships ON
                {$tablePrefix}relationship_tables.{$tablePrefix}relationship_id = {$tablePrefix}relationships.id
                    LEFT JOIN {$tablePrefix}tables AS referenced_table
                                ON {$tablePrefix}relationship_tables.referenced_table = referenced_table.id
                    LEFT JOIN {$tablePrefix}tables AS pivot_table ON
                {$tablePrefix}relationships.pivot_table_id = pivot_table.id 
                    LEFT JOIN {$tablePrefix}databases ON {$tablePrefix}tables.{$tablePrefix}database_id = {$tablePrefix}databases.id
                    LEFT JOIN {$tablePrefix}databases referenced_table_databases ON referenced_table.{$tablePrefix}database_id = referenced_table_databases.id
                    LEFT JOIN {$tablePrefix}databases pivot_database ON pivot_table.{$tablePrefix}database_id = pivot_database.id 
                    LEFT JOIN (SELECT referenced_relationship_tables.id,
                                 referenced_relationship_tables.pivot_column_name,
                                 referenced_relationship_tables.{$tablePrefix}table_id,
                                 referenced_relationship_tables.{$tablePrefix}relationship_id
                          FROM {$tablePrefix}relationship_tables AS referenced_relationship_tables) referenced_relationship_tables
                         ON referenced_relationship_tables.{$tablePrefix}relationship_id = {$tablePrefix}relationships.id AND
                            {$tablePrefix}relationship_tables.referenced_table = referenced_relationship_tables.{$tablePrefix}table_id 
            WHERE IF({$tablePrefix}relationship_tables.{$tablePrefix}table_id=? AND (SELECT COUNT(same_table_relationship.id) FROM {$tablePrefix}relationship_tables same_table_relationship WHERE same_table_relationship.{$tablePrefix}relationship_id={$tablePrefix}relationships.id AND same_table_relationship.{$tablePrefix}table_id=?)>1,IF({$tablePrefix}relationships.relationship_type_id IN(?,?),referenced_table.id IS NULL,referenced_table.id IS NOT NULL AND referenced_relationship_tables.id != {$tablePrefix}relationship_tables.id),$relationshipTablesWhere)
            GROUP BY {$tablePrefix}relationship_tables.{$tablePrefix}relationship_id, {$tablePrefix}tables.id, referenced_table.id,
                    {$tablePrefix}relationship_tables.id,referenced_relationship_tables.id) AS q
        GROUP BY {$tablePrefix}relationship_id";

        return "SELECT DISTINCT JSON_ARRAYAGG(q.relationships) AS relationships, $groupBy
    FROM (SELECT {$tablePrefix}relationship_tables.$groupBy,{$tablePrefix}relationships.relationship_type_id,
                 JSON_OBJECT(
                         'id', {$tablePrefix}relationships.id,
                         'limit', COALESCE(ANY_VALUE(zrt2.limit), ANY_VALUE(zrt.limit)),
                         'sort_order', COALESCE(ANY_VALUE(zrso2.sort_order), ANY_VALUE(zrso.sort_order), JSON_ARRAY()),
                         '{$tablePrefix}relationship_table_id', {$tablePrefix}relationship_tables.id,
                         'relationship_type_id', {$tablePrefix}relationships.relationship_type_id,
                         'use_pivot_table', {$tablePrefix}relationships.use_pivot_table,
                         'custom_pivot_table_name',{$tablePrefix}relationships.custom_pivot_table_name,
                         'nickname', {$tablePrefix}relationships.nickname,
                         'referenced_table', {$tablePrefix}relationship_tables.referenced_table,
                         'if_deleted', {$tablePrefix}relationship_tables.if_deleted,
                         'if_updated', {$tablePrefix}relationship_tables.if_updated,
                         'pivot_table_name', pivot_table.table_name,
                         'pivot_table_id', pivot_table.id,
                         'pivot_database', IF(pivot_table.table_name IS NULL,NULL,IFNULL(pivot_database.name, '{$conn->dbName}')),
                         'description', {$tablePrefix}relationships.description,
                         'sort', {$tablePrefix}relationships.sort,
                         'created_at', {$tablePrefix}relationships.created_at,
                         'updated_at', {$tablePrefix}relationships.updated_at,
                         'referenced', IF(ISNULL(referenced_table.id) OR {$tablePrefix}relationship_tables.referenced_table=?, NULL, JSON_OBJECT(
                         'id', referenced_table.id,
                         'table_name', referenced_table.table_name,
                         'database', IFNULL({$tablePrefix}databases.name,'{$conn->dbName}'),
                         'primary_key', referenced_table.primary_key,
                         'pivot_column_name', referenced_table.pivot_column_name,
                         'table_type_id', referenced_table.table_type_id,
                         'description', referenced_table.description,
                         'name_singular_capitalized', referenced_table.name_singular_capitalized,
                         'name_singular_uncapitalized', referenced_table.name_singular_uncapitalized,
                         'name_plural_capitalized', referenced_table.name_plural_capitalized,
                         'name_plural_uncapitalized', referenced_table.name_plural_uncapitalized,
                         'sort', referenced_table.sort,
                         'created_at', referenced_table.created_at,
                         'updated_at', referenced_table.updated_at)),
                         'relationship_pivot_column_name', IF({$tablePrefix}relationship_tables.pivot_column_name IS NULL OR {$tablePrefix}relationship_tables.pivot_column_name = '',{$tablePrefix}tables.pivot_column_name,{$tablePrefix}relationship_tables.pivot_column_name),
                         'custom_relationship_pivot_column_name',IF({$tablePrefix}relationship_tables.pivot_column_name IS NULL OR {$tablePrefix}relationship_tables.pivot_column_name = '', false,true),
                         'tables', relationship_tables.tables) AS relationships
          FROM {$tablePrefix}relationship_tables
                   INNER JOIN {$tablePrefix}relationships ON
              {$tablePrefix}relationship_tables.{$tablePrefix}relationship_id = {$tablePrefix}relationships.id
                   INNER JOIN {$tablePrefix}tables ON
              {$tablePrefix}relationship_tables.{$tablePrefix}table_id = {$tablePrefix}tables.id
                   LEFT JOIN {$tablePrefix}tables AS referenced_table ON
              {$tablePrefix}relationship_tables.referenced_table = referenced_table.id
                   LEFT JOIN {$tablePrefix}tables AS pivot_table
                             ON {$tablePrefix}relationships.pivot_table_id = pivot_table.id
                   LEFT JOIN {$tablePrefix}databases ON referenced_table.{$tablePrefix}database_id = {$tablePrefix}databases.id
                   LEFT JOIN {$tablePrefix}databases pivot_database ON pivot_table.{$tablePrefix}database_id = pivot_database.id
                   LEFT JOIN ($relationshipTablesQ) AS relationship_tables ON
                  {$tablePrefix}relationship_tables.{$tablePrefix}relationship_id = relationship_tables.{$tablePrefix}relationship_id 
                  LEFT JOIN {$tablePrefix}relationship_tables zrt2 ON {$tablePrefix}relationships.relationship_type_id = ? AND zrt2.{$tablePrefix}relationship_id = {$tablePrefix}relationships.id AND zrt2.id = 
                    (IF((SELECT DISTINCT zrt2.{$tablePrefix}relationship_id FROM {$tablePrefix}relationship_tables WHERE {$tablePrefix}relationship_tables.{$tablePrefix}relationship_id = zrt2.{$tablePrefix}relationship_id AND 
                        {$tablePrefix}relationship_tables.{$tablePrefix}table_id = ? AND COALESCE({$tablePrefix}relationship_tables.pivot_column_name, '') != ?) = zrt2.{$tablePrefix}relationship_id, {$tablePrefix}relationship_tables.id, (
                        SELECT id FROM {$tablePrefix}relationship_tables WHERE {$tablePrefix}relationship_tables.{$tablePrefix}relationship_id = zrt2.{$tablePrefix}relationship_id AND COALESCE({$tablePrefix}relationship_tables.pivot_column_name, '') = ?)))
                  LEFT JOIN (SELECT {$tablePrefix}relationship_table_id,JSON_ARRAYAGG(JSON_OBJECT('{$tablePrefix}field_id', zrso2.{$tablePrefix}field_id,'order_by_direction',zrso2.order_by_direction,'sort',zrso2.sort,'sort_table_id',zf.{$tablePrefix}table_id)) AS sort_order
                    FROM {$tablePrefix}relationship_sort_orders zrso2 
                    INNER JOIN {$tablePrefix}fields zf ON zrso2.{$tablePrefix}field_id = zf.id
                    GROUP BY zrso2.{$tablePrefix}relationship_table_id) zrso2 ON IF(zrt2.{$tablePrefix}table_id = ? AND COALESCE({$tablePrefix}relationship_tables.pivot_column_name, '') != ?,0,zrt2.id) = zrso2.{$tablePrefix}relationship_table_id
                  LEFT JOIN {$tablePrefix}relationship_tables zrt ON {$tablePrefix}relationships.relationship_type_id <> ? AND zrt.{$tablePrefix}relationship_id={$tablePrefix}relationships.id AND zrt.id = (IF({$tablePrefix}relationship_tables.{$tablePrefix}table_id=?, {$tablePrefix}relationship_tables.id,
                    IF(JSON_VALUE(relationship_tables.tables, '$[*].id')=?,JSON_VALUE(relationship_tables.tables, '$[*].{$tablePrefix}relationship_table_id'),0)))
                  LEFT JOIN (SELECT {$tablePrefix}relationship_table_id,JSON_ARRAYAGG(JSON_OBJECT('{$tablePrefix}field_id', zrso.{$tablePrefix}field_id,'order_by_direction',zrso.order_by_direction,'sort',zrso.sort,'sort_table_id',zf.{$tablePrefix}table_id)) AS sort_order
                    FROM {$tablePrefix}relationship_sort_orders zrso
                    INNER JOIN {$tablePrefix}fields zf ON zrso.{$tablePrefix}field_id = zf.id
                    GROUP BY zrso.{$tablePrefix}relationship_table_id) zrso ON zrt.id = zrso.{$tablePrefix}relationship_table_id
                  WHERE IF(JSON_VALUE(relationship_tables.tables,'$[*].id')=?,IF({$tablePrefix}relationships.relationship_type_id IN(?,?),referenced_table.id IS NULL, JSON_VALUE(relationship_tables.tables,'$[0].{$tablePrefix}relationship_table_id') = {$tablePrefix}relationship_tables.id), 1) $where
          GROUP BY {$tablePrefix}relationship_tables.id, relationship_tables.{$tablePrefix}relationship_id,{$tablePrefix}relationships.relationship_type_id ORDER BY relationship_tables.{$tablePrefix}relationship_id) AS q
          $relationshipsWhere GROUP BY $groupBy";
    }

    /**
     * @return string
     */
    private function get_base_url(): string
    {
        if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $protocol = 'https://';
        } else {
            $protocol = 'http://';
        }

        return $protocol . $_SERVER['SERVER_NAME'];
    }
}