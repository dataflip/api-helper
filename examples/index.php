<?php
include_once '../vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', '1');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, AUTHORIZATION ');

$request = json_decode(file_get_contents('php://input'), true);
$errorText = null;
foreach ($_REQUEST as $key => $req) {
    if (!isset($request[$key])) {
        switch (true) {
            case filter_var($req, FILTER_VALIDATE_INT):
                settype($req, 'integer');
                break;
            case filter_var($req, FILTER_VALIDATE_FLOAT) :
                settype($req, 'float');
                break;
            case filter_var($req, FILTER_VALIDATE_BOOLEAN) :
                settype($req, 'boolean');
                break;
        }

        $request[$key] = $req;
    }
}

if (isset($request['ApiMethodPath'])) {
    $url = $request['ApiMethodPath'];
    unset($request['ApiMethodPath']);
    $methodPathArray = explode(DIRECTORY_SEPARATOR, $url);
    $pathSplitCount = count($methodPathArray);
    $lastPart = '';
    $tApiMethodClass = '';
    if ($pathSplitCount == 2) {
        $lastPart = '/index';
        $tApiMethodClass = 'Index';
    }

    if ($pathSplitCount >= 2) {
        $tClass = substr($url, 0, strrpos($url, DIRECTORY_SEPARATOR));
        if (empty($tApiMethodClass)) {
            $tClassArr = explode(DIRECTORY_SEPARATOR, $tClass);
            $tApiMethodClass = (end($tClassArr));
        }

        $apiMethodClassPath = dirname(__FILE__) . DIRECTORY_SEPARATOR . $tClass . $lastPart . '.php';
        if (file_exists($apiMethodClassPath)) {
            require_once($apiMethodClassPath);
            $urlArray = array_filter(explode(DIRECTORY_SEPARATOR, preg_replace('/[_-]/', '', ucwords($_SERVER['REDIRECT_URL'],'/[_-]/'))));
            array_pop($urlArray);
            if (!empty($lastPart)) {
                $urlArray[] = $tApiMethodClass;
            }

            $urlArray = array_map('ucfirst', $urlArray);
            $apiMethodClass = implode("\\", $urlArray);
            if (class_exists($apiMethodClass)) {
                $apiAction = new $apiMethodClass([
                    'request' => $request,
                    'httpMethod' => $_SERVER['REQUEST_METHOD']
                ]);
                $apiAction->callMethod(end($methodPathArray));
            }
        }
    }
}

header('Content-Type: application/json');
echo json_encode([
    'success' => false,
    'errorText' => is_null($errorText) ? 'Unauthorized or Private Method' : $errorText
]);
http_response_code(401);
exit;
