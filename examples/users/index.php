<?php

namespace Examples\Users;

use DataBackFlip\ApiHelper;
use Exception;
use Throwable;

class Index extends ApiHelper
{

    /**
     * The api route will be /examples/users/user
     * @return void
     * @throws Exception
     */
    public function user(): void
    {
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'POST':
                $this->post();
                break;
            case 'GET':
                $this->get();
                break;
            case 'PATCH':
                $this->patch();
                break;
            case 'DELETE':
                $this->delete();
                break;
            default:
                $this->echoErrorAndExit('Method Not Allowed', 405);
        }
    }

    /**
     * @return void $response
     */
    private function post(): void
    {
        try {
            $this->validator->init(
                [
                    ['field' => 'first_name', 'type' => 'string', 'required' => true],
                    ['field' => 'last_name', 'type' => 'string', 'required' => true],
                    ['field' => 'age', 'type' => 'integer', 'required' => true]
                ],
                [
                    ['field' => 'id', 'type' => 'integer', 'required' => true],
                    ['field' => 'first_name', 'type' => 'string', 'required' => true],
                    ['field' => 'last_name', 'type' => 'string', 'required' => true],
                    ['field' => 'age', 'type' => 'integer', 'required' => true]
                ],
                'POST',
                []
            );

            $request = $this->validator->validateRequest($this->request, []);

            $response = [
                'id' => 1,
                'first_name' => $request['first_name'],
                'last_name' => $request['last_name'],
                'age' => $request['age']
            ];

            $this->echoSuccessAndExit($this->validator->validateResponse($response, []));
        } catch (Throwable|Exception $th) {
            $this->throwError($th);
        }
    }

    /**
     * @return void
     */
    private function get(): void
    {
        try {
            $this->validator->init(
                [
                    ['field' => 'id', 'type' => 'integer', 'required' => true],
                ],
                [
                    ['field' => 'id', 'type' => 'integer', 'required' => true],
                    ['field' => 'first_name', 'type' => 'string', 'required' => true],
                    ['field' => 'last_name', 'type' => 'string', 'required' => true],
                    ['field' => 'age', 'type' => 'integer', 'required' => true],
                ],
                'GET',
                []
            );

            $request = $this->validator->validateRequest($this->request, []);
            $response = [
                'id' => $request['id'],
                'first_name' => 'John ' . $request['id'],
                'last_name' => 'Doe ' . $request['id'],
                'age' => 20 + $request['id']
            ];
            $this->echoSuccessAndExit($this->validator->validateResponse($response, []));
        } catch (Throwable|Exception $th) {
            $this->throwError($th);
        }
    }

    /**
     * @return void
     */
    private function patch(): void
    {
        try {
            $this->validator->init(
                [
                    ['field' => 'id', 'type' => 'integer', 'required' => true],
                    ['field' => 'first_name', 'type' => 'string'],
                    ['field' => 'last_name', 'type' => 'string'],
                    ['field' => 'age', 'type' => 'integer']
                ],
                [
                    ['field' => 'id', 'type' => 'integer', 'required' => true],
                    ['field' => 'first_name', 'type' => 'string', 'required' => true],
                    ['field' => 'last_name', 'type' => 'string', 'required' => true],
                    ['field' => 'age', 'type' => 'integer', 'required' => true]
                ],
                'PATCH',
                []
            );

            $request = $this->validator->validateRequest($this->request, []);
            $response = [
                'id' => $request['id'],
                'first_name' => $request['first_name'] ?? 'John ' . $request['id'],
                'last_name' => $request['last_name'] ?? 'Doe ' . $request['id'],
                'age' => $request['age'] ?? 20 + $request['id']
            ];
            $this->echoSuccessAndExit($this->validator->validateResponse($response, []));
        } catch (Throwable|Exception $th) {
            $this->throwError($th);
        }
    }

    /**
     * @return void
     */
    private function delete(): void
    {
        try {
            $this->validator->init(
                [
                    ['field' => 'id', 'type' => 'integer', 'required' => true]
                ],
                [
                    ['field' => 'response', 'type' => 'string', 'required' => true]
                ],
                'DELETE',
                []
            );

            $request = $this->validator->validateRequest($this->request, []);
            $response = [
                'response' => "user with id {$request['id']} has been deleted"
            ];
            $this->echoSuccessAndExit($this->validator->validateResponse($response, []));
        } catch (Throwable|Exception $th) {
            $this->throwError($th);
        }
    }
}
