<?php

namespace api\Examples;

use ApiTester;
use Codeception\Test\Unit;

class AdminTest extends Unit
{
    /**
     * @var ApiTester
     */
    protected ApiTester $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function test0010CreateUserPass()
    {
        $this->tester->haveHttpHeader("Content-Type", "application/json");
        $this->tester->sendPost("/users/admin/user", [
            "first_name" => "John",
            "last_name" => "Doe",
            "age" => 21
        ]);
        $this->tester->seeResponseIsJson();
        $this->tester->seeResponseContainsJson(["success" => true]);
        $this->tester->seeResponseMatchesJsonType(["info" => "array"]);
        $this->tester->seeResponseMatchesJsonType([
            "id" => "integer",
            "first_name" => "string",
            "last_name" => "string",
            "age" => "integer"
        ], "$.info");
    }

    public function test0020CreateUserFailMissingParameter()
    {
        $this->tester->haveHttpHeader("Content-Type", "application/json");
        $this->tester->sendPost("/users/admin/user", [
            "first_name" => "John",
            "last_name" => "Doe"
        ]);
        $this->tester->seeResponseIsJson();
        $this->tester->seeResponseContainsJson(["success" => false]);
        $this->tester->seeResponseMatchesJsonType(["errorText" => 'string:regex(/([\w\s"]*) is not provided$/)']);
    }

    public function test0030CreateUserFailBadParameterType()
    {
        $this->tester->haveHttpHeader("Content-Type", "application/json");
        $this->tester->sendPost("/users/admin/user", [
            "first_name" => "John",
            "last_name" => "Doe",
            "age" => "21"
        ]);
        $this->tester->seeResponseIsJson();
        $this->tester->seeResponseContainsJson(["success" => false]);
        $this->tester->seeResponseMatchesJsonType(["errorText" => 'string:regex(/([\w\s"]*) is (a|an) ([\w\s"]*) and should be (a|an) ([\w\s"]*)/)']);
    }

    public function test0040EditUserPass()
    {
        $this->tester->haveHttpHeader("Content-Type", "application/json");
        $this->tester->sendPatch("/users/admin/user", [
            "id" => 1,
            "first_name" => "Jane"
        ]);
        $this->tester->seeResponseIsJson();
        $this->tester->seeResponseContainsJson(["success" => true]);
        $this->tester->seeResponseMatchesJsonType(["info" => "array"]);
        $this->tester->seeResponseMatchesJsonType([
            "id" => "integer",
            "first_name" => "string",
            "last_name" => "string",
            "age" => "integer"
        ], "$.info");
    }

    public function test0050EditUserFailMissingParameter()
    {
        $this->tester->haveHttpHeader("Content-Type", "application/json");
        $this->tester->sendPatch("/users/admin/user", [
            "first_name" => "Jame"
        ]);
        $this->tester->seeResponseIsJson();
        $this->tester->seeResponseContainsJson(["success" => false]);
        $this->tester->seeResponseMatchesJsonType(["errorText" => 'string:regex(/([\w\s"]*) is not provided$/)']);
    }

    public function test0060EditUserFailBadParameterType()
    {
        $this->tester->haveHttpHeader("Content-Type", "application/json");
        $this->tester->sendPatch("/users/admin/user", [
            "id" => "id",
            "first_name" => "Jane"
        ]);
        $this->tester->seeResponseIsJson();
        $this->tester->seeResponseContainsJson(["success" => false]);
        $this->tester->seeResponseMatchesJsonType(["errorText" => 'string:regex(/([\w\s"]*) is (a|an) ([\w\s"]*) and should be (a|an) ([\w\s"]*)/)']);
    }

    public function test0070GetUserPass()
    {
        $this->tester->haveHttpHeader("Content-Type", "application/json");
        $this->tester->sendGet("/users/admin/user", [
            "id" => 1
        ]);
        $this->tester->seeResponseIsJson();
        $this->tester->seeResponseContainsJson(["success" => true]);
        $this->tester->seeResponseMatchesJsonType(["info" => "array"]);
        $this->tester->seeResponseMatchesJsonType([
            "id" => "integer",
            "first_name" => "string",
            "last_name" => "string",
            "age" => "integer"
        ], "$.info");
    }

    public function test0080GetUserFailMissingParameter()
    {
        $this->tester->haveHttpHeader("Content-Type", "application/json");
        $this->tester->sendGet("/users/admin/user", []);
        $this->tester->seeResponseIsJson();
        $this->tester->seeResponseContainsJson(["success" => false]);
        $this->tester->seeResponseMatchesJsonType(["errorText" => 'string:regex(/([\w\s"]*) is not provided$/)']);
    }

    public function test0090GetUserFailBadParameterType()
    {
        $this->tester->haveHttpHeader("Content-Type", "application/json");
        $this->tester->sendGet("/users/admin/user", [
            "id" => "id"
        ]);
        $this->tester->seeResponseIsJson();
        $this->tester->seeResponseContainsJson(["success" => false]);
        $this->tester->seeResponseMatchesJsonType(["errorText" => 'string:regex(/([\w\s"]*) is (a|an) ([\w\s"]*) and should be (a|an) ([\w\s"]*)/)']);
    }

    public function test00100DeleteUserPass()
    {
        $this->tester->haveHttpHeader("Content-Type", "application/json");
        $this->tester->sendDelete("/users/admin/user", [
            "id" => 1
        ]);
        $this->tester->seeResponseIsJson();
        $this->tester->seeResponseContainsJson(["success" => true]);
        $this->tester->seeResponseMatchesJsonType(["info" => "array"]);
        $this->tester->seeResponseMatchesJsonType([
            "response" => "string"
        ], "$.info");
    }

    public function test00110DeleteUserFailMissingParameter()
    {
        $this->tester->haveHttpHeader("Content-Type", "application/json");
        $this->tester->sendDelete("/users/admin/user", []);
        $this->tester->seeResponseIsJson();
        $this->tester->seeResponseContainsJson(["success" => false]);
        $this->tester->seeResponseMatchesJsonType(["errorText" => 'string:regex(/([\w\s"]*) is not provided$/)']);
    }

    public function test00120DeleteUserFailBadParameterType()
    {
        $this->tester->haveHttpHeader("Content-Type", "application/json");
        $this->tester->sendDelete("/users/admin/user", [
            "id" => "id"
        ]);
        $this->tester->seeResponseIsJson();
        $this->tester->seeResponseContainsJson(["success" => false]);
        $this->tester->seeResponseMatchesJsonType(["errorText" => 'string:regex(/([\w\s"]*) is (a|an) ([\w\s"]*) and should be (a|an) ([\w\s"]*)/)']);
    }

    public function test00130MethodNotFound()
    {
        $this->tester->haveHttpHeader("Content-Type", "application/json");
        $this->tester->sendDelete("/users/admin/users", [
            "id" => "id"
        ]);
        $this->tester->seeResponseIsJson();
        $this->tester->seeResponseContainsJson(["success" => false, "errorText" => "Unauthorized or Private Method"]);
    }
}