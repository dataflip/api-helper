<?php

use Codeception\Test\Unit;
use DataBackFlip\ApiHelper;

class ApiHelperTest extends Unit
{
    /**
     * @throws Exception
     */
    public function test0010CreateInstance()
    {
        $this->assertTrue(is_object($this->getApiHelper()));
    }

    /**
     * @throws Exception
     */
    public function test0020Test()
    {
        $this->assertTrue(is_string($this->getApiHelper()->test()));
    }

    /**
     * @throws Exception
     */
    public function test0030TestMethod()
    {
        $class = $this->getApiHelper();

        $this->assertTrue($class->testMethod("test"));

        $this->assertFalse($class->testMethod("testing"));
    }

    /**
     * @throws Exception
     */
    public function test0040TestCallMethodPass()
    {
        $class = $this->getApiHelper();
        $output = $class->callMethod("test", false);
        $this->assertTrue($output["success"]);

        $class = $this->getApiHelper([],"GET",true);
        $output = $class->callMethod("test", false);
        $this->assertTrue($output["success"]);
    }

    /**
     * @throws Exception
     */
    public function test0040TestCallMethodFail()
    {
        $class = $this->getApiHelper();
        $output = $class->callMethod("unknown",false);
        $this->assertFalse($output["success"]);

        $class = $this->getApiHelper([],"GET",true);
        $output = $class->callMethod("unknown",false);
        $this->assertFalse($output["success"]);
    }

    /**
     * @param array|null $request
     * @param string $httpMethod
     * @param bool $debug
     * @return ApiHelper
     * @throws Exception
     */
    protected function getApiHelper(?array $request = NULL, string $httpMethod = "GET", bool $debug = false): ApiHelper
    {
        return new ApiHelper(["request" => $request, "httpMethod" => $httpMethod], [], $debug);
    }

}