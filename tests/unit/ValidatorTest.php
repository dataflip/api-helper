<?php

use Codeception\Test\Unit;
use DataBackFlip\Validator;

class ValidatorTest extends Unit
{
    protected UnitTester $tester;


    /**
     * @throws Exception
     */
    public function test0010InitPass()
    {
        $class = $this->getValidator();
        $this->assertTrue(is_array($class->init()));
        $class->generateSchema = TRUE;
        $class->exitOnSchemaGenerate = FALSE;
        $this->assertTrue(is_array($class->init(
            [
                ['field' => 0, 'type' => 'array', 'min' => 1, 'format' => 'multi_array', 'required' => true,
                    'nested' => [
                        ['field' => "name", 'type' => 'string', 'required' => true]
                    ]
                ]
            ],
            [
                ['field' => 'user_info', 'type' => 'object', 'required' => true,
                    'nested' => [
                        ['field' => 'id', 'type' => 'integer', 'required' => true],
                        ['field' => 'name', 'type' => 'string', 'required' => true],
                        ['field' => 'age', 'required' => true]
                    ]
                ],
                ['field' => 'children_info', 'type' => 'array', "format" => "multi_array", 'required' => true,
                    'nested' => [
                        ['field' => 'id', 'type' => 'integer', 'required' => true],
                        ['field' => 'name', 'type' => 'string', 'required' => true],
                        ['field' => 'age', 'required' => true]
                    ]
                ]
            ],
            "GET",
            ["field_prefix" => "0.*."]
        )));
    }

    public function test0020InitFailFieldNotProvided()
    {
        $class = $this->getValidator();
        $class->generateSchema = TRUE;
        $class->exitOnSchemaGenerate = FALSE;
        $this->tester->expectThrowable(new Exception('validator "field" is not provided', 400), function () use ($class) {
            $this->assertTrue(is_array($class->init([
                ['type' => 'string', 'required' => true]],
            )));
        });
    }

    /**
     * @throws Exception
     */
    public function test0030ValidateRequestPass()
    {
        $class = $this->getValidator();
        $class->init([['field' => "is_required", 'type' => "boolean"]]);
        $this->assertTrue(is_array($class->validateRequest()));
        $this->assertTrue(is_array($class->validateRequest(["is_required" => true], ["quote_boolean" => true])));
        $this->assertTrue(is_array($class->validateRequest(["is_required" => false], ["quote_boolean" => true])));
        $class->init([
            ['field' => 'user_info', 'type' => 'object', 'required' => true,
                'nested' => [
                    ['field' => 'name', 'type' => 'string', 'required' => true],
                    ['field' => 'age', 'type' => 'integer', 'required' => true],
                ]
            ]
        ]);
        $this->assertTrue(is_array($class->validateRequest(["user_info" => ["name" => "test", "age" => 10]])));
    }


    /**
     * @throws Exception
     */
    public function test0040ValidateFailMethodNotAllowed()
    {
        $class = $this->getValidator();
        $this->tester->expectThrowable(new Exception('Method Not Allowed', 405), function () use ($class) {
            $_SERVER['REQUEST_METHOD'] = "POST";
            $class->init([], [], "GET");
            $class->validateRequest();
        });
    }

    public function test0050ValidateFailFieldNotProvided()
    {
        $class = $this->getValidator();
        $this->tester->expectThrowable(new Exception('validator "field" is not provided', 400), function () use ($class) {
            $class->init(["test" => "fail"]);
            $class->validateRequest();
        });
    }

    public function test0060ValidateFailFieldNameNotProvided()
    {
        $class = $this->getValidator();
        $prefix = "0.0.";
        $tPrefix = "0.";
        $fieldName = $tPrefix . "name";
        $this->tester->expectThrowable(new Exception('"' . $fieldName . '" is not provided', 400), function () use ($class, $fieldName, $prefix) {
            $class->init([['field' => ltrim($fieldName, "0."), 'type' => 'string', 'required' => true]]);
            $class->validateRequest([], ["field_prefix" => $prefix]);
        });
    }

    public function test0070ValidateFailUnknownFieldType()
    {
        $class = $this->getValidator();
        $tPrefix = "0.";
        $fieldName = $tPrefix . "name";
        $this->tester->expectThrowable(new Exception('"' . $fieldName . '" is invalid', 400), function () use ($class, $fieldName) {
            $class->init([['field' => 0, 'type' => 'array', 'min' => 1, 'format' => 'multi_array', 'required' => true,
                'nested' => [
                    ['field' => "name", 'type' => 'unknown', 'required' => true]
                ]]
            ]);
            $class->validateRequest([["name" => "test"]]);
        });
    }

    public function test0080ValidateFailFieldTypeMismatch()
    {
        $class = $this->getValidator();
        $expectedType = "boolean";
        $setType = "string";
        $fieldName = "is_required";
        $this->tester->expectThrowable(new Exception('"' . $fieldName . '" is a "' . $setType . '" and should be a "' . $expectedType . '"', 400), function () use ($class, $fieldName, $expectedType) {
            $class->init([['field' => $fieldName, 'type' => $expectedType, 'required' => true]]);
            $class->validateRequest([$fieldName => "test"]);
        });
    }

    public function test0090ValidateFailFieldExpectedArrayGotObject()
    {
        $class = $this->getValidator();
        $fieldName = "names";
        $this->tester->expectThrowable(new Exception('"' . $fieldName . '" is an "object" and should be an "array"', 400), function () use ($class, $fieldName) {
            $class->init([['field' => $fieldName, 'type' => "array", 'required' => true]]);
            $class->validateRequest([$fieldName => ["name" => "test"]]);
        });
    }

    public function test00100ValidateFailFieldExpectedObject()
    {
        $class = $this->getValidator();
        $fieldName = "info";
        $expectedType = "object";
        $this->tester->expectThrowable(new Exception('"' . $fieldName . '" is an "array" and should be an "' . $expectedType . '"', 400), function () use ($class, $fieldName, $expectedType) {
            $class->init([['field' => $fieldName, 'type' => $expectedType, 'required' => true]]);
            $class->validateRequest([$fieldName => ["test"]]);
        });
    }

    public function test00110ValidateFailFieldExpectedNotToBeEmpty()
    {
        $class = $this->getValidator();
        $fieldName = "is_required";
        $this->tester->expectThrowable(new Exception('"' . $fieldName . '" is empty', 400), function () use ($class, $fieldName) {
            $class->init([['field' => $fieldName, 'type' => "boolean", 'required' => true, "empty" => false]]);
            $class->validateRequest([$fieldName => false]);
        });
    }

    public function test00120ValidateFailFieldNotInArray()
    {
        $class = $this->getValidator();
        $fieldName = "table_delete_actions";
        $this->tester->expectThrowable(new Exception('"' . $fieldName . '" is invalid', 400), function () use ($class, $fieldName) {
            $class->init([['field' => $fieldName, 'type' => "array", 'in_array' => ["RESTRICT", "CASCADE", "NO ACTION", "SET NULL"], 'required' => true]]);
            $class->validateRequest([$fieldName => ["UNKNOWN"]]);
        });

        $fieldName = "table_delete_action";
        $this->tester->expectThrowable(new Exception('"' . $fieldName . '" is invalid', 400), function () use ($class, $fieldName) {
            $class->init([['field' => $fieldName, 'type' => "string", 'in_array' => ["RESTRICT", "CASCADE", "NO ACTION", "SET NULL"], 'required' => true]]);
            $class->validateRequest([$fieldName => "UNKNOWN"]);
        });
    }

    public function test00130ValidateFailFieldNotInArrayCaseInsensitive()
    {
        $class = $this->getValidator();
        $fieldName = "table_delete_actions";
        $this->tester->expectThrowable(new Exception('"' . $fieldName . '" is invalid', 400), function () use ($class, $fieldName) {
            $class->init([['field' => $fieldName, 'type' => "string", 'in_arrayi' => ["RESTRICT", "CASCADE", "NO ACTION", "SET NULL"], 'required' => true]]);
            $class->validateRequest([$fieldName => "RESTRICT"]);
            $class->validateRequest([$fieldName => "UNKNOWN"]);
        });
    }

    public function test00140ValidateFailFieldNotMinimumSize()
    {
        $class = $this->getValidator();
        $fieldName = "table_delete_actions";
        $minSize = 2;
        $this->tester->expectThrowable(new Exception('"' . $fieldName . '" minimum size is "' . $minSize . '"', 400), function () use ($class, $fieldName, $minSize) {
            $class->init([['field' => $fieldName, 'type' => "array", 'min' => $minSize, 'required' => true]]);
            $class->validateRequest([$fieldName => ["UNKNOWN"]]);
        });

        $fieldName = "table_delete_action";
        $this->tester->expectThrowable(new Exception('"' . $fieldName . '" minimum size is "' . $minSize . '"', 400), function () use ($class, $fieldName, $minSize) {
            $class->init([['field' => $fieldName, 'type' => "string", 'min' => $minSize, 'required' => true]]);
            $class->validateRequest([$fieldName => "U"]);
        });
    }

    public function test00150ValidateFailFieldNotMultiArray()
    {
        $class = $this->getValidator();
        $fieldName = "table_delete_actions";
        $this->tester->expectThrowable(new Exception('"' . $fieldName . '" is not a multidimensional array', 400), function () use ($class, $fieldName) {
            $class->init([['field' => $fieldName, 'type' => "array", 'format' => "multi_array", 'required' => true]]);
            $class->validateRequest([$fieldName => ["UNKNOWN"]]);
        });
    }

    public function test00160ValidateFailFieldNotUniqueArray()
    {
        $class = $this->getValidator();
        $fieldName = "table_delete_actions";
        $this->tester->expectThrowable(new Exception('"' . $fieldName . '" must contain unique values', 400), function () use ($class, $fieldName) {
            $class->init([['field' => $fieldName, 'type' => "array", 'is_unique' => true, 'required' => true]]);
            $class->validateRequest([$fieldName => ["UNKNOWN", "UNKNOWN"]]);
        });
    }

    public function test00170ValidateFailFieldNotValidEmail()
    {
        $class = $this->getValidator();
        $fieldName = "user_email";
        $this->tester->expectThrowable(new Exception('"' . $fieldName . '" is not an email address', 400), function () use ($class, $fieldName) {
            $class->init([['field' => $fieldName, 'type' => "string", "format" => "email", 'required' => true]]);
            $class->validateRequest([$fieldName => "test@"]);
        });
    }

    public function test00180ValidateFailFieldNotValidURL()
    {
        $class = $this->getValidator();
        $fieldName = "site_url";
        $this->tester->expectThrowable(new Exception('"' . $fieldName . '" is not a valid URL', 400), function () use ($class, $fieldName) {
            $class->init([['field' => $fieldName, 'type' => "string", "format" => "url", 'required' => true]]);
            $class->validateRequest([$fieldName => "test_url"]);
        });
    }

    /**
     * @throws Exception
     */
    public function test00190ValidateResponse()
    {
        $class = $this->getValidator();
        $this->assertTrue(is_array($class->validateResponse()));
    }

    protected function getValidator(): Validator
    {
        return new Validator();
    }

}